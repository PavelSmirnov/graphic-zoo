struct VsOutput {};

struct GsOutput {
    float4 position : SV_Position;
    float3 direction : DIRECTION;
};

cbuffer ViewProjectionBuffer : register(b0) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

float3 CalcDirection(float2 uv) {
    float a = mul(Projection, float4(0, 0, 1, 0)).z; // TODO: read from buffer
    float4 projectedPosition = float4(2 * uv.x - 1, 1 - 2 * uv.y, a, 1);
    float4 cameraViewDirection = mul(InverseProjection, projectedPosition);
    return normalize(mul(InverseView, cameraViewDirection).xyz);
}

[maxvertexcount(6)]
void sky_directions_gs(point VsOutput input[1], inout TriangleStream<GsOutput> triangleStream) {
    GsOutput ld;
    ld.position = float4(-1, -1, 0, 1);
    ld.direction = CalcDirection(float2(0, 1));

    GsOutput lu;
    lu.position = float4(-1, +1, 0, 1);
    lu.direction = CalcDirection(float2(0, 0));

    GsOutput rd;
    rd.position = float4(+1, -1, 0, 1);
    rd.direction = CalcDirection(float2(1, 1));

    GsOutput ru;
    ru.position = float4(+1, +1, 0, 1);
    ru.direction = CalcDirection(float2(1, 0));

    triangleStream.Append(ld);
    triangleStream.Append(lu);
    triangleStream.Append(ru);
    triangleStream.RestartStrip();

    triangleStream.Append(ld);
    triangleStream.Append(ru);
    triangleStream.Append(rd);
    triangleStream.RestartStrip();
}
