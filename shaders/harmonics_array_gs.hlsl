struct VsOutput {};

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
    uint arrayIndex : SV_RenderTargetArrayIndex;
};

[maxvertexcount(6 * SPHERICAL_HARMONICS_ARRAY_SIZE)]
void harmonics_array_gs(point VsOutput input[1], inout TriangleStream<GsOutput> triangleStream) {
    GsOutput ld;
    ld.position = float4(-1, -1, 0, 1);
    ld.uv = float2(0, 1);
    ld.arrayIndex = SPHERICAL_HARMONICS_ARRAY_SIZE;

    GsOutput lu;
    lu.position = float4(-1, +1, 0, 1);
    lu.uv = float2(0, 0);
    lu.arrayIndex = SPHERICAL_HARMONICS_ARRAY_SIZE;

    GsOutput rd;
    rd.position = float4(+1, -1, 0, 1);
    rd.uv = float2(1, 1);
    rd.arrayIndex = SPHERICAL_HARMONICS_ARRAY_SIZE;

    GsOutput ru;
    ru.position = float4(+1, +1, 0, 1);
    ru.uv = float2(1, 0);
    ru.arrayIndex = SPHERICAL_HARMONICS_ARRAY_SIZE;

    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        lu.arrayIndex = i;
        rd.arrayIndex = i;

        triangleStream.Append(lu);
        triangleStream.Append(ru);
        triangleStream.Append(ld);
        triangleStream.RestartStrip();

        triangleStream.Append(rd);
        triangleStream.Append(ld);
        triangleStream.Append(ru);
        triangleStream.RestartStrip();
    }
}
