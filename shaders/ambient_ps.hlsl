#define DRAW_AMBIENT true
#define EPS 1e-6

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

cbuffer AmbientBuffer : register(b0) {
    float4 EnvironmentIrradianceRedArray[SPHERICAL_HARMONICS_ARRAY_SIZE];
    static float EnvironmentIrradianceRed[SPHERICAL_HARMONICS_NUM_COEFFICIENTS] = (float[SPHERICAL_HARMONICS_NUM_COEFFICIENTS])EnvironmentIrradianceRedArray;

    float4 EnvironmentIrradianceGreenArray[SPHERICAL_HARMONICS_ARRAY_SIZE];
    static float EnvironmentIrradianceGreen[SPHERICAL_HARMONICS_NUM_COEFFICIENTS] = (float[SPHERICAL_HARMONICS_NUM_COEFFICIENTS])EnvironmentIrradianceGreenArray;

    float4 EnvironmentIrradianceBlueArray[SPHERICAL_HARMONICS_ARRAY_SIZE];
    static float EnvironmentIrradianceBlue[SPHERICAL_HARMONICS_NUM_COEFFICIENTS] = (float[SPHERICAL_HARMONICS_NUM_COEFFICIENTS])EnvironmentIrradianceBlueArray;

    float4 DiffuseReflectanceArray[SPHERICAL_HARMONICS_ARRAY_SIZE];
    static float DiffuseReflectance[SPHERICAL_HARMONICS_NUM_COEFFICIENTS] = (float[SPHERICAL_HARMONICS_NUM_COEFFICIENTS])DiffuseReflectanceArray;
};

cbuffer ViewProjectionBuffer : register(b1) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

SamplerState AllPointZeroSampler : register(s0);
SamplerState AllLinearClampSampler : register(s1);
Texture2D GBuffer0 : register(t0); // base color (dielectric diffuse or conductor specular) and dielectric specular color
Texture2D GBuffer1 : register(t1); // cameraSpaceNormal and isOccupied
Texture2D GBuffer2 : register(t2); // worldSpacePosition
Texture2D GBuffer3 : register(t3); // metalness and roughness
Texture2D ReflectionDirections : register(t4);
Texture2DArray ReflectionsHarmonicValues : register(t5);
Texture3D<float> SpecularBrdf : register(t6);
Texture2D<float> DiffuseFresnel : register(t7);

float IntegrateProduct(float lhsHarmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS], float rhsHarmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS]) {
    float result = 0.0;
    for (int i = 0; i < SPHERICAL_HARMONICS_NUM_COEFFICIENTS; ++i) {
        result += lhsHarmonics[i] * rhsHarmonics[i];
    }
    return result;
}

void RotateAroundZ(in float angle,
                   in float harmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS],
                   out float rotatedHarmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS]) {
    float cosA[SPHERICAL_HARMONICS_NUM_BANDS];
    float sinA[SPHERICAL_HARMONICS_NUM_BANDS];
    for (int i = 0; i < SPHERICAL_HARMONICS_NUM_BANDS; ++i) {
        cosA[i] = cos(i * angle);
        sinA[i] = sin(i * angle);
    }

    for (int l = 0; l < SPHERICAL_HARMONICS_NUM_BANDS; ++l) {
        for (int m = -l; m <= l; ++m) {
            int i = l * l + (m + l);
            if (m == 0) {
                rotatedHarmonics[i] = harmonics[i];
            } else if (m < 0) {
                rotatedHarmonics[i] = harmonics[i] * cosA[-m] + harmonics[i - 2 * m] * sinA[-m];
            } else {
                rotatedHarmonics[i] = harmonics[i] * cosA[m] - harmonics[i - 2 * m] * sinA[m];
            }
        }
    }
}

void Rotate90DegreesAroundXForward(in float harmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS],
                                   out float rotatedHarmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS]) {
    if (SPHERICAL_HARMONICS_NUM_BANDS > 0) {
        rotatedHarmonics[0] = harmonics[0];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 1) {
        rotatedHarmonics[1] = harmonics[2];
        rotatedHarmonics[2] = -harmonics[1];
        rotatedHarmonics[3] = harmonics[3];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 2) {
        rotatedHarmonics[4] = harmonics[7];
        rotatedHarmonics[5] = -harmonics[5];
        rotatedHarmonics[6] = -0.5 * harmonics[6] - sqrt(3) * 0.5 * harmonics[8];
        rotatedHarmonics[7] = -harmonics[4];
        rotatedHarmonics[8] = -sqrt(3) * 0.5 * harmonics[6] + 0.5 * harmonics[8];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 3) {
        rotatedHarmonics[9] = -0.7905 * harmonics[12] + 0.6125 * harmonics[14];
        rotatedHarmonics[10] = -harmonics[10];
        rotatedHarmonics[11] = -0.6125 * harmonics[12] - 0.791 * harmonics[14];
        rotatedHarmonics[12] = 0.791 * harmonics[9] + 0.6125 * harmonics[11];
        rotatedHarmonics[13] = -0.25 * harmonics[13] -0.968 * harmonics[15];
        rotatedHarmonics[14] = -0.6125 * harmonics[9] + 0.791 * harmonics[11];
        rotatedHarmonics[15] = -0.968 * harmonics[13] + 0.25 * harmonics[15];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 4) {
        rotatedHarmonics[16] = -0.936 * harmonics[21] + 0.354 * harmonics[23];
        rotatedHarmonics[17] = -0.75 * harmonics[17] + 0.661 * harmonics[19];
        rotatedHarmonics[18] = -0.354 * harmonics[21] - 0.936 * harmonics[23];
        rotatedHarmonics[19] = 0.661 * harmonics[17] + 0.75 * harmonics[19];
        rotatedHarmonics[20] = 0.375 * harmonics[20] + 0.559 * harmonics[22] + 0.74 * harmonics[24];
        rotatedHarmonics[21] = 0.936 * harmonics[16] + 0.354 * harmonics[18];
        rotatedHarmonics[22] = 0.559 * harmonics[20] + 0.5 * harmonics[22] - 0.661 * harmonics[24];
        rotatedHarmonics[23] = -0.354 * harmonics[16] + 0.936 * harmonics[18];
        rotatedHarmonics[24] = 0.74 * harmonics[20] - 0.661 * harmonics[22] + 0.125 * harmonics[24];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 5) {
        rotatedHarmonics[25] = 0.7023 * harmonics[30] - 0.6852 * harmonics[32] + 0.1985 * harmonics[34];
        rotatedHarmonics[26] = -0.5004 * harmonics[26] + 0.8659 * harmonics[28];
        rotatedHarmonics[27] = 0.5228 * harmonics[30] + 0.3067 * harmonics[32] - 0.7956 * harmonics[34];
        rotatedHarmonics[28] = 0.8652 * harmonics[26] + 0.499 * harmonics[28];
        rotatedHarmonics[29] = 0.4847 * harmonics[30] + 0.6614 * harmonics[32] + 0.5729 * harmonics[34];
        rotatedHarmonics[30] = -0.7015 * harmonics[25] - 0.5231 * harmonics[27] - 0.4842 * harmonics[29];
        rotatedHarmonics[31] = 0.4051 * harmonics[33];
        rotatedHarmonics[32] = 0.684 * harmonics[25] - 0.3063 * harmonics[27];
        rotatedHarmonics[33] = 0.4047 * harmonics[31] + 0.813 * harmonics[33] - 0.4194 * harmonics[35];
        rotatedHarmonics[34] = -0.1979 * harmonics[25] - 0.5726 * harmonics[29];
        rotatedHarmonics[35] = 0.9054 * harmonics[31] - 0.4197 * harmonics[33] + 0.0626 * harmonics[35];
    }

    // doesn't matter that much for diffuse lighting
    for (int i = 36; i < SPHERICAL_HARMONICS_NUM_COEFFICIENTS; ++i) {
        rotatedHarmonics[i] = 0;
    }
}

void Rotate90DegreesAroundXBackward(in float harmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS],
                                    out float rotatedHarmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS]) {
    if (SPHERICAL_HARMONICS_NUM_BANDS > 0) {
        rotatedHarmonics[0] = harmonics[0];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 1) {
        rotatedHarmonics[1] = -harmonics[2];
        rotatedHarmonics[2] = harmonics[1];
        rotatedHarmonics[3] = harmonics[3];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 2) {
        rotatedHarmonics[4] = -harmonics[7];
        rotatedHarmonics[5] = -harmonics[5];
        rotatedHarmonics[6] = -0.5 * harmonics[6] - sqrt(3) * 0.5 * harmonics[8];
        rotatedHarmonics[7] = harmonics[4];
        rotatedHarmonics[8] = -sqrt(3) * 0.5 * harmonics[6] + 0.5 * harmonics[8];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 3) {
        rotatedHarmonics[9] = 0.7905 * harmonics[12] - 0.6125 * harmonics[14];
        rotatedHarmonics[10] = -harmonics[10];
        rotatedHarmonics[11] = 0.6125 * harmonics[12] + 0.791 * harmonics[14];
        rotatedHarmonics[12] = -0.791 * harmonics[9] - 0.6125 * harmonics[11];
        rotatedHarmonics[13] = -0.25 * harmonics[13] -0.968 * harmonics[15];
        rotatedHarmonics[14] = 0.6125 * harmonics[9] - 0.791 * harmonics[11];
        rotatedHarmonics[15] = -0.968 * harmonics[13] + 0.25 * harmonics[15];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 4) {
        rotatedHarmonics[16] = 0.936 * harmonics[21] - 0.354 * harmonics[23];
        rotatedHarmonics[17] = -0.75 * harmonics[17] + 0.661 * harmonics[19];
        rotatedHarmonics[18] = 0.354 * harmonics[21] + 0.936 * harmonics[23];
        rotatedHarmonics[19] = 0.661 * harmonics[17] + 0.75 * harmonics[19];
        rotatedHarmonics[20] = 0.375 * harmonics[20] + 0.559 * harmonics[22] + 0.74 * harmonics[24];
        rotatedHarmonics[21] = -0.936 * harmonics[16] - 0.354 * harmonics[18];
        rotatedHarmonics[22] = 0.559 * harmonics[20] + 0.5 * harmonics[22] - 0.661 * harmonics[24];
        rotatedHarmonics[23] = 0.354 * harmonics[16] - 0.936 * harmonics[18];
        rotatedHarmonics[24] = 0.74 * harmonics[20] - 0.661 * harmonics[22] + 0.125 * harmonics[24];
    }

    if (SPHERICAL_HARMONICS_NUM_BANDS > 5) {
        rotatedHarmonics[25] = -0.7025 * harmonics[30] + 0.6851 * harmonics[32] - 0.1979 * harmonics[34];
        rotatedHarmonics[26] = -0.5 * harmonics[26] + 0.8663 * harmonics[28];
        rotatedHarmonics[27] = -0.523 * harmonics[30] - 0.306 * harmonics[32] + 0.7959 * harmonics[34];
        rotatedHarmonics[28] = 0.8654 * harmonics[26];
        rotatedHarmonics[29] = -0.484 * harmonics[30] - 0.6609 * harmonics[32] - 0.573 * harmonics[34];
        rotatedHarmonics[30] = 0.7015 * harmonics[25] + 0.5225 * harmonics[27] + 0.4838 * harmonics[29];
        rotatedHarmonics[31] = 0.1249 * harmonics[31] + 0.4055 * harmonics[33] + 0.9062 * harmonics[35];
        rotatedHarmonics[32] = -0.6843 * harmonics[25] + 0.3062 * harmonics[27];
        rotatedHarmonics[33] = 0.4049 * harmonics[31] + 0.8125 * harmonics[33] - 0.4191 * harmonics[35];
        rotatedHarmonics[34] = -0.7959 * harmonics[27] + 0.5727 * harmonics[29];
        rotatedHarmonics[35] = 0.9058 * harmonics[31] - 0.4195 * harmonics[33] + 0.06235 * harmonics[35];
    }

    // doesn't matter that much for diffuse lighting
    for (int i = 36; i < SPHERICAL_HARMONICS_NUM_COEFFICIENTS; ++i) {
        rotatedHarmonics[i] = 0;
    }
}

void RotateHarmonics(in float3 direction,
                     in float harmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS],
                     out float rotatedHarmonics[SPHERICAL_HARMONICS_NUM_COEFFICIENTS]) {
    float cosTheta = direction.z;
    float sinTheta = sqrt(1 - cosTheta * cosTheta);

    if (sinTheta < EPS) {
        for (int i = 0; i < SPHERICAL_HARMONICS_NUM_COEFFICIENTS; ++i) {
            rotatedHarmonics[i] = harmonics[i];
        }
        return;
    }

    float theta = acos(cosTheta);
    float sinPhi = direction.y / sinTheta;
    float cosPhi = direction.x / sinTheta;
    float phi = atan2(sinPhi, cosPhi);

    float firstRotation[SPHERICAL_HARMONICS_NUM_COEFFICIENTS];
    Rotate90DegreesAroundXForward(harmonics, firstRotation);
    float secondRotation[SPHERICAL_HARMONICS_NUM_COEFFICIENTS];
    RotateAroundZ(theta, firstRotation, secondRotation);
    float thirdRotation[SPHERICAL_HARMONICS_NUM_COEFFICIENTS];
    Rotate90DegreesAroundXBackward(secondRotation, thirdRotation);
    RotateAroundZ(phi, thirdRotation, rotatedHarmonics);
}

float4 ambient_ps(in GsOutput input) : SV_Target {
    float4 gbuffer0 = GBuffer0.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer1 = GBuffer1.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer2 = GBuffer2.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer3 = GBuffer3.SampleLevel(AllPointZeroSampler, input.uv, 0);

    bool isOccupied = gbuffer1.w > 0.5;
    if (!isOccupied) {
        return float4(0, 0, 0, 0);
    }

    float3 lightIntensity = float3(0, 0, 0);
    if (DRAW_AMBIENT) {
        float metalness = gbuffer3.x;
        float3 baseColor = gbuffer0.xyz;
        float3 diffuseColor = (1 - metalness) * baseColor;
        float dielectricSpecular = gbuffer0.w;
        float3 specularColor = metalness * baseColor + (1 - metalness) * dielectricSpecular * float3(1, 1, 1);
        float roughness = gbuffer3.y;
        float specularPower = MIN_SPECULAR_POWER * pow(MAX_SPECULAR_POWER / MIN_SPECULAR_POWER, 1 - roughness);

        float3 cameraSpaceNormal = gbuffer1.xyz;
        float3 worldSpaceNormal = mul(InverseView, float4(cameraSpaceNormal, 0)).xyz;
        float3 worldSpacePosition = gbuffer2.xyz;

        float diffuseReflectance[SPHERICAL_HARMONICS_NUM_COEFFICIENTS];
        RotateHarmonics(worldSpaceNormal, DiffuseReflectance, diffuseReflectance);
        float3 irradiance = float3(IntegrateProduct(EnvironmentIrradianceRed, diffuseReflectance),
                                   IntegrateProduct(EnvironmentIrradianceGreen, diffuseReflectance),
                                   IntegrateProduct(EnvironmentIrradianceBlue, diffuseReflectance));
        irradiance = max(irradiance, float3(0, 0, 0));
        float3 fresnelReflectanceDiffuse = float3(DiffuseFresnel.Sample(AllLinearClampSampler, float2(specularColor.x, 0)),
                                                  DiffuseFresnel.Sample(AllLinearClampSampler, float2(specularColor.y, 0)),
                                                  DiffuseFresnel.Sample(AllLinearClampSampler, float2(specularColor.z, 0)));
        float3 diffuseIntensity = (irradiance / PI) * diffuseColor * (1 - fresnelReflectanceDiffuse);

        float4 reflectionHarmonics[SPHERICAL_HARMONICS_ARRAY_SIZE];
        for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
            reflectionHarmonics[i] = ReflectionsHarmonicValues.Sample(AllPointZeroSampler, float3(input.uv, i));
        }
        float reflectionHarmonicsArray[SPHERICAL_HARMONICS_NUM_COEFFICIENTS];
        for (int i = 0; i < SPHERICAL_HARMONICS_NUM_COEFFICIENTS; ++i) {
            reflectionHarmonicsArray[i] = ((float[4 * SPHERICAL_HARMONICS_ARRAY_SIZE])reflectionHarmonics)[i];
        }
        float3 reflectionColor = float3(IntegrateProduct(EnvironmentIrradianceRed, reflectionHarmonicsArray),
                                        IntegrateProduct(EnvironmentIrradianceGreen, reflectionHarmonicsArray),
                                        IntegrateProduct(EnvironmentIrradianceBlue, reflectionHarmonicsArray));
        reflectionColor = max(reflectionColor, float3(0, 0, 0));
        reflectionColor /= PI; // looks bad due to few bands, so scale
        // two simplifications:
        //reflectionColor = float3(EnvironmentIrradianceRed[0], EnvironmentIrradianceGreen[0], EnvironmentIrradianceBlue[0]);
        //reflectionColor = irradiance / PI;

        float3 directionToEye = normalize(mul(InverseView, float4(0, 0, 0, 1)).xyz - worldSpacePosition);
        float eyeDotNormal = dot(directionToEye, worldSpaceNormal);
        float3 specularFraction = float3(SpecularBrdf.Sample(AllLinearClampSampler, float3(specularColor.x, eyeDotNormal, roughness)),
                                         SpecularBrdf.Sample(AllLinearClampSampler, float3(specularColor.y, eyeDotNormal, roughness)),
                                         SpecularBrdf.Sample(AllLinearClampSampler, float3(specularColor.z, eyeDotNormal, roughness)));
        specularFraction *= 2 * PI; // after integration;
        float3 specularIntensity = specularFraction * reflectionColor;

        lightIntensity = specularIntensity + diffuseIntensity;
    }

    return float4(lightIntensity, 0);
}
