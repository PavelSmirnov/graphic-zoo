#define USE_TAA true
// one minus decay
#define DIRECTION_EPS 1e-6

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

cbuffer TaaBuffer : register(b0) {
    float4x4 PreviousFrameViewProjectionMatrix;
};

SamplerState AllPointZeroSampler : register(s0);
SamplerState AllLinearClampSampler : register(s1);
Texture2D HistoryFrame : register(t0);
Texture2D NewFrame : register(t1);
Texture2D GBufferPartWithWorldPosition : register(t2);

float3 ConvertRGBToYCoCg(float3 rgb) {
    return float3(0.25 * (rgb.x + 2 * rgb.y + rgb.z),
                  0.5 * (rgb.x - rgb.z),
                  0.25 * (-rgb.x + 2 * rgb.y - rgb.z));
}

float3 ConvertYCoCgToRGB(float3 ycocg) {
    return float3(ycocg.x + ycocg.y - ycocg.z,
                  ycocg.x + ycocg.z,
                  ycocg.x - ycocg.y - ycocg.z);
}

float3 ClipColor(float3 historyColor, float3 target, float3 neighborhoodColorMean, float3 neighborhoodColorVariance) {
    float3 lowBorder = neighborhoodColorMean - TAA_VARIANCE_SCALE * neighborhoodColorVariance;
    float3 highBorder = neighborhoodColorMean + TAA_VARIANCE_SCALE * neighborhoodColorVariance;

    // if in AABB
    if (lowBorder.x <= historyColor.x && historyColor.x <= highBorder.x &&
        lowBorder.y <= historyColor.y && historyColor.y <= highBorder.y &&
        lowBorder.z <= historyColor.z && historyColor.z <= highBorder.z) {
        return historyColor;
    }

    float3 direction = target - historyColor;
    float shift = length(direction);
    if (shift < DIRECTION_EPS) { // if in target
        return clamp(historyColor, lowBorder, highBorder);
    }
    direction /= shift; // normalization

    if (lowBorder.x > historyColor.x && direction.x > 0) {
        shift = min(shift, (lowBorder.x - historyColor.x) / direction.x);
    }
    if (lowBorder.y > historyColor.y && direction.y > 0) {
        shift = min(shift, (lowBorder.y - historyColor.y) / direction.y);
    }
    if (lowBorder.z > historyColor.z && direction.z > 0) {
        shift = min(shift, (lowBorder.z - historyColor.z) / direction.z);
    }
    if (highBorder.x < historyColor.x && direction.x < 0) {
        shift = min(shift, (highBorder.x - historyColor.x) / direction.x);
    }
    if (highBorder.y < historyColor.y && direction.y < 0) {
        shift = min(shift, (highBorder.y - historyColor.y) / direction.y);
    }
    if (highBorder.z < historyColor.z && direction.z < 0) {
        shift = min(shift, (highBorder.z - historyColor.z) / direction.z);
    }
    historyColor += direction * shift;
    return clamp(historyColor, lowBorder, highBorder);
}

float4 taa_ps(in GsOutput input) : SV_Target {
    float4 worldPosition = GBufferPartWithWorldPosition.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 reprojectedPosition = mul(PreviousFrameViewProjectionMatrix, worldPosition);

    float4 newPixel = NewFrame.SampleLevel(AllPointZeroSampler, input.uv, 0);
    if (USE_TAA) {
        if (reprojectedPosition.w > 0) {
            reprojectedPosition /= reprojectedPosition.w;
            if (reprojectedPosition.z >= 0 && reprojectedPosition.z <= 1 &&
                reprojectedPosition.x >= -1 && reprojectedPosition.x <= 1 &&
                reprojectedPosition.y >= -1 && reprojectedPosition.y <= 1) {

                uint frameWidth, frameHeight;
                NewFrame.GetDimensions(frameWidth, frameHeight);
                float2 pixelSize = float2(1.0 / frameWidth, 1.0 / frameHeight);
                const int historyNeighborhoodSize = 9;
                float3 historyPixelNeighborhood[historyNeighborhoodSize] = {
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(-pixelSize.x, -pixelSize.y), 0).xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(-pixelSize.x,            0), 0).xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(-pixelSize.x, +pixelSize.y), 0).xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(           0, -pixelSize.y), 0).xyz,
                    newPixel.xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(           0, +pixelSize.y), 0).xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(+pixelSize.x, -pixelSize.y), 0).xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(+pixelSize.x,            0), 0).xyz,
                    NewFrame.SampleLevel(AllPointZeroSampler, input.uv + float2(+pixelSize.x, +pixelSize.y), 0).xyz,
                };
                for (int i = 0; i < historyNeighborhoodSize; ++i) {
                    historyPixelNeighborhood[i] = ConvertRGBToYCoCg(historyPixelNeighborhood[i]);
                }
                float3 neighborhoodColorMean = 0.0;
                for (int i = 0; i < historyNeighborhoodSize; ++i) {
                    neighborhoodColorMean += historyPixelNeighborhood[i];
                }
                neighborhoodColorMean /= historyNeighborhoodSize;
                float3 neighborhoodColorVarianceSquared = 0.0;
                for (int i = 0; i < historyNeighborhoodSize; ++i) {
                    float3 diff = historyPixelNeighborhood[i] - neighborhoodColorMean;
                    neighborhoodColorVarianceSquared += diff * diff;
                }
                neighborhoodColorVarianceSquared /= (historyNeighborhoodSize - 1);
                float3 neighborhoodColorVariance = sqrt(neighborhoodColorVarianceSquared);

                float2 reprojectedUv = float2(reprojectedPosition.x + 1, 1 - reprojectedPosition.y) * 0.5;
                float3 historyColor = ConvertRGBToYCoCg(HistoryFrame.SampleLevel(AllLinearClampSampler, reprojectedUv, 0).xyz);
                historyColor = ClipColor(historyColor, historyPixelNeighborhood[historyNeighborhoodSize / 2], neighborhoodColorMean, neighborhoodColorVariance);
                float4 historyPixel = float4(ConvertYCoCgToRGB(historyColor), 1);
                newPixel = (1.0 - TAA_COEFFICIENT) * historyPixel + TAA_COEFFICIENT * newPixel;
            }
        }
    }
    return newPixel;
}
