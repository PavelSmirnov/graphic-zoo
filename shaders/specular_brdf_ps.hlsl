#define EPS 1e-6

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D LightDirections : register(t0);

cbuffer ReflectanceParameters : register(b0) {
    float Color;
    float EyeDotNormal; // dot(directionToEye, surfaceNormal)
    float Roughness;
};

float specular_brdf_ps(in GsOutput input) : SV_Target {
    float3 surfaceNormal = float3(0, 0, 1);
    float3 directionToLight = LightDirections.Sample(AllPointZeroSampler, input.uv).xyz;
    float3 directionToEye = float3(sqrt(1 - EyeDotNormal * EyeDotNormal), 0, EyeDotNormal);
    float3 halfVector;
    if (abs(dot(directionToEye, directionToLight)) < EPS) {
        halfVector = surfaceNormal;
    } else {
        halfVector = normalize(directionToEye + directionToLight);
    }
    float specularPower = MIN_SPECULAR_POWER * pow(MAX_SPECULAR_POWER / MIN_SPECULAR_POWER, 1 - Roughness);
    float fresnelReflectance = Color + (1 - Color) * pow(1 - dot(directionToLight, halfVector), 5); // F, Schlick's approximation
    float microfacetNormalDistribution = pow(max(0, dot(surfaceNormal, halfVector)), specularPower) * (specularPower + 2) / (2 * PI); // D, normalized Blinn-Phong model
    float implicitShadowingAndMasking = 1.0 / max(EPS, dot(halfVector, directionToLight) * dot(halfVector, directionToLight)); // G, Kelemen et. al. approximation of Cook-Torrance geometry factor
    float specularBrdf = fresnelReflectance * (microfacetNormalDistribution * implicitShadowingAndMasking * 0.25);
    float lightAngleCos = dot(surfaceNormal, directionToLight);
    return specularBrdf * max(0, lightAngleCos);
}
