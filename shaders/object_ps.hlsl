struct VsOutput {
    float4 projectedPosition : SV_Position;
    float4 worldSpacePosition : WORLD_SPACE_POSITION;
    float3 cameraSpaceNormal : CAMERA_SPACE_NORMAL;
    float2 uv : UV;
};

struct PsOutput {
    float4 gbuffer0 : SV_Target0; // base color (dielectric diffuse or conductor specular) and dielectric specular color
    float4 gbuffer1 : SV_Target1; // cameraSpaceNormal and isOccupied
    float4 gbuffer2 : SV_Target2; // worldSpacePosition
    float4 gbuffer3 : SV_Target3; // metalness and roughness
};

cbuffer Material : register(b0) {
    float3 BaseColor;
    float DielectricSpecular;
    float Metalness;
    float Roughness;
    float Height;
    int HaveMapFlags; // (BaseColor, DielectricSpecular, Metalness, Roughness, Height)
};

SamplerState AllLinearClampSampler : register(s0);
Texture2D<float3> BaseColorMap         : register(t0);
Texture2D<float> DielectricSpecularMap : register(t1);
Texture2D<float> MetalnessMap          : register(t2);
Texture2D<float> RoughnessMap          : register(t3);
Texture2D<float> HeightMap             : register(t4);

PsOutput object_ps(in VsOutput input) {
    PsOutput output;

    float3 baseColor;
    if (HaveMapFlags & (1 << 0)) {
        baseColor = BaseColorMap.Sample(AllLinearClampSampler, input.uv);
        baseColor = pow(baseColor, SRGB_GAMMA_CORRECTION);
    } else {
        baseColor = BaseColor;
    }

    float dielectricSpecular;
    if (HaveMapFlags & (1 << 1)) {
        dielectricSpecular = DielectricSpecularMap.Sample(AllLinearClampSampler, input.uv);
    } else {
        dielectricSpecular = DielectricSpecular;
    }

    float metalness;
    if (HaveMapFlags & (1 << 2)) {
        metalness = MetalnessMap.Sample(AllLinearClampSampler, input.uv);
    } else {
        metalness = Metalness;
    }

    float roughness;
    if (HaveMapFlags & (1 << 3)) {
        roughness = RoughnessMap.Sample(AllLinearClampSampler, input.uv);
    } else {
        roughness = Roughness;
    }

    output.gbuffer0 = float4(baseColor, dielectricSpecular);
    output.gbuffer1 = float4(normalize(input.cameraSpaceNormal), 1);
    output.gbuffer2 = input.worldSpacePosition;
    output.gbuffer3.xy = float2(metalness, roughness);
    return output;
}
