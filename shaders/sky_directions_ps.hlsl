struct GsOutput {
    float4 position : SV_Position;
    float3 direction : DIRECTION;
};

float4 sky_directions_ps(in GsOutput input) : SV_Target {
    return float4(normalize(input.direction), 0);
}
