cbuffer FocalPoint : register(b0) {
    float2 FocalPointPosition;
    float HorizontalFov;
}

struct Node {
    uint index : NODE_INDEX;
};

uint Undilate(in uint seq) {
    seq = (seq | (seq >> 1u)) & 0x33333333;
    seq = (seq | (seq >> 2u)) & 0x0F0F0F0F;
    seq = (seq | (seq >> 4u)) & 0x00FF00FF;
    seq = (seq | (seq >> 8u)) & 0x0000FFFF;
    return seq;
}

void DecodeNode(in uint node, out uint level, out uint2 position) {
    level = node & 0xF;
    position.x = Undilate((node >> 4u) & 0x05555555);
    position.y = Undilate((node >> 5u) & 0x05555555);
}

float CalcSquaredDistance(in float2 position) {
    float2 diff = position - FocalPointPosition;
    return dot(diff, diff);
}

float CalcLODSquared(in float4 scaledLdru) {
    float tanHalvedHorFovDoubled = 2.0 * tan(HorizontalFov * 0.5);
    float2 center = 0.5 * float2(scaledLdru.x + scaledLdru.z, scaledLdru.y + scaledLdru.w);
    return CalcSquaredDistance(center) * tanHalvedHorFovDoubled * tanHalvedHorFovDoubled;
}

[maxvertexcount(4)]
void quadtree_gs(point Node input[1], inout PointStream<Node> nodeStream) {
    uint node = input[0].index;
    uint level;
    uint2 position;
    DecodeNode(node, level, position);
    float4 ldru = float4(position, float2(position) + float2(1, 1));
    uint2 parentLd = uint2(position.x & 0xFFFFFFFE, position.y & 0xFFFFFFFE);
    float4 parentLdru = float4(parentLd, parentLd + uint2(2, 2));
    float2 scaledFocalPosition = FocalPointPosition * (1u << level);
    float levelSize = 1.0 / (1u << level);
    float parentLevelSize = 2.0 * levelSize;
    float4 scaledLdru = ldru * levelSize;
    float4 scaledParentLdru = parentLdru * levelSize;

    if (CalcLODSquared(scaledLdru) < QUADTREE_PROXIMITY_FACTOR_SQUARED * levelSize * levelSize) {
        if (level < QUADTREE_MAX_LEVEL) {
            // refine the grid
            uint child = ((node & 0xFFFFFFF0) << 2u) | (level + 1);
            Node output[4];
            output[0].index = child | (0 << 4u);
            output[1].index = child | (1 << 4u);
            output[2].index = child | (2 << 4u);
            output[3].index = child | (3 << 4u);
            nodeStream.Append(output[0]);
            nodeStream.Append(output[1]);
            nodeStream.Append(output[2]);
            nodeStream.Append(output[3]);
        } else {
            // can't refine the grid more
            nodeStream.Append(input[0]);
        }
    } else if (level > 0 && CalcLODSquared(scaledParentLdru) > QUADTREE_PROXIMITY_FACTOR_SQUARED * parentLevelSize * parentLevelSize) {
        if (((position.x & 1) == 0) &&
            ((position.y & 1) == 0)) {
            // a larger cell must be emitted
            Node parentNode;
            parentNode.index = ((node >> 2u) & 0xFFFFFFF0) | (level - 1);
            nodeStream.Append(parentNode);
        } else {
            // the cell disappears
        }
    } else {
        nodeStream.Append(input[0]);
    }
}
