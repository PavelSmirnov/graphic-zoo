struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D GBuffer0 : register(t0); // base color (dielectric diffuse or conductor specular) and dielectric specular color
Texture2D GBuffer1 : register(t1); // cameraSpaceNormal and isOccupied
Texture2D GBuffer2 : register(t2); // worldSpacePosition
Texture2D GBuffer3 : register(t3); // metalness and roughness

cbuffer ViewProjectionBuffer : register(b0) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

float4 reflections_ps(in GsOutput input) : SV_Target {
    float4 gbuffer1 = GBuffer1.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer2 = GBuffer2.SampleLevel(AllPointZeroSampler, input.uv, 0);

    bool isOccupied = gbuffer1.w > 0.5;
    if (!isOccupied) {
        return float4(0, 0, 0, 0);
    }

    float3 worldSpacePosition = gbuffer2.xyz;
    float3 eyePosition = mul(InverseView, float4(0, 0, 0, 1)).xyz;
    float3 directionToEye = normalize(eyePosition - worldSpacePosition);
    float3 cameraSpaceNormal = gbuffer1.xyz;
    float3 worldSpaceNormal = mul(InverseView, float4(cameraSpaceNormal, 0)).xyz;
    float3 reflectedDirection = -directionToEye + 2 * dot(directionToEye, worldSpaceNormal) * worldSpaceNormal;
    return float4(reflectedDirection, 0);
}
