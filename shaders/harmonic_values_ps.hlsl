struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

struct PsOutput {
    float4 values[SPHERICAL_HARMONICS_ARRAY_SIZE] : SV_Target;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D Directions : register(t0);

int CalcLegendrePolynomialIndex(int l, int m) {
    return l * (l + 1) / 2 + m;
}

int CalcHarmonicFunctionIndex(int l, int m) {
    return l * l + (m + l);
}

PsOutput harmonic_values_ps(in GsOutput input) {
    float3 direction = Directions.Sample(AllPointZeroSampler, input.uv).xyz;

    float cosTheta = direction.z;
    float sinTheta = sqrt(1 - cosTheta * cosTheta);
    float cosPhi = direction.x / sinTheta;
    float sinPhi = direction.y / sinTheta;
    float phi = atan2(sinPhi, cosPhi);

    const int maxBand = SPHERICAL_HARMONICS_NUM_BANDS - 1;
    int factorial[maxBand * 2 + 1];
    factorial[0] = 1;
    for (int i = 1; i <= maxBand * 2; ++i) {
        factorial[i] = i * factorial[i - 1];
    }

    int doubleFactorial[maxBand + 1];
    doubleFactorial[1] = 1;
    for (int m = 2; m <= maxBand; ++m) {
        doubleFactorial[m] = (2 * m - 1) * doubleFactorial[m - 1];
    }

    float legendrePolynomials[(maxBand + 1) * (maxBand + 2) / 2];
    legendrePolynomials[0] = 1;
    for (int l = 1; l <= maxBand; ++l) {
        for (int m = 0; m <= l; ++m) {
            int i = CalcLegendrePolynomialIndex(l, m);
            if (l == m) {
                legendrePolynomials[i] = ((m % 2 == 0) ? 1 : -1) *
                                         pow(sinTheta, m) *
                                         doubleFactorial[m];
            } else if (l == m + 1) {
                int j = CalcLegendrePolynomialIndex(m, m);
                legendrePolynomials[i] = cosTheta * (2 * m + 1) * legendrePolynomials[j];
            } else {
                int j1 = CalcLegendrePolynomialIndex(l - 1, m);
                int j2 = CalcLegendrePolynomialIndex(l - 2, m);
                legendrePolynomials[i] = (cosTheta * (2 * l - 1) * legendrePolynomials[j1] -
                                          (l + m - 1) * legendrePolynomials[j2]) / (l - m);
            }
        }
    }

    const int numHarmonicFunctions = (maxBand + 1) * (maxBand + 1);
    const int numHarmonicFunctionTuples = (numHarmonicFunctions + 3) / 4;
    float harmonicFunctions[numHarmonicFunctionTuples * 4];
    for (int l = 0; l <= maxBand; ++l) {
        for (int m = -l; m <= l; ++m) {
            int hfIndex = CalcHarmonicFunctionIndex(l, m);
            int lpIndex = CalcLegendrePolynomialIndex(l, abs(m));
            float coefficient = sqrt((2 * l + 1) * 0.25 / PI * factorial[l - abs(m)] / factorial[l + abs(m)]);
            if (m > 0) {
                harmonicFunctions[hfIndex] = sqrt(2.0) * coefficient * cos(m * phi) * legendrePolynomials[lpIndex];
            } else if (m < 0) {
                harmonicFunctions[hfIndex] = sqrt(2.0) * coefficient * sin(-m * phi) * legendrePolynomials[lpIndex];
            } else {
                harmonicFunctions[hfIndex] = coefficient * legendrePolynomials[lpIndex];
            }
        }
    }

    float4 harmonicFunctionTuples[numHarmonicFunctionTuples] = (float4[numHarmonicFunctionTuples])harmonicFunctions;

    PsOutput output;
    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        output.values[i] = harmonicFunctionTuples[i];
    }
    return output;
}
