#define DRAW_LIGHT true
#define DRAW_SHADOW true
#define EPS 1e-6

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

cbuffer LightBuffer : register(b0) {
    float4 LightCameraSpacePosition;
    float4 LightWorldSpacePosition;
    float4 LightColorAndStrength;
    float4x4 LightProjection[NUM_SHADOW_MAPS];
    float4x4 LightView[NUM_SHADOW_MAPS];
    float4x4 LightViewProjection[NUM_SHADOW_MAPS];
    float4 FrustumPartitionSchemeProjectedZData[(NUM_SHADOW_MAPS - 1 + 3) / 4]; // excluding z=0, z=1
    static float FrustumPartitionSchemeProjectedZ[NUM_SHADOW_MAPS - 1] = (float[NUM_SHADOW_MAPS - 1])FrustumPartitionSchemeProjectedZData;
};

cbuffer ViewProjectionBuffer : register(b1) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

SamplerState AllPointZeroSampler : register(s0);
SamplerComparisonState AllPointClampComparisonSampler : register(s1);
SamplerState AllLinearClampSampler : register(s2);
Texture2DArray<float> ShadowMap : register(t0);
Texture2D GBuffer0 : register(t1); // base color (dielectric diffuse or conductor specular) and dielectric specular color
Texture2D GBuffer1 : register(t2); // cameraSpaceNormal and isOccupied
Texture2D GBuffer2 : register(t3); // worldSpacePosition
Texture2D GBuffer3 : register(t4); // metalness and roughness
Texture2D PrecalcedScattering : register(t5);

int GetShadowMapNumber(float projectedZ) {
    for (int i = 0; i < NUM_SHADOW_MAPS - 1; ++i) {
        if (projectedZ < FrustumPartitionSchemeProjectedZ[i]) {
            return i;
        }
    }
    return NUM_SHADOW_MAPS - 1;
}

float3 CalcDirectLightFraction(float3 toLight) {
    float viewPointAngleCos = dot(float3(0, 0, 1), toLight);
    float viewPointAngleSin = sqrt(1 - viewPointAngleCos * viewPointAngleCos);
    float angleCoordinate;
    if (viewPointAngleCos < 0) {
        angleCoordinate = 1 - viewPointAngleSin * 0.5;
    } else {
        angleCoordinate = viewPointAngleSin * 0.5;
    }
    float4 rayIntensityFraction = PrecalcedScattering.Sample(AllLinearClampSampler, float2((0.5 + 0) / SCATTERING_NUM_SPHERES, angleCoordinate));
    return rayIntensityFraction.xyz + rayIntensityFraction.w;
}

float4 light_ps(in GsOutput input) : SV_Target {
    float4 gbuffer0 = GBuffer0.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer1 = GBuffer1.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer2 = GBuffer2.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer3 = GBuffer3.SampleLevel(AllPointZeroSampler, input.uv, 0);

    bool isOccupied = gbuffer1.w > 0.5;
    if (!isOccupied) {
        return float4(0, 0, 0, 0);
    }

    float4 worldSpacePosition = gbuffer2;
    float4 cameraSpacePosition = mul(View, worldSpacePosition);
    float4 cameraProjectedPosition = mul(ViewProjection, worldSpacePosition);
    cameraProjectedPosition /= cameraProjectedPosition.w;

    float litness = 1; // how much not in shadow
    if (DRAW_SHADOW) {
        uint shadowMapWidth, shadowMapHeight, numShadowMapParts;
        ShadowMap.GetDimensions(shadowMapWidth, shadowMapHeight, numShadowMapParts);
        int shadowMapNumber = GetShadowMapNumber(cameraProjectedPosition.z);

        float4 lightProjectedPosition = mul(LightViewProjection[shadowMapNumber], worldSpacePosition);
        float2 shadowUv = (float2(lightProjectedPosition.x, -lightProjectedPosition.y) / lightProjectedPosition.w + float2(1, 1)) * 0.5;
        float2 offset = float2(0.0, 0.0);
        if (USE_PCF_DITHERING) {
            offset = frac(input.position.xy * 0.5) > 0.25;
            offset.y += offset.x;
            if (offset.y > 1.1) {
                offset.y = 0;
            }
        }
        litness = 0.0;
        for (int i = 0; i < PCF_KERNEL_SIZE; i += (USE_PCF_DITHERING ? 2 : 1)) {
            for (int j = 0; j < PCF_KERNEL_SIZE; j += (USE_PCF_DITHERING ? 2 : 1)) {
                float2 shift = float2(i, j) - 0.5 * float2(PCF_KERNEL_SIZE - 1, PCF_KERNEL_SIZE - 1);
                float2 pixelOffset = offset + shift;
                float2 uvOffset = float2(pixelOffset.x / shadowMapWidth, pixelOffset.y / shadowMapHeight);
                litness += ShadowMap.SampleCmp(AllPointClampComparisonSampler,
                                               float3(shadowUv + uvOffset, shadowMapNumber),
                                               lightProjectedPosition.z / lightProjectedPosition.w);
            }
        }
        litness /= PCF_KERNEL_SIZE * PCF_KERNEL_SIZE * (USE_PCF_DITHERING ? 0.25 : 1.0);
    }

    float3 outcomingLightIntensity = float3(0, 0, 0);
    if (DRAW_LIGHT) {
        float3 cameraSpaceNormal = gbuffer1.xyz;
        float3 surfaceNormal = normalize(cameraSpaceNormal);
        float3 directionToLight = normalize(LightCameraSpacePosition.xyz);
        float3 directionToEye = -normalize(cameraSpacePosition.xyz);
        float3 halfVector = normalize(directionToEye + directionToLight);
        float lightAngleCos = dot(surfaceNormal, directionToLight);

        float3 lightColorAndStrength = CalcDirectLightFraction(normalize(LightWorldSpacePosition)) * (LightColorAndStrength.xyz *  LightColorAndStrength.w);

        float metalness = gbuffer3.x;
        float3 baseColor = gbuffer0.xyz;
        float3 diffuseColor = (1 - metalness) * baseColor;
        float dielectricSpecular = gbuffer0.w;
        float3 specularColor = metalness * baseColor + (1 - metalness) * dielectricSpecular * float3(1, 1, 1);
        float roughness = gbuffer3.y;
        float specularPower = MIN_SPECULAR_POWER * pow(float(MAX_SPECULAR_POWER) / MIN_SPECULAR_POWER, 1 - roughness);

        float3 fresnelReflectance = specularColor + (float3(1, 1, 1) - specularColor) * pow(1 - dot(directionToLight, halfVector), 5); // F, Schlick's approximation
        float microfacetNormalDistribution = pow(max(0, dot(surfaceNormal, halfVector)), specularPower) * (specularPower + 2) / (2 * PI); // D, normalized Blinn-Phong model
        float implicitShadowingAndMasking = 0.25 / max(EPS, dot(halfVector, directionToLight) * dot(halfVector, directionToLight)); // G, Kelemen et. al. approximation of Cook-Torrance geometry factor
        float3 specularBrdf = fresnelReflectance * (microfacetNormalDistribution * implicitShadowingAndMasking);

        float3 fresnelReflectanceDiffuse = specularColor + (float3(1, 1, 1) - specularColor) * pow(1 - dot(directionToLight, surfaceNormal), 5);
        float3 diffuseBrdf = diffuseColor * (1 - fresnelReflectanceDiffuse) / PI;

        float3 brdf = specularBrdf + diffuseBrdf;
        float3 incomingLightIntensity = lightColorAndStrength * litness;
        outcomingLightIntensity = PI * brdf * incomingLightIntensity * max(0, lightAngleCos);
    }
    return float4(outcomingLightIntensity, 1);
}
