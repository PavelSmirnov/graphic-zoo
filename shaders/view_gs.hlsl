struct VsOutput {};

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

[maxvertexcount(6)]
void view_gs(point VsOutput input[1], inout TriangleStream<GsOutput> triangleStream) {
    GsOutput ld;
    ld.position = float4(-1, -1, 0, 1);
    ld.uv = float2(0, 1);

    GsOutput lu;
    lu.position = float4(-1, +1, 0, 1);
    lu.uv = float2(0, 0);

    GsOutput rd;
    rd.position = float4(+1, -1, 0, 1);
    rd.uv = float2(1, 1);

    GsOutput ru;
    ru.position = float4(+1, +1, 0, 1);
    ru.uv = float2(1, 0);

    triangleStream.Append(ld);
    triangleStream.Append(lu);
    triangleStream.Append(ru);
    triangleStream.RestartStrip();

    triangleStream.Append(ld);
    triangleStream.Append(ru);
    triangleStream.Append(rd);
    triangleStream.RestartStrip();
}
