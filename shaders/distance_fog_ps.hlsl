#define DRAW_DISTANCE_FOG true

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

cbuffer LightBuffer : register(b0) {
    float4 LightCameraSpacePosition;
    float4 LightWorldSpacePosition;
    float4 LightColorAndStrength;
    float4x4 LightProjection[NUM_SHADOW_MAPS];
    float4x4 LightView[NUM_SHADOW_MAPS];
    float4x4 LightViewProjection[NUM_SHADOW_MAPS];
    float4 FrustumPartitionSchemeProjectedZData[(NUM_SHADOW_MAPS - 1 + 3) / 4]; // excluding z=0, z=1
    static float FrustumPartitionSchemeProjectedZ[NUM_SHADOW_MAPS - 1] = (float[NUM_SHADOW_MAPS - 1])FrustumPartitionSchemeProjectedZData;
};

cbuffer ViewProjectionBuffer : register(b1) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

cbuffer FogBuffer : register(b2) {
    float3 FogColor;
    float VolumetricFogDensity;
    float VolumetricFogHeight;
    float DistanceFogStart;
    float DistanceFogDistance;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D GBuffer0 : register(t0); // base color (dielectric diffuse or conductor specular) and dielectric specular color
Texture2D GBuffer1 : register(t1); // cameraSpaceNormal and isOccupied
Texture2D GBuffer2 : register(t2); // worldSpacePosition
Texture2D Sky : register(t3);

float4 distance_fog_ps(in GsOutput input) : SV_Target {
    float4 gbuffer1 = GBuffer1.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer2 = GBuffer2.SampleLevel(AllPointZeroSampler, input.uv, 0);

    float3 skyColor = Sky.SampleLevel(AllPointZeroSampler, input.uv, 0).xyz;

    bool isOccupied = gbuffer1.w > 0.5;
    if (!isOccupied) {
        return float4(skyColor, 1);
    }

    float4 worldSpacePosition = gbuffer2;
    float4 cameraSpacePosition = mul(View, worldSpacePosition);

    float fogDensity = 0;
    if (DRAW_DISTANCE_FOG) {
        float dist = sqrt(dot(cameraSpacePosition.xyz, cameraSpacePosition.xyz)) / DistanceFogDistance;
        if (dist > DistanceFogStart) {
            float x = min((dist - DistanceFogStart) / (1 - DistanceFogStart), 1);
            fogDensity = -2 * x * x * x + 3 * x * x;
        }
    }

    return float4(skyColor, fogDensity);
}
