#define DRAW_SKY true

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

cbuffer LightBuffer : register(b0) {
    float4 LightCameraSpacePosition;
    float4 LightWorldSpacePosition;
    float4 LightColorAndStrength;
    float4x4 LightProjection[NUM_SHADOW_MAPS];
    float4x4 LightView[NUM_SHADOW_MAPS];
    float4x4 LightViewProjection[NUM_SHADOW_MAPS];
    float4 FrustumPartitionSchemeProjectedZData[(NUM_SHADOW_MAPS - 1 + 3) / 4]; // excluding z=0, z=1
    static float FrustumPartitionSchemeProjectedZ[NUM_SHADOW_MAPS - 1] = (float[NUM_SHADOW_MAPS - 1])FrustumPartitionSchemeProjectedZData;
};

SamplerState AllLinearClampSampler : register(s0);
SamplerState AllPointZeroSampler : register(s1);
Texture2D PrecalcedScattering : register(t0);
Texture2D Directions : register(t1);
Texture2D NightSky : register(t2);

float CalcAirDensity(float height) {
    return exp(-height / AIR_AVERAGE_HEIGHT);
}

float CalcAerosolesDensity(float height) {
    return exp(-height / AEROSOLES_AVERAGE_HEIGHT);
}

float CalcSphereHeight(int sphereId) {
    if (sphereId == SCATTERING_NUM_SPHERES - 1) {
        return ATMOSPHERE_MAX_HEIGHT;
    }
    return -AIR_AVERAGE_HEIGHT * log(1 - float(sphereId) / (SCATTERING_NUM_SPHERES - 1));
}

float CalcAerosolesPhaseFunction(float phaseCos) {
    float u = AEROSOLES_SCATTERING_U;
    float u2 = u * u;
    float u3 = u2 * u;
    float u4 = u2 * u2;
    float x = u * 5 / 9 + u3 * 125 / 729 + sqrt(64.0 / 27 - u2 * 325 / 243 + u4 * 1250 / 2187);
    float x_cubicroot = pow(x, 1.0 / 3);
    float g = u * 5 / 9 - (4.0 / 3 - u2 * 25 / 81) / x_cubicroot + x_cubicroot;
    float g2 = g * g;
    float term = sqrt(1 + g2 - 2 * g * phaseCos);
    return 3 * (1 - g2) * (1 + phaseCos * phaseCos) / (2 * (2 + g2) * term * term * term);
}

float CalcAirPhaseFunction(float phaseCos) {
    return (1 + phaseCos * phaseCos) * 0.75;
}

float3 CalcDirectLightFraction(float3 toLight) {
    float viewPointAngleCos = dot(float3(0, 0, 1), toLight);
    float viewPointAngleSin = sqrt(1 - viewPointAngleCos * viewPointAngleCos);
    float angleCoordinate;
    if (viewPointAngleCos < 0) {
        angleCoordinate = 1 - viewPointAngleSin * 0.5;
    } else {
        angleCoordinate = viewPointAngleSin * 0.5;
    }
    float4 rayIntensityFraction = PrecalcedScattering.Sample(AllLinearClampSampler, float2((0.5 + 0) / SCATTERING_NUM_SPHERES, angleCoordinate));
    return rayIntensityFraction.xyz + rayIntensityFraction.w;
}

float4 sky_ps(in GsOutput input) : SV_Target {
    float3 skyColor = float3(0, 0, 0);
    if (DRAW_SKY) {
        float3 direction = Directions.Sample(AllPointZeroSampler, input.uv).xyz;
        float3 toLight = normalize(LightWorldSpacePosition.xyz);

        float3 beamEnd = float3(0, 0, EARTH_RADIUS);
        float3 airIntensityFraction = float3(0, 0, 0);
        float aerosolesIntensityFraction = 0;
        float airOpticalDepth = 0;
        float aerosolesOpticalDepth = 0;
        float prevPassedDistance = 0;
        float prevAirDensity = CalcAirDensity(0);
        float prevAerosolesDensity = CalcAerosolesDensity(0);
        float3 airScatteringBeta = float3(AIR_SCATTERING_BETA_RED, AIR_SCATTERING_BETA_GREEN, AIR_SCATTERING_BETA_BLUE);
        for (int i = 1; i < SCATTERING_NUM_SPHERES; ++i) {
            float height = CalcSphereHeight(i);
            float nextPassedDistance = sqrt(EARTH_RADIUS * EARTH_RADIUS * direction.z * direction.z + (2 * EARTH_RADIUS + height) * height) - EARTH_RADIUS * direction.z;
            float stepLength = nextPassedDistance - prevPassedDistance;
            float nextAirDensity = CalcAirDensity(height);
            float nextAerosolesDensity = CalcAerosolesDensity(height);
            airOpticalDepth += 0.5 * (prevAirDensity + nextAirDensity) * stepLength;
            aerosolesOpticalDepth += 0.5 * (prevAerosolesDensity + nextAerosolesDensity) * stepLength;
            float3 midPoint = beamEnd + direction * nextPassedDistance;
            float midPointAngleCos = dot(normalize(midPoint), toLight);
            float midPointAngleSin = sqrt(1 - midPointAngleCos * midPointAngleCos);
            float angleCoordinate;
            if (midPointAngleCos < 0) {
                angleCoordinate = 1 - midPointAngleSin * 0.5;
            } else {
                angleCoordinate = midPointAngleSin * 0.5;
            }
            float4 incidentRayIntensityFraction = PrecalcedScattering.Sample(AllLinearClampSampler, float2((0.5 + i) / SCATTERING_NUM_SPHERES, angleCoordinate));
            airIntensityFraction += stepLength * nextAirDensity * exp(-airOpticalDepth * airScatteringBeta) * incidentRayIntensityFraction.xyz;
            aerosolesIntensityFraction += stepLength * nextAerosolesDensity * exp(-aerosolesOpticalDepth * AEROSOLES_SCATTERING_BETA) * incidentRayIntensityFraction.w;
            prevAirDensity = nextAirDensity;
            prevAerosolesDensity = nextAerosolesDensity;
            prevPassedDistance = nextPassedDistance;
        }

        float phaseCos = dot(toLight, direction);
        airIntensityFraction *= airScatteringBeta * CalcAirPhaseFunction(phaseCos) * 0.25 / PI;
        aerosolesIntensityFraction *= AEROSOLES_SCATTERING_BETA * CalcAerosolesPhaseFunction(phaseCos) * 0.25 / PI;
        skyColor = LightColorAndStrength.xyz * LightColorAndStrength.w * (airIntensityFraction + aerosolesIntensityFraction);

        if (direction.z > 0) {
            float2 skyCoordinates;
            if (direction.z < 0.5) {
                skyCoordinates = float2(direction.xy * (1 - direction.z) + 1) * 0.5;
            } else {
                skyCoordinates = float2(direction.xy + 1) * 0.5;
            }
            skyColor += NightSky.Sample(AllLinearClampSampler, skyCoordinates) * NIGHT_SKY_DIM;
        }
    }
    return float4(skyColor, 0);
}
