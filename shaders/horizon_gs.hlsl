struct VsOutput {};

struct GsOutput {
    float4 position : SV_Position;
    float3 direction : DIRECTION;
};

float3 Rotate(float3 vec, int times) {
    float angle = times * 2 * PI / 3;
    float cosAngle = cos(angle);
    float sinAngle = sin(angle);
    float3x3 rotMat = float3x3(cosAngle, -sinAngle, 0,
                               sinAngle, cosAngle, 0,
                               0, 0, 1);
    return mul(rotMat, vec);
}

[maxvertexcount(18)]
void horizon_gs(point VsOutput input[1], inout TriangleStream<GsOutput> triangleStream) {
    for (int i = 0; i < 3; ++i) {
        float xmin = -1 + 2.0 * i / 3;
        float xmax = -1 + 2.0 * (i + 1) / 3;

        GsOutput ld;
        ld.position = float4(xmin, -1, 0, 1);
        ld.direction = Rotate(float3(1, 0, -HORIZON_WIDTH), i);

        GsOutput lu;
        lu.position = float4(xmin, +1, 0, 1);
        lu.direction = Rotate(float3(1, 0, HORIZON_WIDTH), i);

        GsOutput rd;
        rd.position = float4(xmax, -1, 0, 1);
        rd.direction = Rotate(float3(1, 0, -HORIZON_WIDTH), i + 1);

        GsOutput ru;
        ru.position = float4(xmax, +1, 0, 1);
        ru.direction = Rotate(float3(1, 0, HORIZON_WIDTH), i + 1);

        triangleStream.Append(ld);
        triangleStream.Append(lu);
        triangleStream.Append(ru);
        triangleStream.RestartStrip();

        triangleStream.Append(ld);
        triangleStream.Append(ru);
        triangleStream.Append(rd);
        triangleStream.RestartStrip();
    }
}
