struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D Texture : register(t0);

cbuffer HdrBuffer : register(b0) {
    float AverageLuma;
};

float4 hdr_ps(in GsOutput input) : SV_Target {
    float4 color = Texture.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float luma = dot(color.xyz, float3(0.2126, 0.7152, 0.0722));
    float gray18 = 0.18 * AverageLuma;
    float logLuma = clamp(log2(luma / gray18), -10, 10);
    float coeff = 1.0 / (1.0 + exp(-logLuma));
    coeff = 1;
    color.xyz = color.xyz * coeff; // tone mapping
    color.xyz = pow(color.xyz, 1.0 / GAMMA_CORRECTION_GAMMA);
    return color;
}
