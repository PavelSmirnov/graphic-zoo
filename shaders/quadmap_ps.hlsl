#define DRAW_ALBEDO true
#define DRAW_DETAILS true
#define DRAW_MESH false
#define DETAIL_TEXTURE_UV_SIZE 0.0005
#define DETAIL_MIN_STRENGTH 0.5
#define DETAIL_MAX_STRENGTH 0.9

struct GsOutput {
    float4 projectedPosition : SV_Position;
    float4 worldSpacePosition : WORLD_SPACE_POSITION;
    uint level : LEVEL;
    float4 cameraSpacePosition : CAMERA_SPACE_POSITION;
    float2 uv : UV;
    float3 sideCode : SIDE_CODE;
};

struct PsOutput {
    float4 gbuffer0 : SV_Target0; // base color (dielectric diffuse or conductor specular) and dielectric specular color
    float4 gbuffer1 : SV_Target1; // cameraSpaceNormal and isOccupied
    float4 gbuffer2 : SV_Target2; // worldSpacePosition
    float4 gbuffer3 : SV_Target3; // metalness and roughness
};

cbuffer ViewProjectionBuffer : register(b0) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

SamplerState AllLinearClampSampler : register(s0);
Texture2D NormalTexture : register(t0);
Texture2D AlbedoTexture : register(t1);
Texture2D DetailTexture : register(t2);

PsOutput quadmap_ps(in GsOutput input) {
    PsOutput output;

    output.gbuffer2 = input.worldSpacePosition;

    float3 color = float3(1, 1, 1);
    if (DRAW_ALBEDO) {
        color = AlbedoTexture.Sample(AllLinearClampSampler, input.uv).xyz;
        color = pow(color, SRGB_GAMMA_CORRECTION);
    }
    if (DRAW_DETAILS) {
        int2 tileCoordinates = int2(input.uv / (DETAIL_TEXTURE_UV_SIZE * 2));
        float2 fractionalTileCoordinates = input.uv / (DETAIL_TEXTURE_UV_SIZE * 2);
        float2 localUv = (fractionalTileCoordinates - tileCoordinates) * 2;
        if (localUv.x > 1) {
            localUv.x = 2 - localUv.x;
        }
        if (localUv.y > 1) {
            localUv.y = 2 - localUv.y;
        }
        float detail = DetailTexture.Sample(AllLinearClampSampler, localUv).x;
        float detailStrength = DETAIL_MIN_STRENGTH + (DETAIL_MAX_STRENGTH - DETAIL_MIN_STRENGTH) * (1 - (color.x + color.y + color.z) / 3);
        color *= (1 - detailStrength * (1 - detail));
    }
    if (DRAW_MESH) {
        float sideBorderValue = 0.03;
        float howCloseToSide = max(input.sideCode.x, max(input.sideCode.y, input.sideCode.z));
        float sideBorderNormalized = max(0, howCloseToSide - (1.0 - sideBorderValue)) / sideBorderValue;
        color = sideBorderNormalized * float3(1.0, 0.0, 0.0) +
                (1.0 - sideBorderNormalized) * color;
    }
    float specularColor = 0.04; // TODO: read from map
    output.gbuffer0 = float4(color, specularColor);

    float3 packedNormal = NormalTexture.Sample(AllLinearClampSampler, input.uv).xyz;
    float3 normal = packedNormal - float3(0.5, 0.5, 0.5);
    float3 cameraSpaceNormal = mul(View, float4(normal, 0)).xyz;
    float3 surfaceNormal = normalize(cameraSpaceNormal);
    output.gbuffer1 = float4(surfaceNormal, 1);

     // TODO: read from map
    float metalness = 0;
    float roughness = 1;
    output.gbuffer3.xy = float2(metalness, roughness);

    return output;
}
