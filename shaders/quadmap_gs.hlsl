#define BORDER_COMPARISON_EPS 1e-6

struct DsOutput {
    float4 worldSpacePosition : WORLD_SPACE_POSITION;
    uint level : LEVEL;
    float2 uv : UV;
};

struct GsOutput {
    float4 projectedPosition : SV_Position;
    float4 worldSpacePosition : WORLD_SPACE_POSITION;
    uint level : LEVEL;
    float4 cameraSpacePosition : CAMERA_SPACE_POSITION;
    float2 uv : UV;
    float3 sideCode : SIDE_CODE;
};

cbuffer ViewProjectionBuffer : register(b0) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

bool IsEdgeOnSide(float2 startUv, float2 endUv) {
    return startUv.x < BORDER_COMPARISON_EPS && endUv.x < BORDER_COMPARISON_EPS ||
           startUv.x > 1 - BORDER_COMPARISON_EPS && endUv.x > 1 - BORDER_COMPARISON_EPS ||
           startUv.y < BORDER_COMPARISON_EPS && endUv.y < BORDER_COMPARISON_EPS ||
           startUv.y > 1 - BORDER_COMPARISON_EPS && endUv.y > 1 - BORDER_COMPARISON_EPS;
}

void CalcSideTriangles(GsOutput start, GsOutput end, inout TriangleStream<GsOutput> output) {
    GsOutput side[4];
    side[0].worldSpacePosition = end.worldSpacePosition;
    side[0].cameraSpacePosition = end.cameraSpacePosition;
    side[0].projectedPosition = end.projectedPosition;
    side[0].level = end.level;
    side[0].uv = end.uv;
    side[0].sideCode = float3(1, 1, 0);

    side[1].worldSpacePosition = start.worldSpacePosition;
    side[1].cameraSpacePosition = start.cameraSpacePosition;
    side[1].projectedPosition = start.projectedPosition;
    side[1].level = start.level;
    side[1].uv = start.uv;
    side[1].sideCode = float3(1, 0, 1);

    side[2].worldSpacePosition = float4(start.worldSpacePosition.xy, 0, start.worldSpacePosition.w);
    side[2].cameraSpacePosition = mul(View, side[2].worldSpacePosition);
    side[2].projectedPosition = mul(Projection, side[2].cameraSpacePosition);
    side[2].level = start.level;
    side[2].uv = start.uv;
    side[2].sideCode = float3(0, 1, 1);

    side[3].worldSpacePosition = float4(end.worldSpacePosition.xy, 0, end.worldSpacePosition.w);
    side[3].cameraSpacePosition = mul(View, side[3].worldSpacePosition);
    side[3].projectedPosition = mul(Projection, side[3].cameraSpacePosition);
    side[3].level = end.level;
    side[3].uv = end.uv;
    side[3].sideCode = float3(1, 0, 1);

    output.Append(side[0]);
    output.Append(side[1]);
    output.Append(side[2]);
    output.RestartStrip();
    output.Append(side[2]);
    output.Append(side[3]);
    output.Append(side[0]);
    output.RestartStrip();
}

[maxvertexcount(15)]
void quadmap_gs(triangle DsOutput input[3], inout TriangleStream<GsOutput> output) {
    GsOutput vs[3];
    for (int i = 0; i < 3; i++) {
        vs[i].worldSpacePosition = input[i].worldSpacePosition;
        vs[i].cameraSpacePosition = mul(View, vs[i].worldSpacePosition);
        vs[i].projectedPosition = mul(Projection, vs[i].cameraSpacePosition);
        vs[i].level = input[i].level;
        vs[i].uv = input[i].uv;
    }
    vs[0].sideCode = float3(1, 1, 0);
    vs[1].sideCode = float3(1, 0, 1);
    vs[2].sideCode = float3(0, 1, 1);
    output.Append(vs[0]);
    output.Append(vs[1]);
    output.Append(vs[2]);
    output.RestartStrip();

    if (IsEdgeOnSide(vs[0].uv, vs[1].uv)) {
        CalcSideTriangles(vs[0], vs[1], output);
    }
    if (IsEdgeOnSide(vs[1].uv, vs[2].uv)) {
        CalcSideTriangles(vs[1], vs[2], output);
    }
    if (IsEdgeOnSide(vs[2].uv, vs[0].uv)) {
        CalcSideTriangles(vs[2], vs[0], output);
    }
}
