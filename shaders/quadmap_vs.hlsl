struct VsInput {
    uint index : NODE_INDEX;
};

struct VsPatchData {
    float4 lrdu : LRDU;
    uint level : LEVEL;
};

uint Undilate(in uint seq) {
    seq = (seq | (seq >> 1u)) & 0x33333333;
    seq = (seq | (seq >> 2u)) & 0x0F0F0F0F;
    seq = (seq | (seq >> 4u)) & 0x00FF00FF;
    seq = (seq | (seq >> 8u)) & 0x0000FFFF;
    return seq;
}

void DecodeNode(in uint node, out uint level, out uint2 position) {
    level = node & 0xF;
    position.x = Undilate((node >> 4u) & 0x05555555);
    position.y = Undilate((node >> 5u) & 0x05555555);
}

void quadmap_vs(in VsInput input, out VsPatchData output) {
    uint node = input.index;
    uint2 position;
    DecodeNode(node, output.level, position);
    output.lrdu = float4(position, position + uint2(1, 1)).xzyw;
    output.lrdu /= float(1u << output.level);
}
