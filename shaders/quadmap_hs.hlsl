cbuffer FocalPoint : register(b0) {
    float2 FocalPointPosition;
    float HorizontalFov;
}

struct VsPatchData {
    float4 lrdu : LRDU;
    uint level : LEVEL;
};

struct HsControlPoint {};

struct HsPatchConstantData {
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;

    uint level : LEVEL;
    float4 lrdu : LRDU;
};

float CalcSquaredDistance(in float2 position) {
    float2 diff = position - FocalPointPosition;
    return dot(diff, diff);
}

float CalcLODSquared(in float2 edgeCenter) {
    float tanHalvedHorFovDoubled = 2.0 * tan(HorizontalFov * 0.5);
    return CalcSquaredDistance(edgeCenter) * tanHalvedHorFovDoubled * tanHalvedHorFovDoubled;
}

int ShouldTessellateEdge(float2 edgeCenter, float levelSize) {
    return levelSize * levelSize * QUADTREE_PROXIMITY_FACTOR_SQUARED * 2.0 > CalcLODSquared(edgeCenter);
}

HsPatchConstantData GeneratePatchConstantData(InputPatch<VsPatchData, 1> input) {
    float levelSize = 1.0 / (1u << input[0].level);
    HsPatchConstantData output;
    output.level = input[0].level;
    output.lrdu = input[0].lrdu;
    output.inside[0] = 0;
    output.inside[1] = 0;

    float2 quadCenter = 0.5 * (output.lrdu.xz + output.lrdu.yw);
    output.edges[0] = 1 + ShouldTessellateEdge(float2(output.lrdu.x, quadCenter.y), levelSize);
    output.edges[1] = 1 + ShouldTessellateEdge(float2(quadCenter.x, output.lrdu.z), levelSize);
    output.edges[2] = 1 + ShouldTessellateEdge(float2(output.lrdu.y, quadCenter.y), levelSize);
    output.edges[3] = 1 + ShouldTessellateEdge(float2(quadCenter.x, output.lrdu.w), levelSize);

    return output;
}

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_ccw")]
[outputcontrolpoints(1)]
[patchconstantfunc("GeneratePatchConstantData")]
HsControlPoint quadmap_hs(InputPatch<VsPatchData, 1> input) {
    HsControlPoint output;
    return output;
}
