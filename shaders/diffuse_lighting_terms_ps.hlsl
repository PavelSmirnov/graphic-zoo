struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D Directions : register(t0);

float diffuse_lighting_terms_ps(in GsOutput input) : SV_Target {
    // TODO: smooth the derivative
    float3 direction = Directions.Sample(AllPointZeroSampler, input.uv).xyz;
    return max(0, direction.z);
}
