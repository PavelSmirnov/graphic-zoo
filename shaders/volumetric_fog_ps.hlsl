#define DRAW_VOLUMETRIC_FOG true
#define HEIGHT_EPS 1e-6

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

cbuffer LightBuffer : register(b0) {
    float4 LightCameraSpacePosition;
    float4 LightWorldSpacePosition;
    float4 LightColorAndStrength;
    float4x4 LightProjection[NUM_SHADOW_MAPS];
    float4x4 LightView[NUM_SHADOW_MAPS];
    float4x4 LightViewProjection[NUM_SHADOW_MAPS];
    float4 FrustumPartitionSchemeProjectedZData[(NUM_SHADOW_MAPS - 1 + 3) / 4]; // excluding z=0, z=1
    static float FrustumPartitionSchemeProjectedZ[NUM_SHADOW_MAPS - 1] = (float[NUM_SHADOW_MAPS - 1])FrustumPartitionSchemeProjectedZData;
};

cbuffer ViewProjectionBuffer : register(b1) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

cbuffer FogBuffer : register(b2) {
    float3 FogColor;
    float VolumetricFogDensity;
    float VolumetricFogHeight;
    float DistanceFogStart;
    float DistanceFogDistance;
};

SamplerState AllPointZeroSampler : register(s0);
SamplerState AllLinearClampSampler : register(s1);
Texture2D GBuffer0 : register(t0); // base color (dielectric diffuse or conductor specular) and dielectric specular color
Texture2D GBuffer1 : register(t1); // cameraSpaceNormal and isOccupied
Texture2D GBuffer2 : register(t2); // worldSpacePosition
Texture2D PrecalcedScattering : register(t3);

float3 CalcDirectLightFraction(float3 toLight) {
    float viewPointAngleCos = dot(float3(0, 0, 1), toLight);
    float viewPointAngleSin = sqrt(1 - viewPointAngleCos * viewPointAngleCos);
    float angleCoordinate;
    if (viewPointAngleCos < 0) {
        angleCoordinate = 1 - viewPointAngleSin * 0.5;
    } else {
        angleCoordinate = viewPointAngleSin * 0.5;
    }
    float4 rayIntensityFraction = PrecalcedScattering.Sample(AllLinearClampSampler, float2((0.5 + 0) / SCATTERING_NUM_SPHERES, angleCoordinate));
    return rayIntensityFraction.xyz + rayIntensityFraction.w;
}

float4 volumetric_fog_ps(in GsOutput input) : SV_Target {
    float4 gbuffer1 = GBuffer1.SampleLevel(AllPointZeroSampler, input.uv, 0);
    float4 gbuffer2 = GBuffer2.SampleLevel(AllPointZeroSampler, input.uv, 0);

    bool isOccupied = gbuffer1.w > 0.5;
    if (!isOccupied) {
        return float4(1, 1, 1, 0);
    }

    float4 worldSpacePosition = gbuffer2;
    float4 cameraSpacePosition = mul(View, worldSpacePosition);

    float invFogDensity = 1;
    if (DRAW_VOLUMETRIC_FOG) {
        float4 eye = mul(InverseView, float4(0, 0, 0, 1));
        float heightJump = worldSpacePosition.z - eye.z;
        float slopeFactor = 1;
        if (heightJump > HEIGHT_EPS) {
            slopeFactor = (1 - exp(-heightJump / VolumetricFogHeight)) / (heightJump / VolumetricFogHeight);
        }
        invFogDensity = exp(-VolumetricFogDensity * exp(-eye.z / VolumetricFogHeight) * length(cameraSpacePosition) * slopeFactor);
    }

    return float4(FogColor, 1 - invFogDensity);
}
