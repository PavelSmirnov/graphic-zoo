struct HsPatchConstantData {
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;

    uint level : LEVEL;
    float4 lrdu : LRDU;
};

struct HsControlPoint {};

struct DsOutput {
    float4 worldSpacePosition : WORLD_SPACE_POSITION;
    uint level : LEVEL;
    float2 uv : UV;
};

cbuffer TerrainPlacementBuffer : register(b0) {
    float4 TerrainLrdu;
    float TerrainHeight;
    float3 TerrainReserved;
};

SamplerState Sampler : register(s0);
Texture2D<float> HeightTexture : register(t0);

[domain("quad")]
DsOutput quadmap_ds(HsPatchConstantData input,
                    float2 patchUv : SV_DomainLocation,
                    OutputPatch<HsControlPoint, 1> hsControlPoints) {
    DsOutput result;
    float2 rawUv = input.lrdu.xz + float2(patchUv.x * (input.lrdu.y - input.lrdu.x),
                                          patchUv.y * (input.lrdu.w - input.lrdu.z));
    result.uv = float2(rawUv.x, 1 - rawUv.y);
    float height = HeightTexture.SampleLevel(Sampler, result.uv, 1);
    float2 xy = float2(TerrainLrdu.x + rawUv.x * (TerrainLrdu.y - TerrainLrdu.x),
                       TerrainLrdu.z + rawUv.y * (TerrainLrdu.w - TerrainLrdu.z));
    result.worldSpacePosition = float4(xy, height * TerrainHeight, 1);
    result.level = input.level;
    return result;
}
