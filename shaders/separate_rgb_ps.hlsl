struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D Texture : register(t0);

struct PsOutput {
    float red : SV_Target0;
    float green : SV_Target1;
    float blue : SV_Target2;
};

PsOutput separate_rgb_ps(in GsOutput input) {
    float4 color = Texture.SampleLevel(AllPointZeroSampler, input.uv, 0);

    PsOutput output;
    output.red = color.r;
    output.green = color.g;
    output.blue = color.b;
    return output;
}
