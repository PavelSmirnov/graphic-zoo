struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
    uint arrayIndex : SV_RenderTargetArrayIndex;
};

struct PsOutput {
    float4 values : SV_Target;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2DArray HarmonicFunctions : register(t0);
Texture2D<float> FunctionToHarmonize : register(t1);

PsOutput multiply_by_harmonics_ps(in GsOutput input) {
    float value = FunctionToHarmonize.SampleLevel(AllPointZeroSampler, input.uv, 0);
    PsOutput output;
    output.values = value * HarmonicFunctions.Sample(AllPointZeroSampler, float3(input.uv, input.arrayIndex));
    return output;
}
