struct VsInput {
    float3 modelSpacePosition : MODEL_SPACE_POSITION;
    float2 textureUv : TEXTURE_UV;
    float3 modelSpaceNormal : MODEL_SPACE_NORMAL;
};

struct VsOutput {
    float4 projectedPosition : SV_Position;
    float4 worldSpacePosition : WORLD_SPACE_POSITION;
    float3 cameraSpaceNormal : CAMERA_SPACE_NORMAL;
    float2 uv : UV;
};

cbuffer ViewProjectionBuffer : register(b0) {
    float4x4 Projection;
    float4x4 View;
    float4x4 ViewProjection;
    float4x4 InverseView;
    float4x4 InverseProjection;
};

cbuffer ObjectTransformationBuffer : register(b1) {
    float4x4 Transformation;
};

VsOutput object_vs(in VsInput input) {
    VsOutput output;
    output.worldSpacePosition = mul(Transformation, float4(input.modelSpacePosition, 1));
    output.projectedPosition = mul(ViewProjection, output.worldSpacePosition);
    output.cameraSpaceNormal = mul(View, mul(Transformation, float4(input.modelSpaceNormal, 0))).xyz;
    output.uv = input.textureUv;
    return output;
}
