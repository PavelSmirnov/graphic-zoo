struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

float CalcAirDensity(float height) {
    return exp(-height / AIR_AVERAGE_HEIGHT);
}

float CalcAerosolesDensity(float height) {
    return exp(-height / AEROSOLES_AVERAGE_HEIGHT);
}

float CalcSphereHeight(int sphereId) {
    if (sphereId == SCATTERING_NUM_SPHERES - 1) {
        return ATMOSPHERE_MAX_HEIGHT;
    }
    return -AIR_AVERAGE_HEIGHT * log(1 - float(sphereId) / (SCATTERING_NUM_SPHERES - 1));
}

float4 precalc_scattering_ps(in GsOutput input) : SV_Target {
    float angleCoordinate = input.uv.y;
    float sinAngle;
    float cosAngle;
    if (angleCoordinate > 0.5) {
        sinAngle = (1 - angleCoordinate) * 2;
        cosAngle = -sqrt(1 - sinAngle * sinAngle);
    } else {
        sinAngle = angleCoordinate * 2;
        cosAngle = sqrt(1 - sinAngle * sinAngle);
    }
    int sphereId = int(input.uv.x * SCATTERING_NUM_SPHERES);
    float sphereRadius = EARTH_RADIUS + CalcSphereHeight(sphereId);
    float2 beamEnd = float2(cosAngle, sinAngle) * sphereRadius;

    if (beamEnd.y < EARTH_RADIUS && beamEnd.x < 0) {
        // in shadow
        return float4(0, 0, 0, 0);
    }

    float airOpticalDepth = 0;
    float aerosolesOpticalDepth = 0;
    float prevSphereIntersectionLength = 0;
    float prevAirDensity = CalcAirDensity(0);
    float prevAerosolesDensity = CalcAerosolesDensity(0);
    for (int i = 1; i < SCATTERING_NUM_SPHERES; ++i) {
        float height = CalcSphereHeight(i);
        float currentRadius = EARTH_RADIUS + height;
        float nextAirDensity = CalcAirDensity(height);
        float nextAerosolesDensity = CalcAerosolesDensity(height);
        float nextSphereIntersectionLength = 0;
        if (currentRadius > beamEnd.y) {
            // the sphere is hit by the ray
            float firstIntersectionX = sqrt(currentRadius * currentRadius - beamEnd.y * beamEnd.y);
            float secondIntersectionX = -firstIntersectionX;
            if (beamEnd.x > firstIntersectionX) {
                // beam ends before the sphere
            } else if (beamEnd.x > secondIntersectionX) {
                // beam ends inside the sphere
                nextSphereIntersectionLength = firstIntersectionX - beamEnd.x;
            } else {
                // beam ends after the sphere
                nextSphereIntersectionLength = firstIntersectionX - secondIntersectionX;
            }
            float stepLength = nextSphereIntersectionLength - prevSphereIntersectionLength;
            airOpticalDepth += 0.5 * (nextAirDensity + prevAirDensity) * stepLength;
            aerosolesOpticalDepth += 0.5 * (nextAerosolesDensity + prevAerosolesDensity) * stepLength;
        }
        prevAirDensity = nextAirDensity;
        prevAerosolesDensity = nextAerosolesDensity;
        prevSphereIntersectionLength = nextSphereIntersectionLength;
    }

    float3 airIntensityFraction = exp(-airOpticalDepth * float3(AIR_SCATTERING_BETA_RED, AIR_SCATTERING_BETA_GREEN, AIR_SCATTERING_BETA_BLUE));
    float aerosolesIntensityFraction = exp(-aerosolesOpticalDepth * AEROSOLES_SCATTERING_BETA);
    return float4(airIntensityFraction, aerosolesIntensityFraction);
}
