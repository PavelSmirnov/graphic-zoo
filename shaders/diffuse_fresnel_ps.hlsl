#define EPS 1e-6

struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D LightDirections : register(t0);

cbuffer ReflectanceParameters : register(b0) {
    float Color;
    float EyeDotNormal; // dot(directionToEye, surfaceNormal)
    float Roughness;
};

float diffuse_fresnel_ps(in GsOutput input) : SV_Target {
    float3 surfaceNormal = float3(0, 0, 1);
    float3 directionToLight = LightDirections.Sample(AllPointZeroSampler, input.uv).xyz;
    float fresnelTerm = Color + (1 - Color) * pow(1 - dot(directionToLight, surfaceNormal), 5);
    float weight = max(0,dot(directionToLight, surfaceNormal)) * PI;
    return fresnelTerm * weight;
}
