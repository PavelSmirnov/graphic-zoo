struct GsOutput {
    float4 position : SV_Position;
    float2 uv : UV;
};

SamplerState AllPointZeroSampler : register(s0);
Texture2D Texture : register(t0);

float4 view_ps(in GsOutput input) : SV_Target {
    return Texture.SampleLevel(AllPointZeroSampler, input.uv, 0);
}
