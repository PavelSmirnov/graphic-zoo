# This script calculates the transition matrix for harmonic values after rotating the space 90 degrees about the x-axis
import numpy as np
import scipy.special as ss

TEST_NUM_SAMPLES = 100
NUM_INTEGRATION_SAMPLES = 10000000
NUM_HARMONIC_BANDS = 7
EPS = 1e-2
INVERSE = False

# sphere samples
samples_theta = 2 * np.arccos(np.sqrt(1 - np.random.rand(NUM_INTEGRATION_SAMPLES)))
samples_phi = (2 * np.pi) * np.random.rand(NUM_INTEGRATION_SAMPLES)

samples_x = np.cos(samples_phi) * np.sin(samples_theta)
samples_y = np.sin(samples_phi) * np.sin(samples_theta)
samples_z = np.cos(samples_theta)

if INVERSE:
    rotated_samples_x = samples_x
    rotated_samples_y = samples_z
    rotated_samples_z = -samples_y
else:
    rotated_samples_x = samples_x
    rotated_samples_y = -samples_z
    rotated_samples_z = samples_y

rotated_samples_theta = np.arccos(rotated_samples_z)
rotated_samples_phi = np.arctan2(rotated_samples_y / np.sin(rotated_samples_theta), rotated_samples_x / np.sin(rotated_samples_theta))

def calc_real_harmonics(m, l, theta, phi):
    if m < 0:
        return ss.sph_harm(-m, l, phi, theta).imag * (np.sqrt(2.0) * (1 if m % 2 == 0 else -1))
    elif m > 0:
        return ss.sph_harm(m, l, phi, theta).real * (np.sqrt(2.0) * (1 if m % 2 == 0 else -1))
    return ss.sph_harm(m, l, phi, theta).real

for l in range(NUM_HARMONIC_BANDS - 1, NUM_HARMONIC_BANDS):
    print(f"\nl = {l}")
    for m1 in range(-l, l + 1):
        samples = calc_real_harmonics(m1, l, samples_theta, samples_phi)
        print(f"rotatedHarmonics[{l * l + l + m1}] = ", end='')
        found_first_term = False
        for m2 in range(-l, l + 1):
            test_rotated_samples = calc_real_harmonics(m2, l, rotated_samples_theta[:TEST_NUM_SAMPLES], rotated_samples_phi[TEST_NUM_SAMPLES])
            test_value = np.sum(samples[:TEST_NUM_SAMPLES] * test_rotated_samples) * (4 * np.pi / TEST_NUM_SAMPLES)
            if abs(test_value) < EPS:
                value = test_value
            else:
                rotated_samples = calc_real_harmonics(m2, l, rotated_samples_theta, rotated_samples_phi)
                value = np.sum(samples * rotated_samples) * (4 * np.pi / NUM_INTEGRATION_SAMPLES)
            if abs(value) >= EPS:
                if found_first_term:
                    print(' + ' if  value > 0 else ' - ', end='')
                    value = abs(value)
                found_first_term = True
                print(f"{value:.4} * harmonics[{l * l + l + m2}]", end='')
        print(";")
