# This tool changes texture paths in .mtl to relative paths.
# Use after each material export.
# Don't forget to store textures near material files.
import os

TOOLS_DIR = os.path.dirname(os.path.realpath(__file__))

def relatify(path):
    with open(path, 'r') as f:
        lines = [line for line in f]
    for i in range(len(lines)):
        lines[i] = lines[i].strip()
        parts = lines[i].split(' ')
        if len(parts) == 0:
            continue
        if parts[0].startswith('map') or parts[0] == 'refl':
            assert len(parts) >= 2, f"Invalid line {i + 1} in file {path}"
            if os.path.isabs(parts[1]):
                # parts[1] = os.path.basename(parts[1])
                parts[1] = os.path.relpath(parts[1], os.path.dirname(path))
                lines[i] = ' '.join(parts)
    with open(path, 'w') as f:
        f.write('\n'.join(lines))

if __name__ == '__main__':
    for path, _, files in os.walk(os.path.join(TOOLS_DIR, '../assets')):
        for name in files:
            if not name.endswith('.mtl'):
                continue
            relatify(os.path.join(path, name))
