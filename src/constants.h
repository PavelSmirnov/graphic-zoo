#pragma once
#include "helpers.h"
#include <filesystem>

std::string DefinitionToString(const float value);

////////
// FS //
////////

const std::filesystem::path SHADERS_PATH = "./shaders";

/////////////////
// SHADOW MAPS //
/////////////////

// make sure to synchronize with shaders
constexpr const int NUM_SHADOW_MAPS = 10;
constexpr const float SHADOW_PARTITION_SCHEMES_PROPORTION = 0.999f; // in [0, 1]: 0 for uniform, 1 for logarithmic
constexpr const int SHADOW_MAP_RESOLUTION = 2048;
constexpr const float SHADOW_MAP_MIN_DEPTH = 1e-6f;

constexpr const int PCF_KERNEL_SIZE = 8;
// dithering works correct only with even PCF_KERNEL_SIZE
constexpr const bool USE_PCF_DITHERING = true;

/////////
// TAA //
/////////

#ifdef NO_TAA
const float TAA_SAMPLES_OFFSETS[1][2] = {{0.5, 0.5}};
constexpr const int TAA_NUM_SAMPLES = sizeof(TAA_SAMPLES_OFFSETS) / sizeof(TAA_SAMPLES_OFFSETS[0]);
constexpr const float TAA_COEFFICIENT = 1.0f;
#else
// Halton(2, 3)
const float TAA_SAMPLES_OFFSETS[8][2] = {
    {1.0f /  2, 1.0f / 3},
    {1.0f /  4, 2.0f / 3},
    {3.0f /  4, 1.0f / 9},
    {1.0f /  8, 4.0f / 9},
    {5.0f /  8, 7.0f / 9},
    {3.0f /  8, 2.0f / 9},
    {7.0f /  8, 5.0f / 9},
    {1.0f / 16, 8.0f / 9}
};
constexpr const int TAA_NUM_SAMPLES = sizeof(TAA_SAMPLES_OFFSETS) / sizeof(TAA_SAMPLES_OFFSETS[0]);
constexpr const float TAA_COEFFICIENT = 0.1f;
#endif
constexpr const float TAA_VARIANCE_SCALE = 1.0f;

////////////////////
// SKY SCATTERING //
////////////////////

constexpr const int SCATTERING_NUM_SPHERES = 100;
constexpr const int SCATTERING_NUM_ANGLES = 10000;
constexpr const int HORIZON_RESOLUTION_X = 100;
constexpr const int HORIZON_RESOLUTION_Y = 20;

constexpr const float EARTH_RADIUS = 6371000;
constexpr const float AIR_AVERAGE_HEIGHT = 7994;
constexpr const float AEROSOLES_AVERAGE_HEIGHT = 1200;
constexpr const float ATMOSPHERE_MAX_HEIGHT = 1e5f;
constexpr const float AIR_SCATTERING_BETA_RED = 5.8e-6f;
constexpr const float AIR_SCATTERING_BETA_GREEN = 13.5e-6f;
constexpr const float AIR_SCATTERING_BETA_BLUE = 33.1e-6f;
constexpr const float AEROSOLES_SCATTERING_BETA = 2.0e-5f;
constexpr const float AEROSOLES_SCATTERING_U = 0.85f; // control haze: 0.7 to 0.85
constexpr const float SUN_RADIUS = 6.957e8f;
constexpr const float SUN_DISTANCE = 1.5185e11f;
constexpr const float NIGHT_SKY_DIM = 0.01f;
constexpr const float HORIZON_WIDTH = 0.02f;


/////////////////////////
// SPHERICAL HARMONICS //
/////////////////////////

constexpr const int SPHERICAL_HARMONICS_SIZE_LOG = 10;
constexpr const int SPHERICAL_HARMONICS_NUM_SAMPLES_X = 1 << SPHERICAL_HARMONICS_SIZE_LOG;
constexpr const int SPHERICAL_HARMONICS_NUM_SAMPLES_Y = 1 << SPHERICAL_HARMONICS_SIZE_LOG;
constexpr const int SPHERICAL_HARMONICS_NUM_BANDS = 5; // <= 5
constexpr const int SPHERICAL_HARMONICS_NUM_COEFFICIENTS = SPHERICAL_HARMONICS_NUM_BANDS * SPHERICAL_HARMONICS_NUM_BANDS;
constexpr const int SPHERICAL_HARMONICS_ARRAY_SIZE = (SPHERICAL_HARMONICS_NUM_COEFFICIENTS + 3) / 4;

/////////////////
// REFLECTANCE //
/////////////////

constexpr const int REFLECTANCE_NUM_SAMPLES_LOG = 9;
constexpr const int REFLECTANCE_NUM_SAMPLES_X = 1 << REFLECTANCE_NUM_SAMPLES_LOG;
constexpr const int REFLECTANCE_NUM_SAMPLES_Y = 1 << REFLECTANCE_NUM_SAMPLES_LOG;
constexpr const int SPECULAR_REFLECTANCE_NUM_COLOR_VALUES = 50;
constexpr const int SPECULAR_REFLECTANCE_NUM_ANGLE_VALUES = 50;
constexpr const int SPECULAR_REFLECTANCE_NUM_ROUGHNESS_VALUES = 50;
constexpr const int DIFFUSE_REFLECTANCE_NUM_COLOR_VALUES = 1000;
constexpr const float MIN_SPECULAR_POWER = 1;
constexpr const float MAX_SPECULAR_POWER = 1e5;

//////////////
// QUADTREE //
//////////////

constexpr const float QUADTREE_PROXIMITY_FACTOR_SQUARED  = ((8 * 8) * 30);
constexpr const int QUADTREE_MAX_LEVEL = 12;
static constexpr const int QUADTREE_MAX_SIZE = (1u << 20); // an estimation, the upper bound is (1u << (2 * MAX_LEVEL))

///////////
// OTHER //
///////////

constexpr const float GAMMA_CORRECTION_GAMMA = 2.2f;
constexpr const float SRGB_GAMMA_CORRECTION = 2.2f;
