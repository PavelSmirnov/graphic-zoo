#pragma once
#include <iostream>
#include <sstream>
#include <string>

void EnableLogging(const char* logFilePath);
void Log(const char* message);

std::string ToHexStr(const int value);

#define LOG(m) { \
    std::stringstream ss; \
    ss << m; \
    Log(ss.str().c_str()); \
}

#define LOG_ERROR(m) { LOG("ERROR: " << m); }
#define LOG_WARNING(m) { LOG("WARNING: " << m); }
