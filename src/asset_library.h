#pragma once
#include "d3d.h"
#include "material.h"
#include "model.h"
#include "storage.h"
#include <filesystem>
#include <set>
#include <unordered_map>
#include <vector>

class TAssetLibrary {
public:
    static TAssetLibrary* Create(const std::filesystem::path& assetsPath, TDirectX* directx);
    static TAssetLibrary* Get();
    static void Destroy();

    TStorageItemId LoadModel(const std::string& path, const std::string& name);
    void UnloadModel(const TStorageItemId modelId);

    void GetModel(const TStorageItemId modelId, int* numIndices, ID3D11Buffer** indexBuffer, ID3D11Buffer** vertexBuffer, TStorageItemId* materialId);
    void GetMaterial(const TStorageItemId materialId,
                     ID3D11Buffer** materialBuffer,
                     ID3D11ShaderResourceView** baseColorView,
                     ID3D11ShaderResourceView** dielectricSpecularView,
                     ID3D11ShaderResourceView** metalnessView,
                     ID3D11ShaderResourceView** roughnessView,
                     ID3D11ShaderResourceView** heightView);

private:
    struct TModelData {
        int numModelIndices = 0;
        ID3D11Buffer* indexBuffer = nullptr;
        ID3D11Buffer* vertexBuffer = nullptr;
        TStorageItemId materialId = -1;
        TModel model;

        TModelData() = default;
        TModelData(const TModelData& other);
        TModelData(TModelData&& other);
        ~TModelData();
        TModelData& operator=(TModelData other);
        void swap(TModelData& other);
    };

    struct TMaterialData {
        ID3D11Buffer* materialBuffer = nullptr;
        TMaterial material;

        ID3D11Texture2D* baseColor = nullptr;
        ID3D11ShaderResourceView* baseColorView = nullptr;
        ID3D11Texture2D* dielectricSpecular = nullptr;
        ID3D11ShaderResourceView* dielectricSpecularView = nullptr;
        ID3D11Texture2D* metalness = nullptr;
        ID3D11ShaderResourceView* metalnessView = nullptr;
        ID3D11Texture2D* roughness = nullptr;
        ID3D11ShaderResourceView* roughnessView = nullptr;
        ID3D11Texture2D* height = nullptr;
        ID3D11ShaderResourceView* heightView = nullptr;

        TMaterialData() = default;
        TMaterialData(const TMaterialData& other);
        TMaterialData(TMaterialData&& other);
        ~TMaterialData();
        TMaterialData& operator=(TMaterialData other);
        void swap(TMaterialData& other);
    };

private:
    TAssetLibrary(const std::filesystem::path& assetsPath, TDirectX* directx);

    void FillMaterialTextures(const TTextureDependency& textureDependency, const std::string& path, TMaterialData* materialData);
    void FillMaterialTexture(const std::string& dependency, const std::string& path, ID3D11Texture2D** texture, ID3D11ShaderResourceView** view);
    TStorageItemId LoadMaterial(const std::string& path, const std::string& name);
    TStorageItemId LoadModel(TModel&& model, const TStorageItemId materialId);
    TStorageItemId LoadMaterial(TMaterial&& material, const std::string& path, const TTextureDependency& textureDependency);

private:
    const std::filesystem::path AssetsPath;
    TDirectX* DirectX = nullptr;

    TStorage<TModelData> ModelStorage;
    TStorageItemId DefaultModelId;

    TStorage<TMaterialData> MaterialStorage;
    TStorageItemId DefaultMaterialId;
};
