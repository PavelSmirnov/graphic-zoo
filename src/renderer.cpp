#include "error.h"
#include "log.h"
#include "renderer.h"
#include "shaders.h"
#include "wic_tex.h"
#include <algorithm>
#include <random>
#include <wincodec.h>
#include <winuser.h>

namespace {
    static constexpr const float TERRAIN_LRDU[4] = {-5000, 5000, -5000, 5000};
    static constexpr const float TERRAIN_HEIGHT = 5000;

    static constexpr const float ZERO_COLOR[] = {0, 0, 0, 0};

    TRenderer* RENDERER = nullptr;

    int GetLog2(int value) {
        int l = 0;
        while (1LL << (l + 1) < value) {
            ++l;
        }
        return l;
    }

    bool ConvertPresentResultToIsIdle(const HRESULT presentResult) {
        if (presentResult == DXGI_STATUS_OCCLUDED) {
            return true;
        } else {
            ENSURE_D3D(presentResult, "SwapChain->Present failed.");
        }
        return false;
    }

    TTransformation GetJitteredProjectionMatrix(const TCamera& camera, const unsigned int frame, const unsigned int xResolution, const unsigned int yResolution) {
        const int sampleIndex = frame % TAA_NUM_SAMPLES;
        const float xOffset = (TAA_SAMPLES_OFFSETS[sampleIndex][0] * 2 - 1) * (0.5f / xResolution);
        const float yOffset = (TAA_SAMPLES_OFFSETS[sampleIndex][1] * 2 - 1) * (0.5f / yResolution);
        return GetProjectionMatrix(camera.nearClip, camera.farClip, camera.horizontalAov, camera.verticalAov, xOffset, yOffset);
    }

    TTransformation GetInverseJitteredProjectionMatrix(const TCamera& camera, const unsigned int frame, const unsigned int xResolution, const unsigned int yResolution) {
        const int sampleIndex = frame % TAA_NUM_SAMPLES;
        const float xOffset = (TAA_SAMPLES_OFFSETS[sampleIndex][0] * 2 - 1) * (0.5f / xResolution);
        const float yOffset = (TAA_SAMPLES_OFFSETS[sampleIndex][1] * 2 - 1) * (0.5f / yResolution);
        return GetInverseProjectionMatrix(camera.nearClip, camera.farClip, camera.horizontalAov, camera.verticalAov, xOffset, yOffset);
    }

    std::vector<float> CalculatePartitionScheme(const TCamera& camera) {
        std::vector<float> partitionZ(NUM_SHADOW_MAPS + 1);
        float cLog = camera.nearClip;
        const float base = std::pow(camera.farClip / camera.nearClip, 1.0f / NUM_SHADOW_MAPS);
        for (int shadowPart = 0; shadowPart <= NUM_SHADOW_MAPS; ++shadowPart) {
            const float cUni = camera.nearClip + (camera.farClip - camera.nearClip) * shadowPart / NUM_SHADOW_MAPS;
            partitionZ[shadowPart] = cLog * SHADOW_PARTITION_SCHEMES_PROPORTION + (1 - SHADOW_PARTITION_SCHEMES_PROPORTION) * cUni;
            cLog *= base;
        }
        return partitionZ;
    }

    std::vector<float> ProjectPartitionScheme(const TCamera& camera, const std::vector<float>& partitionSchemeZ) {
        std::vector<float> projectedZ(partitionSchemeZ.size());
        for (int shadowPart = 0; shadowPart < partitionSchemeZ.size(); ++shadowPart) {
            const float z = partitionSchemeZ[shadowPart];
            projectedZ[shadowPart] = camera.farClip / (camera.farClip - camera.nearClip) * (z - camera.nearClip) / z;
        }
        return projectedZ;
    }

    void CalculateLightMVP(const TVector<3>& lightPosition,
                           const std::vector<float>& partitionSchemeProjectedZ,
                           const int shadowPart,
                           const TCamera& camera,
                           const unsigned int frame,
                           const unsigned int cameraXResolution,
                           const unsigned int cameraYResolution,
                           TTransformation* projectionMatrix,
                           TTransformation* viewMatrix,
                           TTransformation* viewProjectionMatrix) {
        float zProjLow = partitionSchemeProjectedZ[shadowPart];
        float zProjHigh = partitionSchemeProjectedZ[shadowPart + 1];

        const TVector<3> cameraDirection = Normalize(camera.lookAt - camera.eye);
        *viewMatrix = GetViewMatrix(lightPosition, {0, 0, 0}, cameraDirection); // TODO: up is incorrect?

        const TTransformation inverseCameraView = GetInverseViewMatrix(camera.eye, camera.lookAt, camera.up);
        const TTransformation inverseCameraProjection = GetInverseJitteredProjectionMatrix(camera, frame, cameraXResolution, cameraYResolution);
        const TTransformation cameraProjectionToLightView = (*viewMatrix) * inverseCameraView * inverseCameraProjection;
        const std::vector<TVector<4>> frustumPartVertices = {
            cameraProjectionToLightView * TVector<4>{-1, -1, zProjLow, 1},
            cameraProjectionToLightView * TVector<4>{-1, 1, zProjLow, 1},
            cameraProjectionToLightView * TVector<4>{1, -1, zProjLow, 1},
            cameraProjectionToLightView * TVector<4>{1, 1, zProjLow, 1},
            cameraProjectionToLightView * TVector<4>{-1, -1, zProjHigh, 1},
            cameraProjectionToLightView * TVector<4>{-1, 1, zProjHigh, 1},
            cameraProjectionToLightView * TVector<4>{1, -1, zProjHigh, 1},
            cameraProjectionToLightView * TVector<4>{1, 1, zProjHigh, 1}
        };
        float minX, maxX, minY, maxY;
        float maxZ = SHADOW_MAP_MIN_DEPTH;
        for (int i = 0; i < frustumPartVertices.size(); ++i) {
            const auto point = Get3DPoint(frustumPartVertices[i]);
            if (i == 0 || minX > point(0)) {
                minX = point(0);
            }
            if (i == 0 || maxX < point(0)) {
                maxX = point(0);
            }
            if (i == 0 || minY > point(1)) {
                minY = point(1);
            }
            if (i == 0 || maxY < point(1)) {
                maxY = point(1);
            }
            if (maxZ < point(2)) {
                maxZ = point(2);
            }
        }

        const float maxWidth = std::max<float>(maxX - minX, maxY - minY);
        *projectionMatrix = GetOrthoProjectionMatrix(0, maxZ, maxWidth, maxWidth, 0.5f * (maxX + minX), 0.5f * (maxY + minY));

        // This has higher resolution but looks worse. Omnidirectional alias is visible? PCF works differenlty for different axes?
        //*projectionMatrix = GetOrthoProjectionMatrix(0, maxZ, maxX - minX, maxY - minY, 0.5 * (maxX + minX), 0.5 * (maxY + minY));

        *viewProjectionMatrix = (*projectionMatrix) * (*viewMatrix);
    }
}

TRenderer* TRenderer::Create(const TWindow* window, TDirectX* directx, TAssetLibrary* assetLibrary) {
    ENSURE(RENDERER == nullptr, "New renderer to be created, yet the old one is not destroyed.");
    return RENDERER = new TRenderer(window, directx, assetLibrary);
}

TRenderer* TRenderer::Get() {
    return RENDERER;
}

void TRenderer::Destroy() {
    ENSURE(RENDERER != nullptr, "The renderer to be destroyed, but it is not created yet.");
    delete RENDERER;
    RENDERER = nullptr;
}

TRenderer::TRenderer(const TWindow* window, TDirectX* directx, TAssetLibrary* assetLibrary)
    : Window(window)
    , Device(directx->GetDevice())
    , Context(directx->GetContext())
    , AssetLibrary(assetLibrary)
    , PlaneModelId(AssetLibrary->LoadModel("models/plane/model.obj", "plane")) {
    Device->AddRef();
    Context->AddRef();
    InitializeSwapChain();
    InitializeResources();
    InitializeShaders();
    InitializeTextures();
    InitializeBuffers();
    InitializePipelines();
    PrecalcScattering();
    PrecalcHarmonicValues();
    PrecalcReflectanceHarmonics();
    PrecalcSpecularBrdfAndDiffuseFresnel();
}

TRenderer::~TRenderer() {
    SpecularBrdf->Release();
    SpecularBrdfResource->Release();
    ReflectanceParametersBuffer->Release(); // TODO: release earlier
    AdditiveBlend->Release();
    ProportionalBlend->Release();
    AllLinearClampSamplerState->Release();
    AllPointClampComparisonSamplerState->Release();
    AllPointZeroSamplerState->Release();
    DepthStencilState->Release();
    DrawRasterizerState->Release();
    ShadowDepthRasterizerState->Release();

    NightSky->Release();
    NightSkyResource->Release();
    TerrainPlacementBuffer->Release();
    TerrainDetailTexture->Release();
    TerrainDetailView->Release();
    TerrainAlbedoTexture->Release();
    TerrainAlbedoView->Release();
    TerrainHeightsTexture->Release();
    TerrainHeightsView->Release();
    TerrainNormalTexture->Release();
    TerrainNormalView->Release();

    AmbientBuffer->Release();
    FocalPointBuffer->Release();
    FogBuffer->Release();
    LightBuffer->Release();
    LightViewProjectionBuffer->Release();
    ObjectTransformationBuffer->Release();
    TaaBuffer->Release();
    HdrBuffer->Release();
    ViewProjectionBuffer->Release();

    SingleZeroIndexBuffer->Release();

    QuadtreeNodesMainBuffers[0]->Release();
    QuadtreeNodesMainBuffers[1]->Release();

    BackBufferRenderTargetView->Release();
    SwapChain->Release();
}

void TRenderer::InitializeSwapChain() {
    DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
    swapChainDesc.Width = Window->GetWidth();
    swapChainDesc.Height = Window->GetHeight();
    swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.Stereo = false;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.BufferCount = 2;
    swapChainDesc.Scaling = DXGI_SCALING_NONE;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
    swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
    swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

    DXGI_SWAP_CHAIN_FULLSCREEN_DESC swapChainFullScreenDesc = {};
    swapChainFullScreenDesc.RefreshRate.Numerator = 60;
    swapChainFullScreenDesc.RefreshRate.Denominator = 1;
    swapChainFullScreenDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainFullScreenDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    swapChainFullScreenDesc.Windowed = true;

    IDXGIDevice* dxgiDevice = nullptr;
    ENSURE_D3D(Device->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice),
               "Failed to get IDXGIDevice from DirectX11 Device.");
    IDXGIAdapter* dxgiAdapter = nullptr;
    ENSURE_D3D(dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter),
               "Failed to get IDXGIAdapter from IDXGIDevice.");
    IDXGIAdapter* dxgiFactory = nullptr;
    ENSURE_D3D(dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory),
               "Failed to get IDXGIFactory from IDXGIAdapter.");

    IDXGIFactory2* dxgiFactory2 = nullptr;
    // TODO: if not S_OK, then there is no DirectX 11.1, can try dxgiFactory->CreateSwapChain();
    ENSURE_D3D(dxgiFactory->QueryInterface(__uuidof(IDXGIFactory2), (void**)&dxgiFactory2),
               "Failed to get IDXGIFactory2 from IDXGIFactory.");

    ENSURE_D3D(dxgiFactory2->CreateSwapChainForHwnd(Device,
                                                    Window->GetHwnd(),
                                                    &swapChainDesc,
                                                    &swapChainFullScreenDesc,
                                                    nullptr,
                                                    &SwapChain),
               "Failed to create swap chain.");

    dxgiFactory2->Release();
    dxgiFactory->Release();
    dxgiAdapter->Release();
    dxgiDevice->Release();

    if (Window->GetIsFullscreen()) {
        ENSURE_D3D(SwapChain->SetFullscreenState(true, nullptr),
                   "Failed to go fullscreen.");
        ENSURE_D3D(SwapChain->ResizeBuffers(2, Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM, swapChainDesc.Flags),
                   "Failed to resize SwapChain buffers.");
    }
}

void TRenderer::InitializeResources() {
    { // BackBuffer
        ENSURE_D3D(SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&BackBuffer)),
                   "Failed to access back buffer to set a render target.");
        ENSURE_D3D(Device->CreateRenderTargetView(BackBuffer, nullptr, &BackBufferRenderTargetView),
                   "Failed to create BackBufferRenderTargetView render target view.");
    }
    { // screen viewport
        ScreenViewport.TopLeftX = 0;
        ScreenViewport.TopLeftY = 0;
        ScreenViewport.Width = static_cast<float>(Window->GetWidth());
        ScreenViewport.Height = static_cast<float>(Window->GetHeight());
        ScreenViewport.MinDepth = 0;
        ScreenViewport.MaxDepth = 1;
    }
    { // screen viewport
        UnitViewport.TopLeftX = 0;
        UnitViewport.TopLeftY = 0;
        UnitViewport.Width = 1;
        UnitViewport.Height = 1;
        UnitViewport.MinDepth = 0;
        UnitViewport.MaxDepth = 1;
    }
    { // specular brdf viewport
        ReflectanceViewport.TopLeftX = 0;
        ReflectanceViewport.TopLeftY = 0;
        ReflectanceViewport.Width = static_cast<float>(REFLECTANCE_NUM_SAMPLES_X);
        ReflectanceViewport.Height = static_cast<float>(REFLECTANCE_NUM_SAMPLES_Y);
        ReflectanceViewport.MinDepth = 0;
        ReflectanceViewport.MaxDepth = 1;
    }
    { // harmonics viewport
        SkySamplesViewport.TopLeftX = 0;
        SkySamplesViewport.TopLeftY = 0;
        SkySamplesViewport.Width = static_cast<float>(SPHERICAL_HARMONICS_NUM_SAMPLES_X);
        SkySamplesViewport.Height = static_cast<float>(SPHERICAL_HARMONICS_NUM_SAMPLES_Y);
        SkySamplesViewport.MinDepth = 0;
        SkySamplesViewport.MaxDepth = 1;
    }
    { // horizon viewport
        HorizonViewport.TopLeftX = 0;
        HorizonViewport.TopLeftY = 0;
        HorizonViewport.Width = static_cast<float>(HORIZON_RESOLUTION_X);
        HorizonViewport.Height = static_cast<float>(HORIZON_RESOLUTION_Y);
        HorizonViewport.MinDepth = 0;
        HorizonViewport.MaxDepth = 1;
    }
    { // precalced scattering viewport
        ScatteringViewport.TopLeftX = 0;
        ScatteringViewport.TopLeftY = 0;
        ScatteringViewport.Width = SCATTERING_NUM_SPHERES;
        ScatteringViewport.Height = SCATTERING_NUM_ANGLES;
        ScatteringViewport.MinDepth = 0;
        ScatteringViewport.MaxDepth = 1;
    }
    { // shadow map viewport
        LightScreenViewport.TopLeftX = 0;
        LightScreenViewport.TopLeftY = 0;
        LightScreenViewport.Width = SHADOW_MAP_RESOLUTION;
        LightScreenViewport.Height = SHADOW_MAP_RESOLUTION;
        LightScreenViewport.MinDepth = 0;
        LightScreenViewport.MaxDepth = 1;
    }
    { // additive blend
        D3D11_BLEND_DESC desc = {};
        desc.AlphaToCoverageEnable = false;
        desc.IndependentBlendEnable = false;
        desc.RenderTarget[0].BlendEnable = true;
        desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
        desc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
        desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
        desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
        desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
        desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
        desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
        ENSURE_D3D(Device->CreateBlendState(&desc, &AdditiveBlend), "Failed to create additive blend state.");
    }
    { // proportional blend
        D3D11_BLEND_DESC desc = {};
        desc.AlphaToCoverageEnable = false;
        desc.IndependentBlendEnable = false;
        desc.RenderTarget[0].BlendEnable = true;
        desc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
        desc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
        desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
        desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
        desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
        desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
        desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
        ENSURE_D3D(Device->CreateBlendState(&desc, &ProportionalBlend), "Failed to create proportional blend state.");
    }
    { // depth-stencil
        D3D11_DEPTH_STENCIL_DESC dsDesc = {};
        // depth test params
        dsDesc.DepthEnable = true;
        dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
        dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

        // stencil test params
        dsDesc.StencilEnable = false;
        dsDesc.StencilReadMask = 0xFF;
        dsDesc.StencilWriteMask = 0xFF;

        // front facing pixel stencil operations
        dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
        dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
        dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
        dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

        // back face
        dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
        dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
        dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
        dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

        ENSURE_D3D(Device->CreateDepthStencilState(&dsDesc, &DepthStencilState),
                   "Failed to create DepthStencilState.");
    }

    DepthStencil.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R32_TYPELESS, D3D11_USAGE_DEFAULT, 1, 1, D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE, 0, 0, Device)
        .AddDsv(Device, 0, DXGI_FORMAT_D32_FLOAT)
        .AddSrv(Device, 1, 0, DXGI_FORMAT_R32_FLOAT);

    NormalizedHdr.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R8G8B8A8_UNORM, D3D11_USAGE_DEFAULT, 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    for (int i = 0; i < 2; ++i) {
        TaaFrames[i].Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_USAGE_DEFAULT, 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
            .AddSrv(Device)
            .AddRtv(Device);
    }

    ShadowMap.Create(SHADOW_MAP_RESOLUTION, SHADOW_MAP_RESOLUTION, DXGI_FORMAT_R32_TYPELESS, D3D11_USAGE_DEFAULT,
                     1, NUM_SHADOW_MAPS, D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE, 0, 0, Device)
        .AddArraySrv(Device, 1, 0, 0, NUM_SHADOW_MAPS, DXGI_FORMAT_R32_FLOAT);
    for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
        ShadowMap.AddArrayDsv(Device, 0, shadowPart, 1, DXGI_FORMAT_D32_FLOAT);
    }

    PrecalcedScattering.Create(SCATTERING_NUM_SPHERES, SCATTERING_NUM_ANGLES, DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_USAGE_DEFAULT,
                               1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    { // draw rasterizer
        D3D11_RASTERIZER_DESC desc = {};
        desc.FillMode = D3D11_FILL_SOLID;
        desc.CullMode = D3D11_CULL_BACK;
        desc.FrontCounterClockwise = false;

        desc.DepthBias = 0; // 5 is just enough for light perpendicular to a plane
        desc.DepthBiasClamp = 0; // hope for now light parallel to planes
        desc.SlopeScaledDepthBias = 0; // 4 seems mathematically correct

        desc.DepthClipEnable = true;
        desc.ScissorEnable = false;
        desc.MultisampleEnable = false;
        desc.AntialiasedLineEnable = false;

        ENSURE_D3D(Device->CreateRasterizerState(&desc, &DrawRasterizerState),
                   "Failed to create draw rasterizer state.");
    }
    { // shadow map rasterizer
        D3D11_RASTERIZER_DESC desc = {};
        desc.FillMode = D3D11_FILL_SOLID;
        desc.CullMode = D3D11_CULL_BACK;
        desc.FrontCounterClockwise = false;

        desc.DepthBias = 100'000; // 5 is just enough for light perpendicular to a plane
        desc.DepthBiasClamp = 1e-2f; // hope for now light parallel to planes
        desc.SlopeScaledDepthBias = 10; // 4 seems mathematically correct

        desc.DepthClipEnable = true;
        desc.ScissorEnable = false;
        desc.MultisampleEnable = false;
        desc.AntialiasedLineEnable = false;

        ENSURE_D3D(Device->CreateRasterizerState(&desc, &ShadowDepthRasterizerState),
                   "Failed to create shadow rasterizer state.");
    }

    SkySamples.Create(SPHERICAL_HARMONICS_NUM_SAMPLES_X, SPHERICAL_HARMONICS_NUM_SAMPLES_Y, DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_USAGE_DEFAULT,
                      1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    Horizon.Create(HORIZON_RESOLUTION_X, HORIZON_RESOLUTION_Y, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                   1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    HorizonStaging.Create(HORIZON_RESOLUTION_X, HORIZON_RESOLUTION_Y, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_STAGING,
                          1, 1, 0, D3D11_CPU_ACCESS_READ, 0, Device);

    Sky.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_USAGE_DEFAULT,
               1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    SkyDirections.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                         1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    DiffuseLightingTerms.Create(SPHERICAL_HARMONICS_NUM_SAMPLES_X, SPHERICAL_HARMONICS_NUM_SAMPLES_Y, DXGI_FORMAT_R32_FLOAT, D3D11_USAGE_DEFAULT,
                                1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    ReflectionsHarmonicValues.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                                    1, SPHERICAL_HARMONICS_ARRAY_SIZE, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddArraySrv(Device, 1, 0, 0, SPHERICAL_HARMONICS_ARRAY_SIZE);
    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        ReflectionsHarmonicValues.AddArrayRtv(Device, 0, i, 1);
    }

    ReflectionDirections.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                                1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    HorizonDirections.Create(HORIZON_RESOLUTION_X, HORIZON_RESOLUTION_Y, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                             1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    const int maxMip = std::max<int>(GetLog2(Window->GetWidth()), GetLog2(Window->GetHeight()));
    AverageColorCalculation.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_USAGE_DEFAULT,
                                   maxMip + 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0,
                                   D3D11_RESOURCE_MISC_GENERATE_MIPS, Device)
        .AddArraySrv(Device, maxMip + 1, 0, 0, 1)
        .AddArraySrv(Device, 1, maxMip);
    AverageColorImage.Create(1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT, 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddRtv(Device);
    AverageColorCalculationStaging.Create(1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_STAGING,
                                          1, 1, 0, D3D11_CPU_ACCESS_READ, 0, Device);

    SphericalHarmonicValues.Create(SPHERICAL_HARMONICS_NUM_SAMPLES_X, SPHERICAL_HARMONICS_NUM_SAMPLES_Y, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                                   1, SPHERICAL_HARMONICS_ARRAY_SIZE, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddArraySrv(Device, 1, 0, 0, SPHERICAL_HARMONICS_ARRAY_SIZE);
    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        SphericalHarmonicValues.AddArrayRtv(Device, 0, i, 1);
    }

    SphericalHarmonicsCalculation.Create(SPHERICAL_HARMONICS_NUM_SAMPLES_X, SPHERICAL_HARMONICS_NUM_SAMPLES_Y, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                                         SPHERICAL_HARMONICS_SIZE_LOG + 1, SPHERICAL_HARMONICS_ARRAY_SIZE, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0,
                                         D3D11_RESOURCE_MISC_GENERATE_MIPS, Device)
        .AddArraySrv(Device, SPHERICAL_HARMONICS_SIZE_LOG + 1, 0, 0, SPHERICAL_HARMONICS_ARRAY_SIZE);
    SphericalHarmonicsCalculation.AddArrayRtv(Device, 0, 0, SPHERICAL_HARMONICS_ARRAY_SIZE);

    SphericalHarmonicsCalculationStaging.Create(SPHERICAL_HARMONICS_ARRAY_SIZE, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_STAGING,
                                                1, 1, 0, D3D11_CPU_ACCESS_READ, 0, Device);

    ReflectanceCalculation.Create(REFLECTANCE_NUM_SAMPLES_X, REFLECTANCE_NUM_SAMPLES_Y, DXGI_FORMAT_R32_FLOAT, D3D11_USAGE_DEFAULT,
                                  REFLECTANCE_NUM_SAMPLES_LOG + 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0,
                                  D3D11_RESOURCE_MISC_GENERATE_MIPS, Device)
        .AddArraySrv(Device, REFLECTANCE_NUM_SAMPLES_LOG + 1, 0, 0, 1)
        .AddRtv(Device);

    SeparatedRGB.Create(SPHERICAL_HARMONICS_NUM_SAMPLES_X, SPHERICAL_HARMONICS_NUM_SAMPLES_Y, DXGI_FORMAT_R32_FLOAT, D3D11_USAGE_DEFAULT,
                        1, 3, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device);
    for (int i = 0; i < 3; ++i) {
        SeparatedRGB
            .AddArraySrv(Device, 1, 0, i, 1)
            .AddArrayRtv(Device, 0, i, 1);
    }

    LitScene.Create(Window->GetWidth(), Window->GetHeight(), DXGI_FORMAT_R16G16B16A16_FLOAT, D3D11_USAGE_DEFAULT, 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
        .AddSrv(Device)
        .AddRtv(Device);

    const int numGBuffers = sizeof(GBuffer) / sizeof(GBuffer[0]);
    DXGI_FORMAT gBufferFormats[numGBuffers] = {
        DXGI_FORMAT_R16G16B16A16_FLOAT, // 0: base color (dielectric diffuse or conductor specular) and dielectric specular color
        DXGI_FORMAT_R16G16B16A16_FLOAT, // 1: normal and is occupied
        DXGI_FORMAT_R32G32B32A32_FLOAT, // 2: world space position
        DXGI_FORMAT_R16G16B16A16_FLOAT, // 3: metalness and roughness
    };

    for (int i = 0; i < numGBuffers; ++i) {
        GBuffer[i].Create(Window->GetWidth(), Window->GetHeight(), gBufferFormats[i], D3D11_USAGE_DEFAULT, 1, 1, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET, 0, 0, Device)
            .AddSrv(Device)
            .AddRtv(Device);
    }

    { // specular brdf
        D3D11_TEXTURE3D_DESC texDesc = {};
        texDesc.Width = SPECULAR_REFLECTANCE_NUM_COLOR_VALUES;
        texDesc.Height = SPECULAR_REFLECTANCE_NUM_ANGLE_VALUES;
        texDesc.Depth = SPECULAR_REFLECTANCE_NUM_ROUGHNESS_VALUES;
        texDesc.MipLevels = 1;
        texDesc.Format = DXGI_FORMAT_R32_FLOAT;
        texDesc.Usage = D3D11_USAGE_DEFAULT;
        texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        texDesc.CPUAccessFlags = 0;
        texDesc.MiscFlags = 0;

        ENSURE_D3D(Device->CreateTexture3D(&texDesc, nullptr, &SpecularBrdf),
                   "Failed to create SpecularBrdf texture.");

        D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
        srvDesc.Format = texDesc.Format;
        srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
        srvDesc.Texture3D.MostDetailedMip = 0;
        srvDesc.Texture3D.MipLevels = 1;

        ENSURE_D3D(Device->CreateShaderResourceView(SpecularBrdf, &srvDesc, &SpecularBrdfResource),
                   "Failed to create SpecularBrdf SRV.");
    }

    DiffuseFresnel.Create(DIFFUSE_REFLECTANCE_NUM_COLOR_VALUES, 1, DXGI_FORMAT_R32_FLOAT, D3D11_USAGE_DEFAULT,
                          1, 1, D3D11_BIND_SHADER_RESOURCE, 0, 0, Device)
        .AddSrv(Device);

    InitializeSphereSamples(SPHERICAL_HARMONICS_NUM_SAMPLES_X, SPHERICAL_HARMONICS_NUM_SAMPLES_Y, false, &SphereSamples);
    InitializeSphereSamples(REFLECTANCE_NUM_SAMPLES_X, REFLECTANCE_NUM_SAMPLES_Y, true, &ReflectanceSphereSamples);
}

void TRenderer::InitializeShaders() {
    ID3DBlob* compileErrors = nullptr;

    std::filesystem::path ShadersPath = "./shaders";

    ObjectVS.Create("object_vs", OBJECT_INPUT_LAYOUT, sizeof(OBJECT_INPUT_LAYOUT) / sizeof(D3D11_INPUT_ELEMENT_DESC), Device);
    QuadmapVS.Create("quadmap_vs", QUADTREE_AND_QUADMAP_INPUT_LAYOUT, sizeof(QUADTREE_AND_QUADMAP_INPUT_LAYOUT) / sizeof(D3D11_INPUT_ELEMENT_DESC), Device);
    QuadtreeVS.Create("quadtree_vs", QUADTREE_AND_QUADMAP_INPUT_LAYOUT, sizeof(QUADTREE_AND_QUADMAP_INPUT_LAYOUT) / sizeof(D3D11_INPUT_ELEMENT_DESC), Device);
    ViewVS.Create("view_vs", VIEW_INPUT_LAYOUT, sizeof(VIEW_INPUT_LAYOUT) / sizeof(D3D11_INPUT_ELEMENT_DESC), Device);

    QuadmapHS.Create("quadmap_hs", Device);

    QuadmapDS.Create("quadmap_ds", Device);

    HarmonicsArrayGS.Create("harmonics_array_gs", Device);
    HorizonGS.Create("horizon_gs", Device);
    QuadmapGS.Create("quadmap_gs", Device);
    QuadtreeGS.CreateWithStreamOutput("quadtree_gs",
                                      QUADTREE_SO_DECLARATION_ENTRY,
                                      sizeof(QUADTREE_SO_DECLARATION_ENTRY) / sizeof(D3D11_SO_DECLARATION_ENTRY),
                                      Device);
    SkyDirectionsGS.Create("sky_directions_gs", Device);
    ViewGS.Create("view_gs", Device);

    AmbientPS.Create("ambient_ps", Device);
    DiffuseFresnelPS.Create("diffuse_fresnel_ps", Device);
    DiffuseLightingTermsPS.Create("diffuse_lighting_terms_ps", Device);
    DistanceFogPS.Create("distance_fog_ps", Device);
    HarmonicValuesPS.Create("harmonic_values_ps", Device);
    HdrPS.Create("hdr_ps", Device);
    LightPS.Create("light_ps", Device);
    MultiplyByHarmonicsPS.Create("multiply_by_harmonics_ps", Device);
    ObjectPS.Create("object_ps", Device);
    PrecalcScatteringPS.Create("precalc_scattering_ps", Device);
    QuadmapPS.Create("quadmap_ps", Device);
    ReflectionsPS.Create("reflections_ps", Device);
    SeparateRGBPS.Create("separate_rgb_ps", Device);
    SkyDirectionsPS.Create("sky_directions_ps", Device);
    SkyPS.Create("sky_ps", Device);
    SpecularBrdfPS.Create("specular_brdf_ps", Device);
    TaaPS.Create("taa_ps", Device);
    ViewPS.Create("view_ps", Device);
    VolumetricFogPS.Create("volumetric_fog_ps", Device);

    { // AllLinearClampSamplerState
        D3D11_SAMPLER_DESC samplerDesc = {};
        std::memset(&samplerDesc, 0, sizeof(samplerDesc));
        samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
        samplerDesc.MipLODBias = 0;
        samplerDesc.MaxAnisotropy = 1;
        samplerDesc.MinLOD = 0;
        samplerDesc.MaxLOD = QUADTREE_MAX_LEVEL;

        ENSURE_D3D(Device->CreateSamplerState(&samplerDesc, &AllLinearClampSamplerState),
                   "Failed to create a sampler state.");
    }

    { // AllPointClampComparisonSamplerState
        D3D11_SAMPLER_DESC samplerDesc = {};
        std::memset(&samplerDesc, 0, sizeof(samplerDesc));
        samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
        samplerDesc.MipLODBias = 0;
        samplerDesc.MaxAnisotropy = 1;
        samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS;
        samplerDesc.MinLOD = 0;
        samplerDesc.MaxLOD = QUADTREE_MAX_LEVEL;

        ENSURE_D3D(Device->CreateSamplerState(&samplerDesc, &AllPointClampComparisonSamplerState),
                   "Failed to create a sampler state.");
    }
    { // AllPointZeroSamplerState
        D3D11_SAMPLER_DESC samplerDesc = {};
        std::memset(&samplerDesc, 0, sizeof(samplerDesc));
        samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
        samplerDesc.MipLODBias = 0;
        samplerDesc.MaxAnisotropy = 1;
        samplerDesc.MinLOD = 0;
        samplerDesc.MaxLOD = 0;
        samplerDesc.BorderColor[0] = 0;
        samplerDesc.BorderColor[1] = 0;
        samplerDesc.BorderColor[2] = 0;
        samplerDesc.BorderColor[3] = 0;

        ENSURE_D3D(Device->CreateSamplerState(&samplerDesc, &AllPointZeroSamplerState),
                   "Failed to create a sampler state.");
    }
}

void TRenderer::InitializeTextures() {
    ENSURE(ReadTiffTextureFromFile(L"assets\\terrain\\detail.tif", true/*generateMipMaps*/, &TerrainDetailTexture, &TerrainDetailView),
           "Failed to read the terrain detail texture.");
    ENSURE(ReadTiffTextureFromFile(L"assets\\terrain\\albedo.tif", true/*generateMipMaps*/, &TerrainAlbedoTexture, &TerrainAlbedoView),
           "Failed to read the terrain albedo texture.");
    ENSURE(ReadTiffTextureFromFile(L"assets\\terrain\\heights.tif", false/*generateMipMaps*/, &TerrainHeightsTexture, &TerrainHeightsView),
           "Failed to read the terrain height texture.");
    ENSURE(ReadTiffTextureFromFile(L"assets\\terrain\\normals.tif", true/*generateMipMaps*/, &TerrainNormalTexture, &TerrainNormalView),
           "Failed to read the terrain normal texture.");
    ENSURE(ReadTiffTextureFromFile(L"assets\\environment\\sky.tif", false/*generateMipMaps*/, &NightSky, &NightSkyResource),
           "Failed to read the night sky texture.");
}

void TRenderer::InitializeBuffers() {
    { // ObjectTransformationBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(TTransformation::GetDataSize());
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;
        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &ObjectTransformationBuffer),
                   "Failed to create the object transformation buffer.");
    }
    { // ReflectanceParametersBuffer
        D3D11_BUFFER_DESC desc = {};
        desc.ByteWidth = sizeof(float) * 4;
        desc.Usage = D3D11_USAGE_DYNAMIC;
        desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        desc.MiscFlags = 0;
        desc.StructureByteStride = 0;
        ENSURE_D3D(Device->CreateBuffer(&desc, nullptr, &ReflectanceParametersBuffer),
                   "Failed to create the specular brdf parameters buffer.");
    }
    { // AmbientBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(SPHERICAL_HARMONICS_ARRAY_SIZE * 4 * sizeof(float)) * 4; // 4 = red + green + blue + diffuse
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    }
    { // AmbientBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(SPHERICAL_HARMONICS_ARRAY_SIZE * 4 * sizeof(float)) * 4; // 4 = red + green + blue + diffuse
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;
        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &AmbientBuffer),
                   "Failed to create the ambient buffer.");
    }
    { // FogBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = sizeof(float) * 8;
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &FogBuffer),
                   "Failed to create the fog buffer.");
    }
    { // TaaBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(TTransformation::GetDataSize());
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &TaaBuffer),
                   "Failed to create the taa buffer.");
    }
    { // HdrBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(sizeof(float) * 4);
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &HdrBuffer),
                   "Failed to create the hdr buffer.");
    }
    { // ViewProjectionBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(TTransformation::GetDataSize() * 5); // 5 matrices
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &ViewProjectionBuffer),
                   "Failed to create a buffer for view and projection matrices.");
    }
    { // LightViewProjectionBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = static_cast<UINT>(TTransformation::GetDataSize() * 3); // 3 matrices
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &LightViewProjectionBuffer),
                   "Failed to create a buffer for light view and projection matrices.");
    }
    { // LightBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        const int numElementsInPartition = (NUM_SHADOW_MAPS - 1 + 3) / 4 * 4;
        typeDesc.ByteWidth = static_cast<UINT>(sizeof(float) * 4 * 3 +
                                               TTransformation::GetDataSize() * 3 * NUM_SHADOW_MAPS +
                                               sizeof(float) * numElementsInPartition);
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &LightBuffer),
                   "Failed to create a buffer for light.");
    }
    { // FocalPointBuffer
        FocalPointX = 0.0;
        FocalPointY = 0.0;
        HorizontalFov = 0.0;
        float focalPointData[] = {FocalPointX, FocalPointY, HorizontalFov, 0}; // at least 16 bytes for constant buffers

        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = sizeof(focalPointData);
        typeDesc.Usage = D3D11_USAGE_DYNAMIC;
        typeDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        typeDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        D3D11_SUBRESOURCE_DATA storeDesc = {};
        storeDesc.pSysMem = focalPointData;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, &storeDesc, &FocalPointBuffer),
                   "Failed to create focal point buffer for a quadtree.");
    }
    { // QuadtreeNodesMainBuffers
        for (int i = 0; i < 2; ++i) {
            D3D11_BUFFER_DESC typeDesc = {};
            typeDesc.ByteWidth = sizeof(unsigned int) * QUADTREE_MAX_SIZE;
            typeDesc.Usage = D3D11_USAGE_DEFAULT;
            typeDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_STREAM_OUTPUT;
            typeDesc.CPUAccessFlags = 0;
            typeDesc.MiscFlags = 0;
            typeDesc.StructureByteStride = 0;

            ENSURE_D3D(Device->CreateBuffer(&typeDesc, nullptr, &QuadtreeNodesMainBuffers[i]),
                       "Failed to create nodes buffer for a quadtree.");
        }
        QuadtreeNodesBackBufferPtr = QuadtreeNodesMainBuffers[0];
    }
    { // SingleZeroIndexBuffer
        D3D11_BUFFER_DESC typeDesc = {};
        typeDesc.ByteWidth = sizeof(unsigned int) * 4;
        typeDesc.Usage = D3D11_USAGE_IMMUTABLE;
        typeDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        typeDesc.CPUAccessFlags = 0;
        typeDesc.MiscFlags = 0;
        typeDesc.StructureByteStride = 0;

        unsigned int data[4] = {0};
        D3D11_SUBRESOURCE_DATA storeDesc = {};
        storeDesc.pSysMem = data;

        ENSURE_D3D(Device->CreateBuffer(&typeDesc, &storeDesc, &SingleZeroIndexBuffer),
                   "Failed to create nodes buffer for a quadtree.");
        QuadtreeNodesFrontBufferPtr = SingleZeroIndexBuffer;
    }
    { // TerrainPlacementBuffer
        D3D11_BUFFER_DESC desc = {};

        desc.ByteWidth = sizeof(float) * 8;
        desc.Usage = D3D11_USAGE_IMMUTABLE;
        desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        desc.CPUAccessFlags = 0;
        desc.MiscFlags = 0;
        desc.StructureByteStride = 0;

        float data[] = {
            TERRAIN_LRDU[0],
            TERRAIN_LRDU[1],
            TERRAIN_LRDU[2],
            TERRAIN_LRDU[3],
            TERRAIN_HEIGHT,
            0, 0, 0 // unused
        };
        D3D11_SUBRESOURCE_DATA dataDesc = {};
        dataDesc.pSysMem = data;

        ENSURE_D3D(Device->CreateBuffer(&desc, &dataDesc, &TerrainPlacementBuffer),
                   "Failed to create terrain placement buffer for the quadmap.");
    }
}

void TRenderer::InitializePipelines() {
    // TODO: should you unbind constant buffer before updating?
    // TODO: pass desired slots, so that context doesn't unbind resources from other slots
    CurrentPipeline = FixPipeline(DefaultPipeline);

    ViewPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                    D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                    nullptr,
                                    DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(ViewPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(&ViewInputResourcePtr)
        .AddOMRenderTarget(&BackBufferRenderTargetView)
        .Build();

    ConvertPixelPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                            D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                            nullptr,
                                            DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(UnitViewport)
        .SetPS(ViewPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(AverageColorCalculation.GetSrv(1))
        .AddOMRenderTarget(AverageColorImage.GetRtv())
        .Build();

    CalculateDiffuseFresnelPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                                       D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                       nullptr,
                                                       DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ReflectanceViewport)
        .SetPS(DiffuseFresnelPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(ReflectanceSphereSamples.GetSrv())
        .AddPSBuffer(&ReflectanceParametersBuffer)
        .AddOMRenderTarget(ReflectanceCalculation.GetRtv())
        .Build();

    CalculateSpecularBrdfPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                                     D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                     nullptr,
                                                     DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ReflectanceViewport)
        .SetPS(SpecularBrdfPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(ReflectanceSphereSamples.GetSrv())
        .AddPSBuffer(&ReflectanceParametersBuffer)
        .AddOMRenderTarget(ReflectanceCalculation.GetRtv())
        .Build();

    ReflectionsPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                           D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                           nullptr,
                                           DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(ReflectionsPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(GBuffer[0].GetSrv())
        .AddPSResource(GBuffer[1].GetSrv())
        .AddPSResource(GBuffer[2].GetSrv())
        .AddPSResource(GBuffer[3].GetSrv())
        .AddPSBuffer(&ViewProjectionBuffer)
        .AddOMRenderTarget(ReflectionDirections.GetRtv())
        .Build();

    DiffuseLightingTermsPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                                    D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                    nullptr,
                                                    DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(SkySamplesViewport)
        .SetPS(DiffuseLightingTermsPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(SphereSamples.GetSrv())
        .AddOMRenderTarget(DiffuseLightingTerms.GetRtv())
        .Build();

    MultiplyByHarmonicsPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                                   D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                   nullptr,
                                                   DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(HarmonicsArrayGS)
        .AddRSViewport(SkySamplesViewport)
        .SetPS(MultiplyByHarmonicsPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(SphericalHarmonicValues.GetSrv())
        .AddPSResource(&FunctionToHarmonizeResourcePtr)
        .AddOMRenderTarget(SphericalHarmonicsCalculation.GetRtv())
        .Build();

    SeparateRGBPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                           D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                           nullptr,
                                           DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(SkySamplesViewport)
        .SetPS(SeparateRGBPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(SkySamples.GetSrv())
        .AddOMRenderTarget(SeparatedRGB.GetRtv(0))
        .AddOMRenderTarget(SeparatedRGB.GetRtv(1))
        .AddOMRenderTarget(SeparatedRGB.GetRtv(2))
        .Build();

    PrecalcScatteringPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                                 D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                 nullptr,
                                                 DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScatteringViewport)
        .SetPS(PrecalcScatteringPS)
        .AddOMRenderTarget(PrecalcedScattering.GetRtv())
        .Build();

    NormalizeHDRPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                            D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                            nullptr,
                                            DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(HdrPS)
        .AddPSBuffer(&HdrBuffer)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(&NormalizeHDRInputResourcePtr)
        .AddOMRenderTarget(NormalizedHdr.GetRtv())
        .Build();

    TaaPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                   D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                   nullptr,
                                   DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(TaaPS)
        .AddPSBuffer(&TaaBuffer)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSResource(&TaaHistoryResourcePtr)
        .AddPSResource(&TaaNewFrameResourcePtr)
        .AddPSResource(GBuffer[2].GetSrv())
        .AddOMRenderTarget(&TaaBlendedTargetPtr)
        .Build();

    DistanceFogPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                          D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                          nullptr,
                                          DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(DistanceFogPS)
        .AddPSBuffer(&LightBuffer)
        .AddPSBuffer(&ViewProjectionBuffer)
        .AddPSBuffer(&FogBuffer)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(GBuffer[0].GetSrv())
        .AddPSResource(GBuffer[1].GetSrv())
        .AddPSResource(GBuffer[2].GetSrv())
        .AddPSResource(Sky.GetSrv())
        .AddOMRenderTarget(LitScene.GetRtv())
        .SetOMBlendState(ProportionalBlend)
        .Build();

    VolumetricFogPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                             D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                             nullptr,
                                             DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(VolumetricFogPS)
        .AddPSBuffer(&LightBuffer)
        .AddPSBuffer(&ViewProjectionBuffer)
        .AddPSBuffer(&FogBuffer)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSResource(GBuffer[0].GetSrv())
        .AddPSResource(GBuffer[1].GetSrv())
        .AddPSResource(GBuffer[2].GetSrv())
        .AddPSResource(PrecalcedScattering.GetSrv())
        .AddOMRenderTarget(LitScene.GetRtv())
        .SetOMBlendState(ProportionalBlend)
        .Build();

    AmbientPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                       D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                       nullptr,
                                       DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(AmbientPS)
        .AddPSBuffer(&AmbientBuffer)
        .AddPSBuffer(&ViewProjectionBuffer)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSResource(GBuffer[0].GetSrv())
        .AddPSResource(GBuffer[1].GetSrv())
        .AddPSResource(GBuffer[2].GetSrv())
        .AddPSResource(GBuffer[3].GetSrv())
        .AddPSResource(ReflectionDirections.GetSrv())
        .AddPSResource(ReflectionsHarmonicValues.GetSrv())
        .AddPSResource(&SpecularBrdfResource)
        .AddOMRenderTarget(LitScene.GetRtv())
        .Build();

    SkySamplesPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                          D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                          nullptr,
                                          DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(SkySamplesViewport)
        .SetPS(SkyPS)
        .AddPSBuffer(&LightBuffer)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(PrecalcedScattering.GetSrv())
        .AddPSResource(SphereSamples.GetSrv())
        .AddPSResource(&NightSkyResource)
        .AddOMRenderTarget(SkySamples.GetRtv())
        .Build();

    SkyPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                   D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                   nullptr,
                                   DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(SkyPS)
        .AddPSBuffer(&LightBuffer)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(PrecalcedScattering.GetSrv())
        .AddPSResource(SkyDirections.GetSrv())
        .AddPSResource(&NightSkyResource)
        .AddOMRenderTarget(Sky.GetRtv())
        .Build();

    SkyDirectionsPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                             D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                             nullptr,
                                             DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(SkyDirectionsGS)
        .AddGSBuffer(&ViewProjectionBuffer)
        .AddRSViewport(ScreenViewport)
        .SetPS(SkyDirectionsPS)
        .AddOMRenderTarget(SkyDirections.GetRtv())
        .Build();

    HorizonDirectionsPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                                 D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                 nullptr,
                                                 DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(HorizonGS)
        .AddRSViewport(HorizonViewport)
        .SetPS(SkyDirectionsPS)
        .AddOMRenderTarget(HorizonDirections.GetRtv())
        .Build();

    HorizonPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                       D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                       nullptr,
                                       DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(HorizonViewport)
        .SetPS(SkyPS)
        .AddPSBuffer(&LightBuffer)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(PrecalcedScattering.GetSrv())
        .AddPSResource(HorizonDirections.GetSrv())
        .AddPSResource(&NightSkyResource)
        .AddOMRenderTarget(Horizon.GetRtv())
        .Build();

    auto reflectionsHarmonicValuesPipelineBuilder = TPipelineBuilder(ViewVS.GetLayout(),
                                                                     D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                                     nullptr,
                                                                     DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(HarmonicValuesPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(ReflectionDirections.GetSrv());
    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        reflectionsHarmonicValuesPipelineBuilder.AddOMRenderTarget(ReflectionsHarmonicValues.GetRtv(i));
    }
    ReflectionsHarmonicValuesPipeline = reflectionsHarmonicValuesPipelineBuilder.Build();

    auto harmonicValuesPipelineBuilder = TPipelineBuilder(ViewVS.GetLayout(),
                                                          D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                                          nullptr,
                                                          DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(SkySamplesViewport)
        .SetPS(HarmonicValuesPS)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSResource(SphereSamples.GetSrv());
    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        harmonicValuesPipelineBuilder.AddOMRenderTarget(SphericalHarmonicValues.GetRtv(i));
    }
    HarmonicValuesPipeline = harmonicValuesPipelineBuilder.Build();

    LightPipeline = TPipelineBuilder(ViewVS.GetLayout(),
                                     D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                     nullptr,
                                     DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&SingleZeroIndexBuffer, sizeof(int), 0)
        .SetVS(ViewVS)
        .SetGS(ViewGS)
        .AddRSViewport(ScreenViewport)
        .SetPS(LightPS)
        .AddPSBuffer(&LightBuffer)
        .AddPSBuffer(&ViewProjectionBuffer)
        .AddPSSampler(AllPointZeroSamplerState)
        .AddPSSampler(AllPointClampComparisonSamplerState)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSResource(ShadowMap.GetSrv())
        .AddPSResource(GBuffer[0].GetSrv())
        .AddPSResource(GBuffer[1].GetSrv())
        .AddPSResource(GBuffer[2].GetSrv())
        .AddPSResource(GBuffer[3].GetSrv())
        .AddPSResource(PrecalcedScattering.GetSrv())
        .AddOMRenderTarget(LitScene.GetRtv())
        .SetOMBlendState(AdditiveBlend, nullptr)
        .Build();

    QuadtreeUpdatePipeline = TPipelineBuilder(QuadtreeVS.GetLayout(),
                                              D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
                                              nullptr,
                                              DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&QuadtreeNodesFrontBufferPtr, sizeof(int), 0)
        .SetVS(QuadtreeVS)
        .SetGS(QuadtreeGS)
        .AddGSBuffer(&FocalPointBuffer)
        .AddSOTarget(&QuadtreeNodesBackBufferPtr, 0)
        .Build();

    QuadmapDrawPipeline = TPipelineBuilder(QuadmapVS.GetLayout(),
                                           D3D11_PRIMITIVE_TOPOLOGY_1_CONTROL_POINT_PATCHLIST,
                                           nullptr,
                                           DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&QuadtreeNodesFrontBufferPtr, sizeof(int), 0)
        .SetVS(QuadmapVS)
        .SetHS(QuadmapHS)
        .AddHSBuffer(&FocalPointBuffer)
        .SetDS(QuadmapDS)
        .AddDSBuffer(&TerrainPlacementBuffer)
        .AddDSResource(&TerrainHeightsView)
        .AddDSSampler(AllLinearClampSamplerState)
        .SetGS(QuadmapGS)
        .AddGSBuffer(&ViewProjectionBuffer)
        .SetRSState(&DrawRasterizerState)
        .AddRSViewport(ScreenViewport)
        .SetPS(QuadmapPS)
        .AddPSBuffer(&ViewProjectionBuffer)
        .AddPSResource(&TerrainNormalView)
        .AddPSResource(&TerrainAlbedoView)
        .AddPSResource(&TerrainDetailView)
        .AddPSSampler(AllLinearClampSamplerState)
        .SetOMDepthStencilState(DepthStencilState)
        .SetOMDepthStencil(DepthStencil.GetDsv())
        .AddOMRenderTarget(GBuffer[0].GetRtv())
        .AddOMRenderTarget(GBuffer[1].GetRtv())
        .AddOMRenderTarget(GBuffer[2].GetRtv())
        .AddOMRenderTarget(GBuffer[3].GetRtv())
        .Build();

    QuadmapShadowPipeline = TPipelineBuilder(QuadmapVS.GetLayout(),
                                             D3D11_PRIMITIVE_TOPOLOGY_1_CONTROL_POINT_PATCHLIST,
                                             nullptr,
                                             DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&QuadtreeNodesFrontBufferPtr, sizeof(int), 0)
        .SetVS(QuadmapVS)
        .SetHS(QuadmapHS)
        .AddHSBuffer(&FocalPointBuffer)
        .SetDS(QuadmapDS)
        .AddDSBuffer(&TerrainPlacementBuffer)
        .AddDSResource(&TerrainHeightsView)
        .AddDSSampler(AllLinearClampSamplerState)
        .SetGS(QuadmapGS)
        .AddGSBuffer(&LightViewProjectionBuffer)
        .SetRSState(&ShadowDepthRasterizerState)
        .AddRSViewport(LightScreenViewport)
        .SetOMDepthStencilState(DepthStencilState)
        .SetOMDepthStencil(&ShadowMapDepthViewPtr)
        .Build();

    ObjectDrawPipeline = TPipelineBuilder(ObjectVS.GetLayout(),
                                          D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
                                          &ObjectIndexBufferPtr,
                                          DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&ObjectVertexBufferPtr, sizeof(TModelVertex), 0)
        .SetVS(ObjectVS)
        .AddVSBuffer(&ViewProjectionBuffer)
        .AddVSBuffer(&ObjectTransformationBuffer)
        .SetRSState(&DrawRasterizerState)
        .AddRSViewport(ScreenViewport)
        .SetPS(ObjectPS)
        .AddPSBuffer(&MaterialBufferPtr)
        .AddPSSampler(AllLinearClampSamplerState)
        .AddPSResource(&BaseColorViewPtr)
        .AddPSResource(&DielectricSpecularViewPtr)
        .AddPSResource(&MetalnessViewPtr)
        .AddPSResource(&RoughnessViewPtr)
        .AddPSResource(&HeightViewPtr)
        .SetOMDepthStencilState(DepthStencilState)
        .SetOMDepthStencil(DepthStencil.GetDsv())
        .AddOMRenderTarget(GBuffer[0].GetRtv())
        .AddOMRenderTarget(GBuffer[1].GetRtv())
        .AddOMRenderTarget(GBuffer[2].GetRtv())
        .AddOMRenderTarget(GBuffer[3].GetRtv())
        .Build();

    ObjectShadowPipeline = TPipelineBuilder(ObjectVS.GetLayout(),
                                            D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
                                            &ObjectIndexBufferPtr,
                                            DXGI_FORMAT_R32_UINT)
        .AddVertexBuffer(&ObjectVertexBufferPtr, sizeof(TModelVertex), 0)
        .SetVS(ObjectVS)
        .AddVSBuffer(&LightViewProjectionBuffer)
        .AddVSBuffer(&ObjectTransformationBuffer)
        .SetRSState(&ShadowDepthRasterizerState)
        .AddRSViewport(LightScreenViewport)
        .SetOMDepthStencilState(DepthStencilState)
        .SetOMDepthStencil(&ShadowMapDepthViewPtr)
        .Build();
}

void TRenderer::PrecalcScattering() {
    SetPipeline(PrecalcScatteringPipeline);
    Context->Draw(1, 0);

    SetPipeline(HorizonDirectionsPipeline);
    Context->Draw(1, 0);
}

void TRenderer::InitializeSphereSamples(const unsigned int sizeX,
                                        const unsigned int sizeY,
                                        const bool hemisphereOnly,
                                        TTexture2D* samplesTexture) {
    std::vector<float> samples(sizeX * 4 * sizeY);
    const unsigned int rowDataSize = sizeX * sizeof(float) * 4;
    std::uniform_real_distribution<float> distribution(0, 1);
    std::default_random_engine engine;
    for (unsigned int i = 0; i < sizeX * sizeY * 4; i += 4) {
        const float ksi_x = distribution(engine);
        const float ksi_y = distribution(engine);
        float theta = std::acos(1 - 2 * ksi_x);
        if (hemisphereOnly && theta > PI_CONST / 2) {
            theta = PI_CONST - theta;
        }
        const float phi = 2.0f * PI_CONST * ksi_y;
        samples[i + 0] = std::sin(theta) * std::sin(phi);
        samples[i + 1] = std::sin(theta) * std::cos(phi);
        samples[i + 2] = std::cos(theta);
        samples[i + 3] = 0;
    }

    D3D11_SUBRESOURCE_DATA dataDesc;
    dataDesc.pSysMem = samples.data();
    dataDesc.SysMemPitch = rowDataSize;
    dataDesc.SysMemSlicePitch = 0;

    samplesTexture->Create(sizeX, sizeY, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_USAGE_DEFAULT,
                           1, 1, D3D11_BIND_SHADER_RESOURCE, 0, 0, Device, &dataDesc)
        .AddSrv(Device);
}

void TRenderer::PrecalcHarmonicValues() {
    SetPipeline(HarmonicValuesPipeline);
    Context->Draw(1, 0);
}

void TRenderer::PrecalcReflectanceHarmonics() {
    SetPipeline(DiffuseLightingTermsPipeline);
    Context->Draw(1, 0);
    DiffuseHarmonics = CalculateHarmonicValues(*DiffuseLightingTerms.GetSrv());
}

void TRenderer::PrecalcSpecularBrdfAndDiffuseFresnel() {
    for (int color = 0; color < SPECULAR_REFLECTANCE_NUM_COLOR_VALUES; ++color) {
        for (int angle = 0; angle < SPECULAR_REFLECTANCE_NUM_ANGLE_VALUES; ++angle) {
            for (int roughness = 0; roughness < SPECULAR_REFLECTANCE_NUM_ROUGHNESS_VALUES; ++roughness) {
                D3D11_MAPPED_SUBRESOURCE mapped = {};
                ENSURE_D3D(Context->Map(ReflectanceParametersBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
                           "Failed to map ReflectanceParametersBuffer.");
                float* values = reinterpret_cast<float*>(mapped.pData);
                values[0] = float(color) / (SPECULAR_REFLECTANCE_NUM_COLOR_VALUES - 1);
                values[1] = float(angle) / (SPECULAR_REFLECTANCE_NUM_ANGLE_VALUES - 1);
                values[2] = float(roughness) / (SPECULAR_REFLECTANCE_NUM_ROUGHNESS_VALUES - 1);
                Context->Unmap(ReflectanceParametersBuffer, 0);

                SetPipeline(CalculateSpecularBrdfPipeline);
                Context->Draw(1, 0);
                Context->GenerateMips(*ReflectanceCalculation.GetSrv());
                const D3D11_BOX unitBox = {0, 0, 0, 1, 1, 1};
                Context->CopySubresourceRegion(SpecularBrdf, 0, color, angle, roughness, ReflectanceCalculation.GetResource(),
                                               D3D11CalcSubresource(REFLECTANCE_NUM_SAMPLES_LOG, 0, REFLECTANCE_NUM_SAMPLES_LOG + 1),
                                               &unitBox);
            }
        }
    }
    for (int color = 0; color < DIFFUSE_REFLECTANCE_NUM_COLOR_VALUES; ++color) {
        D3D11_MAPPED_SUBRESOURCE mapped = {};
        ENSURE_D3D(Context->Map(ReflectanceParametersBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
                   "Failed to map ReflectanceParametersBuffer.");
        float* values = reinterpret_cast<float*>(mapped.pData);
        values[0] = float(color) / (DIFFUSE_REFLECTANCE_NUM_COLOR_VALUES - 1);
        Context->Unmap(ReflectanceParametersBuffer, 0);

        SetPipeline(CalculateDiffuseFresnelPipeline);
        Context->Draw(1, 0);
        Context->GenerateMips(*ReflectanceCalculation.GetSrv());
        const D3D11_BOX unitBox = {0, 0, 0, 1, 1, 1};
        Context->CopySubresourceRegion(DiffuseFresnel.GetResource(), 0, color, 0, 0, ReflectanceCalculation.GetResource(),
                                       D3D11CalcSubresource(REFLECTANCE_NUM_SAMPLES_LOG, 0, REFLECTANCE_NUM_SAMPLES_LOG + 1),
                                       &unitBox);
    }
}

void TRenderer::SetPipeline(const TVariablePipeline& pipeline) {
    CurrentPipeline = SwitchPipeline(CurrentPipeline, pipeline, Context);
}

void TRenderer::UpdateViewProjectionBuffer() {
    const auto projectionMatrix = GetJitteredProjectionMatrix(Camera, CurrentFrame, Window->GetWidth(), Window->GetHeight());
    const auto viewMatrix = GetViewMatrix(Camera);
    const auto viewProjectionMatrix = projectionMatrix * viewMatrix;

    const auto inverseProjectionMatrix = GetInverseJitteredProjectionMatrix(Camera, CurrentFrame, Window->GetWidth(), Window->GetHeight());
    const auto inverseViewMatrix = GetInverseViewMatrix(Camera.eye, Camera.lookAt, Camera.up);

    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(ViewProjectionBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map ViewProjectionBuffer.");
    char* data = reinterpret_cast<char*>(mapped.pData);
    std::memcpy(data, projectionMatrix.GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    std::memcpy(data, viewMatrix.GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    std::memcpy(data, viewProjectionMatrix.GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    std::memcpy(data, inverseViewMatrix.GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    std::memcpy(data, inverseProjectionMatrix.GetData(), TTransformation::GetDataSize());
    Context->Unmap(ViewProjectionBuffer, 0);
}

void TRenderer::UpdateLightViewProjectionBuffer(const int shadowPart) {
    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(LightViewProjectionBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map LightViewProjectionBuffer.");
    char* data = reinterpret_cast<char*>(mapped.pData);
    std::memcpy(data, ShadowMapProjectionMatrices[shadowPart].GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    std::memcpy(data, ShadowMapViewMatrices[shadowPart].GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    std::memcpy(data, ShadowMapViewProjectionMatrices[shadowPart].GetData(), TTransformation::GetDataSize());
    // no need to fill two inverse matrices: they are only used in the environment shader for camera view
    Context->Unmap(LightViewProjectionBuffer, 0);
}

void TRenderer::UpdateLightBuffer() {
    const auto cameraViewMatrix = GetViewMatrix(Camera);
    const TVector<4> cameraSpacePosition = cameraViewMatrix * LightPosition;

    const std::vector<float> partitionSchemeZ = CalculatePartitionScheme(Camera);
    const std::vector<float> partitionSchemeProjectedZ = ProjectPartitionScheme(Camera, partitionSchemeZ);

    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(LightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map LightBuffer.");
    char* data = (char*)mapped.pData;

    { // light source characteristics
        std::memcpy(data, cameraSpacePosition.GetData(), cameraSpacePosition.GetDataSize());
        data += cameraSpacePosition.GetDataSize();
        std::memcpy(data, LightPosition.GetData(), LightPosition.GetDataSize());
        data += LightPosition.GetDataSize();
        std::memcpy(data, LightColorAndStrength.GetData(), LightColorAndStrength.GetDataSize());
        data += LightColorAndStrength.GetDataSize();
    }

    { // transformations for all shadow parts
        for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
            std::memcpy(data, ShadowMapProjectionMatrices[shadowPart].GetData(), TTransformation::GetDataSize());
            data += TTransformation::GetDataSize();
        }
        for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
            std::memcpy(data, ShadowMapViewMatrices[shadowPart].GetData(), TTransformation::GetDataSize());
            data += TTransformation::GetDataSize();
        }
        for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
            std::memcpy(data, ShadowMapViewProjectionMatrices[shadowPart].GetData(), TTransformation::GetDataSize());
            data += TTransformation::GetDataSize();
        }
    }

    std::memcpy(data, partitionSchemeProjectedZ.data() + 1, (partitionSchemeProjectedZ.size() - 2) * sizeof(float));
    data += (partitionSchemeProjectedZ.size() - 2) * sizeof(float);

    Context->Unmap(LightBuffer, 0);
}

void TRenderer::UpdateAmbientBuffer(const TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS>& red,
                                    const TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS>& green,
                                    const TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS>& blue) {
    D3D11_MAPPED_SUBRESOURCE mapped = {};
    ENSURE_D3D(Context->Map(AmbientBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map AmbientBuffer.");
    char* data = (char*)mapped.pData;

    std::memcpy(data, reinterpret_cast<const char*>(red.GetData()), red.GetDataSize());
    data += SPHERICAL_HARMONICS_ARRAY_SIZE * 4 * sizeof(float);
    std::memcpy(data, reinterpret_cast<const char*>(green.GetData()), green.GetDataSize());
    data += SPHERICAL_HARMONICS_ARRAY_SIZE * 4 * sizeof(float);
    std::memcpy(data, reinterpret_cast<const char*>(blue.GetData()), blue.GetDataSize());
    data += SPHERICAL_HARMONICS_ARRAY_SIZE * 4 * sizeof(float);
    std::memcpy(data, reinterpret_cast<const char*>(DiffuseHarmonics.GetData()), DiffuseHarmonics.GetDataSize());
    data += SPHERICAL_HARMONICS_ARRAY_SIZE * 4 * sizeof(float);

    Context->Unmap(AmbientBuffer, 0);
}

void TRenderer::UpdateFocalPointBuffer() {
    const float focalPointX = (PositionX - TERRAIN_LRDU[0]) / (TERRAIN_LRDU[1] - TERRAIN_LRDU[0]);
    const float focalPointY = (PositionY - TERRAIN_LRDU[2]) / (TERRAIN_LRDU[3] - TERRAIN_LRDU[2]);
    if (focalPointX != FocalPointX || focalPointY != FocalPointY || HorizontalFov != Camera.horizontalAov) {
        FocalPointX = focalPointX;
        FocalPointY = focalPointY;
        HorizontalFov = Camera.horizontalAov;
        float focalPointData[] = {FocalPointX, FocalPointY, HorizontalFov, 0};

        D3D11_MAPPED_SUBRESOURCE mappedResource;
        ENSURE_D3D(Context->Map(FocalPointBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource),
                   "Failed to map focal point buffer for a quadtree.");
        std::memcpy(mappedResource.pData, focalPointData, sizeof(focalPointData));
        Context->Unmap(FocalPointBuffer, 0);
    }
}

void TRenderer::UpdateFogBuffer() {
    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(FogBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map FogBuffer.");
    const float data[] = {
        FogColor(0),
        FogColor(1),
        FogColor(2),
        VolumetricFogDensity,
        VolumetricFogHeight,
        DistanceFogStart,
        Camera.farClip
    };
    std::memcpy(mapped.pData, data, sizeof(data));
    Context->Unmap(FogBuffer, 0);
}

void TRenderer::UpdateTaaBuffer() {
    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(TaaBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map TaaBuffer.");
    char* data = reinterpret_cast<char*>(mapped.pData);
    std::memcpy(data, PreviousFrameViewProjectionMatrix.GetData(), TTransformation::GetDataSize());
    data += TTransformation::GetDataSize();
    Context->Unmap(TaaBuffer, 0);
}

void TRenderer::UpdateHdrBuffer(const float averageLuma) {
    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(HdrBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map HdrBuffer.");
    char* data = reinterpret_cast<char*>(mapped.pData);
    std::memcpy(data, &averageLuma, sizeof(float));
    data += sizeof(float);
    Context->Unmap(HdrBuffer, 0);
}

void TRenderer::UpdateObjectTransformationBuffer(const TTransformation& transformation) {
    D3D11_MAPPED_SUBRESOURCE mapped;
    ENSURE_D3D(Context->Map(ObjectTransformationBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped),
               "Can't map ObjectTransformationBuffer.");
    std::memcpy(mapped.pData, transformation.GetData(), transformation.GetDataSize()); // TODO: no copy needed?
    Context->Unmap(ObjectTransformationBuffer, 0);
}

void TRenderer::CalculateShadowMapSetup() {
    const std::vector<float> partitionSchemeZ = CalculatePartitionScheme(Camera);
    const std::vector<float> partitionSchemeProjectedZ = ProjectPartitionScheme(Camera, partitionSchemeZ);
    const TVector<3> lightPosition = Get3DVector(LightPosition);
    for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
        CalculateLightMVP(lightPosition,
                          partitionSchemeProjectedZ,
                          shadowPart,
                          Camera,
                          CurrentFrame,
                          Window->GetWidth(),
                          Window->GetHeight(),
                          &ShadowMapProjectionMatrices[shadowPart],
                          &ShadowMapViewMatrices[shadowPart],
                          &ShadowMapViewProjectionMatrices[shadowPart]);
    }
}

void TRenderer::CalculateVolumetricFogColor() {
    Context->CopyResource(HorizonStaging.GetResource(), Horizon.GetResource());
    D3D11_MAPPED_SUBRESOURCE mapped = {};
    ENSURE_D3D(Context->Map(HorizonStaging.GetResource(), 0, D3D11_MAP_READ, 0, &mapped),
               "Failed to map HorizonStaging.");
    const char* data = reinterpret_cast<const char*>(mapped.pData);
    TVector<3> colorSum = TVector<3>::Zero();
    for (int y = 0; y < HORIZON_RESOLUTION_Y; ++y) {
        const float* row = reinterpret_cast<const float*>(data);
        for (int x = 0; x < HORIZON_RESOLUTION_X; ++x) {
            for (int i = 0; i < 3; ++i) {
                colorSum(i) += row[x * 4 + i];
            }
        }
        data += mapped.RowPitch;
    }
    FogColor = colorSum / (HORIZON_RESOLUTION_X * HORIZON_RESOLUTION_Y);
    Context->Unmap(HorizonStaging.GetResource(), 0);
}

TVector<3> TRenderer::CalculateAverageColor(ID3D11Texture2D* resource) {
    const D3D11_BOX box = {0, 0, 0, (UINT)Window->GetWidth(), (UINT)Window->GetHeight(), 1};
    Context->CopySubresourceRegion(AverageColorCalculation.GetResource(), 0, 0, 0, 0, resource, 0, &box);
    Context->GenerateMips(*AverageColorCalculation.GetSrv());

    SetPipeline(ConvertPixelPipeline);
    Context->Draw(1, 0);

    const D3D11_BOX unitBox = {0, 0, 0, 1, 1, 1};
    Context->CopySubresourceRegion(AverageColorCalculationStaging.GetResource(), 0, 0, 0, 0, AverageColorImage.GetResource(), 0, &unitBox);

    D3D11_MAPPED_SUBRESOURCE mapped = {};
    ENSURE_D3D(Context->Map(AverageColorCalculationStaging.GetResource(), 0, D3D11_MAP_READ, 0, &mapped),
               "Failed to map AverageColorCalculationStaging.");
    const float* data = reinterpret_cast<const float*>(mapped.pData);
    TVector<3> result;
    for (int i = 0; i < 3; ++i) {
        result(i) = data[i];
    }
    Context->Unmap(AverageColorCalculationStaging.GetResource(), 0);
    return result;
}

TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS> TRenderer::CalculateHarmonicValues(ID3D11ShaderResourceView* resource) {
    FunctionToHarmonizeResourcePtr = resource;
    SetPipeline(MultiplyByHarmonicsPipeline);
    Context->Draw(1, 0);
    Context->GenerateMips(*SphericalHarmonicsCalculation.GetSrv());

    for (int i = 0; i < SPHERICAL_HARMONICS_ARRAY_SIZE; ++i) {
        const D3D11_BOX unitBox = {0, 0, 0, 1, 1, 1};
        Context->CopySubresourceRegion(SphericalHarmonicsCalculationStaging.GetResource(),
                                       0,
                                       i,
                                       0,
                                       0,
                                       SphericalHarmonicsCalculation.GetResource(),
                                       D3D11CalcSubresource(SPHERICAL_HARMONICS_SIZE_LOG, i, SPHERICAL_HARMONICS_SIZE_LOG + 1),
                                       &unitBox);
    }
    D3D11_MAPPED_SUBRESOURCE mapped = {};
    ENSURE_D3D(Context->Map(SphericalHarmonicsCalculationStaging.GetResource(), 0, D3D11_MAP_READ, 0, &mapped),
               "Failed to map SphericalHarmonicsCalculationStaging.");
    const float* data = reinterpret_cast<const float*>(mapped.pData);
    TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS> result;
    for (int i = 0; i < SPHERICAL_HARMONICS_NUM_COEFFICIENTS; ++i) {
        result(i) = data[i];
    }
    Context->Unmap(SphericalHarmonicsCalculationStaging.GetResource(), 0);
    return result * (4 * PI_CONST);
}

void TRenderer::DrawObject(const TVariablePipeline& pipeline, const TStorageItemId modelId, const TTransformation& transformation) {
    UpdateObjectTransformationBuffer(transformation);
    int numIndices;
    TStorageItemId materialId;
    AssetLibrary->GetModel(modelId, &numIndices, &ObjectIndexBufferPtr, &ObjectVertexBufferPtr, &materialId);
    AssetLibrary->GetMaterial(materialId, &MaterialBufferPtr, &BaseColorViewPtr, &DielectricSpecularViewPtr, &MetalnessViewPtr, &RoughnessViewPtr, &HeightViewPtr);
    SetPipeline(pipeline);
    Context->DrawIndexed(numIndices, 0, 0);
}

void TRenderer::Render() {
    SetPipeline(DefaultPipeline);
    Context->ClearRenderTargetView(BackBufferRenderTargetView, ZERO_COLOR);
    Context->ClearRenderTargetView(*GBuffer[0].GetRtv(), ZERO_COLOR);
    Context->ClearRenderTargetView(*GBuffer[1].GetRtv(), ZERO_COLOR);
    Context->ClearRenderTargetView(*GBuffer[2].GetRtv(), ZERO_COLOR);
    Context->ClearRenderTargetView(*GBuffer[3].GetRtv(), ZERO_COLOR);
    Context->ClearDepthStencilView(*DepthStencil.GetDsv(), D3D11_CLEAR_DEPTH, 1.0f, 0);
    for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
        Context->ClearDepthStencilView(*ShadowMap.GetDsv(shadowPart), D3D11_CLEAR_DEPTH, 1.0f, 0);
    }

    CalculateShadowMapSetup();
    UpdateFocalPointBuffer();
    UpdateViewProjectionBuffer();
    UpdateLightBuffer();

    TTransformation CurrentFrameViewProjectionMatrix = GetProjectionMatrix(Camera) * GetViewMatrix(Camera);
    if (!HaveHistoryFrames) {
        PreviousFrameViewProjectionMatrix = CurrentFrameViewProjectionMatrix;
    }
    UpdateTaaBuffer();

    // update structures
    int numQuadtreeUpdates = HasQuadtreeBeenUpdatedAtLeastOnce ? 1 : QUADTREE_MAX_LEVEL;
    while (numQuadtreeUpdates--) {
        SetPipeline(QuadtreeUpdatePipeline);
        if (HasQuadtreeBeenUpdatedAtLeastOnce) {
            Context->DrawAuto();
            std::swap(QuadtreeNodesFrontBufferPtr, QuadtreeNodesBackBufferPtr);
        } else {
            Context->Draw(1, 0);
            HasQuadtreeBeenUpdatedAtLeastOnce = true;
            QuadtreeNodesFrontBufferPtr = QuadtreeNodesMainBuffers[0];
            QuadtreeNodesBackBufferPtr = QuadtreeNodesMainBuffers[1];
        }
    }

    // draw the scene onto GBuffer (for each object)
    SetPipeline(QuadmapDrawPipeline);
    Context->DrawAuto();
    for (int objectId = 0; objectId < StagedObjectsModelIds.size(); ++objectId) {
        DrawObject(ObjectDrawPipeline, StagedObjectsModelIds[objectId], StagedObjectsTransformations[objectId]);
    }

    // calculate reflections
    SetPipeline(ReflectionsPipeline);
    Context->Draw(1, 0);
    SetPipeline(ReflectionsHarmonicValuesPipeline);
    Context->Draw(1, 0);

    // sky samples
    SetPipeline(SkySamplesPipeline);
    Context->Draw(1, 0);
    SetPipeline(SeparateRGBPipeline);
    Context->Draw(1, 0);
    TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS> skyLightHarmonics[3];
    for (int i = 0; i < 3; ++i) {
        skyLightHarmonics[i] = CalculateHarmonicValues(*SeparatedRGB.GetSrv(i));
    }
    UpdateAmbientBuffer(skyLightHarmonics[0], skyLightHarmonics[1], skyLightHarmonics[2]);

    // horizon color
    SetPipeline(HorizonPipeline);
    Context->Draw(1, 0);
    CalculateVolumetricFogColor();
    UpdateFogBuffer();

    // sky light
    SetPipeline(SkyDirectionsPipeline);
    Context->Draw(1, 0);
    SetPipeline(SkyPipeline);
    Context->Draw(1, 0);

    // ambient light
    SetPipeline(AmbientPipeline);
    Context->Draw(1, 0);

    // light the scene (for each light)
    for (int lightId = 0; lightId < 1; ++lightId) {
        // draw each object onto the shadow map
        for (int shadowPart = 0; shadowPart < NUM_SHADOW_MAPS; ++shadowPart) {
            UpdateLightViewProjectionBuffer(shadowPart);
            ShadowMapDepthViewPtr = *ShadowMap.GetDsv(shadowPart);

            SetPipeline(QuadmapShadowPipeline);
            Context->DrawAuto();
            // draw a plane under the quadmap
            const TTransformation TerrainBottomTransformation = GetTranslationMatrix({TERRAIN_LRDU[0], TERRAIN_LRDU[2], 0}) *
                                                                GetScalingMatrix({TERRAIN_LRDU[1] - TERRAIN_LRDU[0], TERRAIN_LRDU[3] - TERRAIN_LRDU[2], 1}) *
                                                                GetRotationMatrix({1, 0, 0}, PI_CONST);
            DrawObject(ObjectShadowPipeline, PlaneModelId, TerrainBottomTransformation);

            for (int objectId = 0; objectId < StagedObjectsModelIds.size(); ++objectId) {
                DrawObject(ObjectShadowPipeline, StagedObjectsModelIds[objectId], StagedObjectsTransformations[objectId]);
            }
        }

        // apply light
        SetPipeline(LightPipeline);
        Context->Draw(1, 0);
    }

    // volumetric fog
    SetPipeline(VolumetricFogPipeline);
    Context->Draw(1, 0);

    // global fog
    SetPipeline(DistanceFogPipeline);
    Context->Draw(1, 0);

    // TAA
    if (HaveHistoryFrames) {
        TaaHistoryResourcePtr = *TaaFrames[TaaFrontBufferIndex].GetSrv();
        TaaNewFrameResourcePtr = *LitScene.GetSrv();
        TaaBlendedTargetPtr = *TaaFrames[1 - TaaFrontBufferIndex].GetRtv();
        TaaFrontBufferIndex = 1 - TaaFrontBufferIndex;
        SetPipeline(TaaPipeline);
        Context->Draw(1, 0);
        PreviousFrameViewProjectionMatrix = CurrentFrameViewProjectionMatrix;
    } else {
        Context->CopyResource(LitScene.GetResource(), TaaFrames[TaaFrontBufferIndex].GetResource());
    }

    const TVector<3> averageColor = CalculateAverageColor(TaaFrames[TaaFrontBufferIndex].GetResource());
    const float averageLuma = CalcScalarProduct(averageColor, {0.2126f, 0.7152f, 0.0722f});
    UpdateHdrBuffer(averageLuma);

    // adapt HDR to sRGB
    NormalizeHDRInputResourcePtr = *TaaFrames[TaaFrontBufferIndex].GetSrv();
    SetPipeline(NormalizeHDRPipeline);
    Context->Draw(1, 0);

    // view
    ViewInputResourcePtr = *NormalizedHdr.GetSrv();
    SetPipeline(ViewPipeline);
    Context->Draw(1, 0);
}

bool TRenderer::GetIsIdle() const {
    return IsIdle;
}

void TRenderer::RenderAndUpdateIsIdle() {
    HRESULT presentResult;
    if (IsIdle) {
        // to check if the window is visible
        presentResult = SwapChain->Present(0, DXGI_PRESENT_TEST);
    } else {
        Render();
        presentResult = SwapChain->Present(1, 0);
        ++CurrentFrame;
        HaveHistoryFrames = true;
    }
    IsIdle = ConvertPresentResultToIsIdle(presentResult);
}

void TRenderer::UpdatePosition(const float positionX, const float positionY) {
    PositionX = positionX;
    PositionY = positionY;
}

void TRenderer::UpdateCamera(const TCamera& camera) {
    Camera = camera;
}

void TRenderer::SetFog(const float volumetricFogDensity, float volumetricFogHeight, float distanceFogStart) {
    VolumetricFogDensity = volumetricFogDensity;
    VolumetricFogHeight = volumetricFogHeight;
    DistanceFogStart = distanceFogStart;
}

void TRenderer::SetLight(const TVector<3>& lightPosition, const TVector<4>& lightColorAndStrength) {
    for (int i = 0; i < 3; ++i) {
        LightPosition(i) = lightPosition(i);
    }
    LightColorAndStrength = lightColorAndStrength;
}

void TRenderer::StageObject(const TStorageItemId modelId, const TTransformation& transformation) {
    StagedObjectsModelIds.push_back(modelId);
    StagedObjectsTransformations.push_back(transformation);
}

void TRenderer::UnstageAllObjects() {
    StagedObjectsModelIds.clear();
    StagedObjectsTransformations.clear();
}

bool TRenderer::ReadTiffTextureFromFile(const wchar_t* path,
                                        const bool generateMipMaps,
                                        ID3D11Texture2D** texture,
                                        ID3D11ShaderResourceView** view) {
    const auto hr = CreateWICTextureFromFile(Device, generateMipMaps ? Context : nullptr, path, (ID3D11Resource**)texture, view, 0);
    if (hr != S_OK) {
       LOG("Error: Failed to create WIC texture from file.");
       return false;
    }
    return true;
}
