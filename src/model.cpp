#include "error.h"
#include "log.h"
#include "model.h"
#include "str.h"
#include <fstream>
#include <map>
#include <tuple>
#include <utility>

namespace {
    struct TDataV {
        float x;
        float y;
        float z;
    };

    struct TDataVT {
        float u;
        float v;
    };

    struct TDataVN {
        float x;
        float y;
        float z;
    };

    struct TDataF {
        int v[3];
        int vt[3];
        int vn[3];
    };

    struct TModelInConstruction {
        std::string o;
        std::vector<TDataV> v;
        std::vector<TDataVT> vt;
        std::vector<TDataVN> vn;
        std::vector<TDataF> f;
        std::string material = "";
    };

    TModel ConstructModelFromObjData(const TModelInConstruction& model) {
        std::vector<unsigned int> indicesData;
        std::vector<TModelVertex> verticesData;

        std::map<std::tuple<int, int, int>, int> indexTriples;
        for (const auto& face : model.f) {
            for (int i = 0; i < 3; ++i) {
                std::tuple<int, int, int> triple(face.v[i], face.vt[i], face.vn[i]);
                auto iter = indexTriples.find(triple);
                if (iter != indexTriples.end()) {
                    indicesData.push_back(iter->second);
                    continue;
                }
                const unsigned int newIndex = static_cast<unsigned int>(indexTriples.size());
                indicesData.push_back(newIndex);
                indexTriples[triple] = newIndex;
                verticesData.emplace_back(TModelVertex{model.v[face.v[i]].x,
                                                       model.v[face.v[i]].z,
                                                       model.v[face.v[i]].y,
                                                       model.vt[face.vt[i]].u,
                                                       1 - model.vt[face.vt[i]].v,
                                                       model.vn[face.vn[i]].x,
                                                       model.vn[face.vn[i]].z,
                                                       model.vn[face.vn[i]].y});
            }
        }

        return TModel{std::string(model.o), std::move(indicesData), std::move(verticesData)};
    }
} // anonymous


std::vector<std::pair<TModel, TMaterialDependency>> ReadModelsFromObj(const std::filesystem::path& path) {
    std::string mtllib = "";
    TModelInConstruction model;

    std::ifstream inputFile(path);
    std::string fileLine;

    std::vector<std::pair<TModel, TMaterialDependency>> result;

    bool readFirstModelStatement = false;
    int lineNum = 0;
    while (std::getline(inputFile, fileLine)) {
        ++lineNum;
        fileLine = StripLine(fileLine);
        if (fileLine.empty() || fileLine[0] == '#') {
            continue;
        }
        const auto tokens = SplitLine(fileLine);
        const std::string& statement = tokens[0]; // tokens not empty since fileLine.empty() == false
        if (statement == "mtllib") {
            ENSURE(tokens.size() == 2, "Invalid model format line " << lineNum << ", file " << path << ".");
            mtllib = tokens[1];
        } else if (statement == "usemtl") {
            ENSURE(tokens.size() == 2, "Invalid model format line " << lineNum << ", file " << path << ".");
            model.material = tokens[1];
        } else if (statement == "o") {
            if (readFirstModelStatement) {
                result.emplace_back(ConstructModelFromObjData(model), TMaterialDependency{mtllib, model.material});
            }
            readFirstModelStatement = true;
            model = TModelInConstruction();
            ENSURE(tokens.size() == 2, "Invalid model format line " << lineNum << ", file " << path << ".");
            model.o = tokens[1];
        } else if (statement == "v") {
            ENSURE(tokens.size() == 4, "Invalid model format line " << lineNum << ", file " << path << ".");
            model.v.emplace_back(TDataV{std::stof(tokens[1]),
                                        std::stof(tokens[2]),
                                        std::stof(tokens[3])});
        } else if (statement == "vt") {
            ENSURE(tokens.size() == 3, "Invalid model format line " << lineNum << ", file " << path << ".");
            model.vt.emplace_back(TDataVT{std::stof(tokens[1]),
                                          std::stof(tokens[2])});
        } else if (statement == "vn") {
            ENSURE(tokens.size() == 4, "Invalid model format line " << lineNum << ", file " << path << ".");
            model.vn.emplace_back(TDataVN{std::stof(tokens[1]),
                                          std::stof(tokens[2]),
                                          std::stof(tokens[3])});
        } else if (statement == "f") {
            ENSURE(tokens.size() >= 4, "Invalid face in line " << lineNum << ", file " << path << ". Only triangle faces are supported.");
            if (tokens.size() > 4) {
                LOG_WARNING("Invalid face in line " << lineNum << ", file " << path << ". Only triangle faces are supported.\n");
            }
            TDataF& face = model.f.emplace_back();
            for (int i = 0; i < 3; ++i) {
                const auto indices = SplitLine(tokens[i + 1], '/');
                ENSURE(indices.size() == 3, "Invalid model format line " << lineNum << ", file " << path << ".");
                face.v[i] = std::stoi(indices[0]) - 1;
                face.vt[i] = std::stoi(indices[1]) - 1;
                face.vn[i] = std::stoi(indices[2]) - 1;
            }
        } else {
            LOG_WARNING("Don't know what to do with statement '" << statement << "' while reading model in line " << lineNum << ", file " << path << ".\n");
        }
    }
    if (readFirstModelStatement) {
        result.emplace_back(ConstructModelFromObjData(model), TMaterialDependency{mtllib, model.material});
    }
    return result;
}

TModel GetCubeModel() {
    TModel model {
        "cube",
        {3, 1, 0,
         0, 2, 3,
         4, 5, 7,
         7, 6, 4,
         0, 1, 5,
         5, 4, 0,
         7, 3, 2,
         2, 6, 7,
         6, 2, 0,
         0, 4, 6,
         1, 3, 7,
         7, 5, 1},
        {{-1.0f, -1.0f, -1.0f, 0, 0, -1.0f, -1.0f, -1.0f},
         {-1.0f, -1.0f, +1.0f, 0, 0, -1.0f, -1.0f, +1.0f},
         {-1.0f, +1.0f, -1.0f, 0, 0, -1.0f, +1.0f, -1.0f},
         {-1.0f, +1.0f, +1.0f, 0, 0, -1.0f, +1.0f, +1.0f},
         {+1.0f, -1.0f, -1.0f, 0, 0, +1.0f, -1.0f, -1.0f},
         {+1.0f, -1.0f, +1.0f, 0, 0, +1.0f, -1.0f, +1.0f},
         {+1.0f, +1.0f, -1.0f, 0, 0, +1.0f, +1.0f, -1.0f},
         {+1.0f, +1.0f, +1.0f, 0, 0, +1.0f, +1.0f, +1.0f}}
    };
    return model;
}
