#include "error.h"
#include "pipeline.h"


namespace {
    template <typename T>
    std::vector<T*> FixPointers(const std::vector<T**> pointers) {
        std::vector<T*> values;
        values.reserve(pointers.size());
        for (auto* pointer : pointers) {
            if (pointer == nullptr) {
                values.emplace_back(nullptr);
            } else {
                values.emplace_back(*pointer);
            }
        }
        return std::move(values);
    }

    template <typename T, void (ID3D11DeviceContext::*Method)(UINT, UINT, T* const*)>
    void ResetInputs(const std::vector<T*>& oldInputs, const std::vector<T*>& newInputs, ID3D11DeviceContext* context) {
        if (oldInputs != newInputs) {
            (context->*Method)(0,
                               static_cast<UINT>(newInputs.size()),
                               newInputs.data());
            if (newInputs.size() < oldInputs.size()) {
                std::vector<T*> nullptrArray(oldInputs.size() - newInputs.size(), nullptr);
                (context->*Method)(static_cast<UINT>(newInputs.size()),
                                   static_cast<UINT>(nullptrArray.size()),
                                   nullptrArray.data());
            }
        }
    }
} // anonymous


TVariablePipeline TPipelineBuilder::Build() {
    return Pipeline;
}

TPipelineBuilder::TPipelineBuilder(ID3D11InputLayout* inputLayout,
                                   D3D11_PRIMITIVE_TOPOLOGY primitiveTopology,
                                   ID3D11Buffer** indexBuffer,
                                   DXGI_FORMAT indexBufferFormat,
                                   unsigned int indexBufferOffset) {
    Pipeline.inputLayout = inputLayout;
    Pipeline.primitiveTopology = primitiveTopology;
    Pipeline.indexBuffer = indexBuffer;
    Pipeline.indexBufferFormat = indexBufferFormat;
    Pipeline.indexBufferOffset = indexBufferOffset;
}

TPipelineBuilder& TPipelineBuilder::AddVertexBuffer(ID3D11Buffer** buffer,
                                                    unsigned int stride,
                                                    unsigned int offset) {
    Pipeline.vertexBuffers.push_back(buffer);
    Pipeline.vertexBufferStrides.push_back(stride);
    Pipeline.vertexBufferOffsets.push_back(offset);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetVS(TVertexShader& shader) {
    Pipeline.vertexShader = shader.GetShader();
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddVSBuffer(ID3D11Buffer** buffer) {
    Pipeline.vertexShaderConstantBuffers.push_back(buffer);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddVSSampler(ID3D11SamplerState* sampler) {
    Pipeline.vertexShaderSamplers.push_back(sampler);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddVSResource(ID3D11ShaderResourceView** resource) {
    Pipeline.vertexShaderResources.push_back(resource);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetHS(THullShader& shader) {
    Pipeline.hullShader = shader.GetShader();
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddHSBuffer(ID3D11Buffer** buffer) {
    Pipeline.hullShaderConstantBuffers.push_back(buffer);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddHSSampler(ID3D11SamplerState* sampler) {
    Pipeline.hullShaderSamplers.push_back(sampler);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddHSResource(ID3D11ShaderResourceView** resource) {
    Pipeline.hullShaderResources.push_back(resource);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetDS(TDomainShader& shader) {
    Pipeline.domainShader = shader.GetShader();
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddDSBuffer(ID3D11Buffer** buffer) {
    Pipeline.domainShaderConstantBuffers.push_back(buffer);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddDSSampler(ID3D11SamplerState* sampler) {
    Pipeline.domainShaderSamplers.push_back(sampler);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddDSResource(ID3D11ShaderResourceView** resource) {
    Pipeline.domainShaderResources.push_back(resource);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetGS(TGeometryShader& shader) {
    Pipeline.geometryShader = shader.GetShader();
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddGSBuffer(ID3D11Buffer** buffer) {
    Pipeline.geometryShaderConstantBuffers.push_back(buffer);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddGSSampler(ID3D11SamplerState* sampler) {
    Pipeline.geometryShaderSamplers.push_back(sampler);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddGSResource(ID3D11ShaderResourceView** resource) {
    Pipeline.geometryShaderResources.push_back(resource);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddSOTarget(ID3D11Buffer** buffer, unsigned int offset) {
    Pipeline.streamOutputTargets.push_back(buffer);
    Pipeline.streamOutputTargetOffsets.push_back(offset);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetRSState(ID3D11RasterizerState** state) {
    Pipeline.rasterizerState = state;
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddRSViewport(const D3D11_VIEWPORT& viewport) {
    Pipeline.rasterizerViewports.push_back(viewport);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetPS(TPixelShader& shader) {
    Pipeline.pixelShader = shader.GetShader();
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddPSBuffer(ID3D11Buffer** buffer) {
    Pipeline.pixelShaderConstantBuffers.push_back(buffer);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddPSSampler(ID3D11SamplerState* sampler) {
    Pipeline.pixelShaderSamplers.push_back(sampler);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddPSResource(ID3D11ShaderResourceView** resource) {
    Pipeline.pixelShaderResources.push_back(resource);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetOMDepthStencilState(ID3D11DepthStencilState* state, unsigned int ref) {
    Pipeline.depthStencilState = state;
    Pipeline.depthStencilRef = ref;
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetOMDepthStencil(ID3D11DepthStencilView** depthStencil) {
    Pipeline.depthStencilView = depthStencil;
    return *this;
}

TPipelineBuilder& TPipelineBuilder::AddOMRenderTarget(ID3D11RenderTargetView** renderTarget) {
    Pipeline.renderTargetViews.push_back(renderTarget);
    return *this;
}

TPipelineBuilder& TPipelineBuilder::SetOMBlendState(ID3D11BlendState* blendState,
                                                    const float* blendFactor,
                                                    unsigned int blendSampleMask) {
    Pipeline.blendState = blendState;
    if (blendFactor == nullptr) {
        for (int i = 0; i < 4; ++i) {
            Pipeline.blendFactor[i] = 1;
        }
    } else {
        std::memcpy(Pipeline.blendFactor, blendFactor, 4 * sizeof(float));
    }
    Pipeline.blendSampleMask = blendSampleMask;
    return *this;
}

TFixedPipeline FixPipeline(const TVariablePipeline& variablePipeline) {
    TFixedPipeline fixedPipeline;
    // IA
    fixedPipeline.inputLayout = variablePipeline.inputLayout;
    fixedPipeline.primitiveTopology = variablePipeline.primitiveTopology;
    if (variablePipeline.indexBuffer != nullptr) {
        fixedPipeline.indexBuffer = *variablePipeline.indexBuffer;
    }
    fixedPipeline.indexBufferFormat = variablePipeline.indexBufferFormat;
    fixedPipeline.indexBufferOffset = variablePipeline.indexBufferOffset;
    for (auto* vertexBuffer : variablePipeline.vertexBuffers) {
        fixedPipeline.vertexBuffers.push_back(*vertexBuffer);
    }
    fixedPipeline.vertexBufferStrides = variablePipeline.vertexBufferStrides;
    fixedPipeline.vertexBufferOffsets = variablePipeline.vertexBufferOffsets;
    // VS
    fixedPipeline.vertexShader = variablePipeline.vertexShader;
    fixedPipeline.vertexShaderConstantBuffers = FixPointers(variablePipeline.vertexShaderConstantBuffers);
    fixedPipeline.vertexShaderSamplers = variablePipeline.vertexShaderSamplers;
    for (auto* resource : variablePipeline.vertexShaderResources) {
        fixedPipeline.vertexShaderResources.push_back(*resource);
    }
    // HS
    fixedPipeline.hullShader = variablePipeline.hullShader;
    fixedPipeline.hullShaderConstantBuffers = FixPointers(variablePipeline.hullShaderConstantBuffers);
    fixedPipeline.hullShaderSamplers = variablePipeline.hullShaderSamplers;
    for (auto* resource : variablePipeline.hullShaderResources) {
        fixedPipeline.hullShaderResources.push_back(*resource);
    }
    // DS
    fixedPipeline.domainShader = variablePipeline.domainShader;
    fixedPipeline.domainShaderConstantBuffers = FixPointers(variablePipeline.domainShaderConstantBuffers);
    fixedPipeline.domainShaderSamplers = variablePipeline.domainShaderSamplers;
    for (auto* resource : variablePipeline.domainShaderResources) {
        fixedPipeline.domainShaderResources.push_back(*resource);
    }
    // GS
    fixedPipeline.geometryShader = variablePipeline.geometryShader;
    fixedPipeline.geometryShaderConstantBuffers = FixPointers(variablePipeline.geometryShaderConstantBuffers);
    fixedPipeline.geometryShaderSamplers = variablePipeline.geometryShaderSamplers;
    for (auto* resource : variablePipeline.geometryShaderResources) {
        fixedPipeline.geometryShaderResources.push_back(*resource);
    }
    for (auto* streamOutputTarget : variablePipeline.streamOutputTargets) {
        fixedPipeline.streamOutputTargets.push_back(*streamOutputTarget);
    }
    fixedPipeline.streamOutputTargetOffsets = variablePipeline.streamOutputTargetOffsets;
    // RS
    if (variablePipeline.rasterizerState != nullptr) {
        fixedPipeline.rasterizerState = *variablePipeline.rasterizerState;
    }
    fixedPipeline.rasterizerViewports = variablePipeline.rasterizerViewports;
    // PS
    fixedPipeline.pixelShader = variablePipeline.pixelShader;
    fixedPipeline.pixelShaderConstantBuffers = FixPointers(variablePipeline.pixelShaderConstantBuffers);
    fixedPipeline.pixelShaderSamplers = variablePipeline.pixelShaderSamplers;
    for (auto* resource : variablePipeline.pixelShaderResources) {
        fixedPipeline.pixelShaderResources.push_back(*resource);
    }
    // OM
    fixedPipeline.depthStencilState = variablePipeline.depthStencilState;
    fixedPipeline.depthStencilRef = variablePipeline.depthStencilRef;
    if (variablePipeline.depthStencilView != nullptr) {
        fixedPipeline.depthStencilView = *variablePipeline.depthStencilView;
    }
    for (auto* renderTargetView : variablePipeline.renderTargetViews) {
        fixedPipeline.renderTargetViews.push_back(*renderTargetView);
    }
    fixedPipeline.blendState = variablePipeline.blendState;
    std::memcpy(fixedPipeline.blendFactor, variablePipeline.blendFactor, 4 * sizeof(float));
    fixedPipeline.blendSampleMask = variablePipeline.blendSampleMask;
    return fixedPipeline;
}

TFixedPipeline SwitchPipeline(const TFixedPipeline& oldPipeline,
                              const TVariablePipeline& variableNewPipeline,
                              ID3D11DeviceContext* context) {
    TFixedPipeline newPipeline = FixPipeline(variableNewPipeline);

    // first, we unbind all targets, so that we can use them as inputs
    // unbind SO targets
    bool changeStreamOutputTargets =
        oldPipeline.streamOutputTargets != newPipeline.streamOutputTargets ||
        oldPipeline.streamOutputTargetOffsets != newPipeline.streamOutputTargetOffsets;
    if (changeStreamOutputTargets && oldPipeline.streamOutputTargets.size() > 0) {
        context->SOSetTargets(0, nullptr, nullptr);
    }
    // unbind OM targets
    bool changeRenderTargets =
        oldPipeline.depthStencilView != newPipeline.depthStencilView ||
        oldPipeline.renderTargetViews != newPipeline.renderTargetViews;
    if (changeRenderTargets) {
        context->OMSetRenderTargets(0, nullptr, nullptr);
    }


    // now we set all except targets
    // IA
    if (oldPipeline.inputLayout != newPipeline.inputLayout) {
        context->IASetInputLayout(newPipeline.inputLayout);
    }
    if (oldPipeline.primitiveTopology != newPipeline.primitiveTopology) {
        context->IASetPrimitiveTopology(newPipeline.primitiveTopology);
    }
    if (oldPipeline.indexBuffer != newPipeline.indexBuffer ||
        oldPipeline.indexBufferFormat != newPipeline.indexBufferFormat ||
        oldPipeline.indexBufferOffset != newPipeline.indexBufferOffset) {
        context->IASetIndexBuffer(newPipeline.indexBuffer,
                                  newPipeline.indexBufferFormat,
                                  newPipeline.indexBufferOffset);
    }
    if (oldPipeline.vertexBuffers != newPipeline.vertexBuffers ||
        oldPipeline.vertexBufferStrides != newPipeline.vertexBufferStrides ||
        oldPipeline.vertexBufferOffsets != newPipeline.vertexBufferOffsets) {
        context->IASetVertexBuffers(0,
                                    static_cast<UINT>(newPipeline.vertexBuffers.size()),
                                    newPipeline.vertexBuffers.data(),
                                    newPipeline.vertexBufferStrides.data(),
                                    newPipeline.vertexBufferOffsets.data());
        if (newPipeline.vertexBuffers.size() < oldPipeline.vertexBuffers.size()) {
            std::vector<ID3D11Buffer*> nullptrArray(oldPipeline.vertexBuffers.size() - newPipeline.vertexBuffers.size(), nullptr);
            std::vector<UINT> zeroArray(nullptrArray.size(), 0);
            context->IASetVertexBuffers(static_cast<UINT>(newPipeline.vertexBuffers.size()),
                                        static_cast<UINT>(nullptrArray.size()),
                                        nullptrArray.data(),
                                        zeroArray.data(),
                                        zeroArray.data());
        }
    }
    // VS
    if (oldPipeline.vertexShader != newPipeline.vertexShader) {
        context->VSSetShader(newPipeline.vertexShader, nullptr, 0);
    }
    ResetInputs<ID3D11Buffer, &ID3D11DeviceContext::VSSetConstantBuffers>(oldPipeline.vertexShaderConstantBuffers,
                                                                          newPipeline.vertexShaderConstantBuffers,
                                                                          context);
    ResetInputs<ID3D11SamplerState, &ID3D11DeviceContext::VSSetSamplers>(oldPipeline.vertexShaderSamplers,
                                                                         newPipeline.vertexShaderSamplers,
                                                                         context);
    ResetInputs<ID3D11ShaderResourceView, &ID3D11DeviceContext::VSSetShaderResources>(oldPipeline.vertexShaderResources,
                                                                                      newPipeline.vertexShaderResources,
                                                                                      context);
    // HS
    if (oldPipeline.hullShader != newPipeline.hullShader) {
        context->HSSetShader(newPipeline.hullShader, nullptr, 0);
    }
    ResetInputs<ID3D11Buffer, &ID3D11DeviceContext::HSSetConstantBuffers>(oldPipeline.hullShaderConstantBuffers,
                                                                          newPipeline.hullShaderConstantBuffers,
                                                                          context);
    ResetInputs<ID3D11SamplerState, &ID3D11DeviceContext::HSSetSamplers>(oldPipeline.hullShaderSamplers,
                                                                         newPipeline.hullShaderSamplers,
                                                                         context);
    ResetInputs<ID3D11ShaderResourceView, &ID3D11DeviceContext::HSSetShaderResources>(oldPipeline.hullShaderResources,
                                                                                      newPipeline.hullShaderResources,
                                                                                      context);
    // DS
    if (oldPipeline.domainShader != newPipeline.domainShader) {
        context->DSSetShader(newPipeline.domainShader, nullptr, 0);
    }
    ResetInputs<ID3D11Buffer, &ID3D11DeviceContext::DSSetConstantBuffers>(oldPipeline.domainShaderConstantBuffers,
                                                                          newPipeline.domainShaderConstantBuffers,
                                                                          context);
    ResetInputs<ID3D11SamplerState, &ID3D11DeviceContext::DSSetSamplers>(oldPipeline.domainShaderSamplers,
                                                                         newPipeline.domainShaderSamplers,
                                                                         context);
    ResetInputs<ID3D11ShaderResourceView, &ID3D11DeviceContext::DSSetShaderResources>(oldPipeline.domainShaderResources,
                                                                                      newPipeline.domainShaderResources,
                                                                                      context);
    // GS
    if (oldPipeline.geometryShader != newPipeline.geometryShader) {
        context->GSSetShader(newPipeline.geometryShader, nullptr, 0);
    }
    ResetInputs<ID3D11Buffer, &ID3D11DeviceContext::GSSetConstantBuffers>(oldPipeline.geometryShaderConstantBuffers,
                                                                          newPipeline.geometryShaderConstantBuffers,
                                                                          context);
    ResetInputs<ID3D11SamplerState, &ID3D11DeviceContext::GSSetSamplers>(oldPipeline.geometryShaderSamplers,
                                                                         newPipeline.geometryShaderSamplers,
                                                                         context);
    ResetInputs<ID3D11ShaderResourceView, &ID3D11DeviceContext::GSSetShaderResources>(oldPipeline.geometryShaderResources,
                                                                                      newPipeline.geometryShaderResources,
                                                                                      context);
    // RS
    if (oldPipeline.rasterizerState != newPipeline.rasterizerState) {
        context->RSSetState(newPipeline.rasterizerState);
    }
    if (oldPipeline.rasterizerViewports != newPipeline.rasterizerViewports) {
        context->RSSetViewports(static_cast<UINT>(newPipeline.rasterizerViewports.size()),
                                newPipeline.rasterizerViewports.data());
    }
    // PS
    if (oldPipeline.pixelShader != newPipeline.pixelShader) {
        context->PSSetShader(newPipeline.pixelShader, nullptr, 0);
    }
    ResetInputs<ID3D11Buffer, &ID3D11DeviceContext::PSSetConstantBuffers>(oldPipeline.pixelShaderConstantBuffers,
                                                                          newPipeline.pixelShaderConstantBuffers,
                                                                          context);
    ResetInputs<ID3D11SamplerState, &ID3D11DeviceContext::PSSetSamplers>(oldPipeline.pixelShaderSamplers,
                                                                         newPipeline.pixelShaderSamplers,
                                                                         context);
    ResetInputs<ID3D11ShaderResourceView, &ID3D11DeviceContext::PSSetShaderResources>(oldPipeline.pixelShaderResources,
                                                                                      newPipeline.pixelShaderResources,
                                                                                      context);
    // OM
    if (oldPipeline.depthStencilState != newPipeline.depthStencilState ||
        oldPipeline.depthStencilRef != newPipeline.depthStencilRef) {
        context->OMSetDepthStencilState(newPipeline.depthStencilState,
                                        newPipeline.depthStencilRef);
    }
    if (oldPipeline.blendState != newPipeline.blendState ||
        std::memcmp(oldPipeline.blendFactor, newPipeline.blendFactor, 4 * sizeof(float)) != 0 ||
        oldPipeline.blendSampleMask != newPipeline.blendSampleMask) {
        context->OMSetBlendState(newPipeline.blendState, newPipeline.blendFactor, newPipeline.blendSampleMask);
    }


    // now we can bind so targets, since we don't use them as inputs anywhere
    // bind SO targets
    if (changeStreamOutputTargets && newPipeline.streamOutputTargets.size() > 0) {
        context->SOSetTargets(static_cast<UINT>(newPipeline.streamOutputTargets.size()),
                              newPipeline.streamOutputTargets.data(),
                              newPipeline.streamOutputTargetOffsets.data());
    }
    // bind OM targets
    if (changeRenderTargets && (newPipeline.renderTargetViews.size() > 0 || newPipeline.depthStencilView != nullptr)) {
        context->OMSetRenderTargets(static_cast<UINT>(newPipeline.renderTargetViews.size()),
                                    newPipeline.renderTargetViews.data(),
                                    newPipeline.depthStencilView);
    }

    return newPipeline;
}
