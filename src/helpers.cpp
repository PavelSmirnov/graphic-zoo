#include "helpers.h"
#include <sstream>

std::string DefinitionToString(const float value) {
    std::ostringstream ss;
    ss.precision(8);
    ss << std::scientific << "(" << value << ")";
    return ss.str();
}
