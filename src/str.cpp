#include "str.h"

std::string StripLine(const std::string& str) {
    int l = 0;
    while (l < str.size() && IsWhiteSpace(l)) {
        ++l;
    }
    if (l == str.size()) {
        return "";
    }
    int r = static_cast<int>(str.size());
    while (r > 0 && IsWhiteSpace(str[r - 1])) {
        --r;
    }
    return str.substr(l, r - l);
}

std::vector<std::string> SplitLine(const std::string& str, const char sep) {
    std::vector<std::string> parts;
    int last = 0;
    for (int i = 0; i <= str.size(); ++i) {
        if (i == str.size() || str[i] == sep) {
            parts.push_back(str.substr(last, i - last));
            last = i + 1;
        }
    }
    return parts;
}
