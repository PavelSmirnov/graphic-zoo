#include "error.h"
#include "geometry.h"


float CalcDet(const float a00, const float a01,
              const float a10, const float a11) {
    return a00 * a11 - a01 * a10;
}


TQuaternion::TQuaternion(float r, float ux, float uy, float uz)
    : R(r), U({ux, uy, uz}) {}


TQuaternion::TQuaternion(const float r, const TVector<3>& u)
    : R(r), U(u) {}


TQuaternion TQuaternion::operator*(const TQuaternion& other) const {
    return TQuaternion(R * other.R - CalcScalarProduct(U, other.U),
                       R * other.U + other.R * U + CalcVectorProduct(U, other.U));
}


TTransformation TQuaternion::GetRotationMatrix() const {
    return {1 - 2 * (CalcSquare(U(1)) + CalcSquare(U(2))), 2 * (U(0) * U(1) - R * U(2)), 2 * (U(0) * U(2) + R * U(1)), 0,
            2 * (U(0) * U(1) + R * U(2)), 1 - 2 * (CalcSquare(U(0)) + CalcSquare(U(2))), 2 * (U(1) * U(2) - R * U(0)), 0,
            2 * (U(0) * U(2) - R * U(1)), 2 * (U(1) * U(2) + R * U(0)), 1 - 2 * (CalcSquare(U(0)) + CalcSquare(U(1))), 0,
            0, 0, 0, 1};
}


TMatrix<3, 3> TQuaternion::GetRotationMatrix3x3() const {
    return {1 - 2 * (CalcSquare(U(1)) + CalcSquare(U(2))), 2 * (U(0) * U(1) - R * U(2)), 2 * (U(0) * U(2) + R * U(1)),
            2 * (U(0) * U(1) + R * U(2)), 1 - 2 * (CalcSquare(U(0)) + CalcSquare(U(2))), 2 * (U(1) * U(2) - R * U(0)),
            2 * (U(0) * U(2) - R * U(1)), 2 * (U(1) * U(2) + R * U(0)), 1 - 2 * (CalcSquare(U(0)) + CalcSquare(U(1)))};
}


TQuaternion TQuaternion::FromRotation(const float angle, const TVector<3>& axis) {
    return TQuaternion(std::cos(angle * 0.5f), std::sin(angle * 0.5f) * Normalize(axis));
}


TVector<3> CalcVectorProduct(const TVector<3>& lhs, const TVector<3>& rhs) {
    return {CalcDet(lhs(1), rhs(1),
                    lhs(2), rhs(2)),
            CalcDet(lhs(2), rhs(2),
                    lhs(0), rhs(0)),
            CalcDet(lhs(0), rhs(0),
                    lhs(1), rhs(1))};
}


TTransformation GetProjectionMatrix(const float nearClip,
                                    const float farClip,
                                    const float horizontalAov,
                                    const float verticalAov,
                                    const float xOffset,
                                    const float yOffset) {
    ENSURE(nearClip > 0.0, "Near clip can't be zero.");
    const float workDistance = farClip - nearClip;
    ENSURE(workDistance > 0.0, "Near clip is too close to far clip.");
    const float a = farClip / workDistance;
    const float b = -farClip * nearClip / workDistance;
    ENSURE(horizontalAov < PI_CONST && verticalAov < PI_CONST,
           "Angle of view must be less than pi radians.");
    const float h = 1.0f / std::tan(horizontalAov * 0.5f);
    const float v = 1.0f / std::tan(verticalAov * 0.5f);
    return {h, 0, xOffset, 0,
            0, v, yOffset, 0,
            0, 0,       a, b,
            0, 0,       1, 0};
}


TTransformation GetInverseProjectionMatrix(const float nearClip,
                                           const float farClip,
                                           const float horizontalAov,
                                           const float verticalAov,
                                           const float xOffset,
                                           const float yOffset) {
    ENSURE(nearClip > 0.0, "Near clip can't be zero.");
    const float workDistance = farClip - nearClip;
    ENSURE(workDistance > 0.0, "Near clip is too close to far clip.");
    const float a = farClip / workDistance;
    const float b = -farClip * nearClip / workDistance;
    ENSURE(horizontalAov < PI_CONST && verticalAov < PI_CONST,
           "Angle of view must be less than pi radians.");
    const float h = 1.0f / std::tan(horizontalAov * 0.5f);
    const float v = 1.0f / std::tan(verticalAov * 0.5f);
    return {1.0f / h,        0,        0, -xOffset / h,
                   0, 1.0f / v,        0, -yOffset / v,
                   0,        0,        0,            1,
                   0,        0, 1.0f / b,       -a / b};
}


TTransformation GetOrthoProjectionMatrix(const float nearClip,
                                         const float farClip,
                                         const float width,
                                         const float height,
                                         const float xOffset,
                                         const float yOffset) {
    const float workDistance = farClip - nearClip;
    ENSURE(workDistance > 0.0, "Near clip is too close to far clip.");
    const float a = 1.0f / workDistance;
    const float b = -nearClip / workDistance;
    const float h = 2.0f / width;
    const float v = 2.0f / height;
    return {h, 0, 0, -xOffset * h,
            0, v, 0, -yOffset * v,
            0, 0, a,            b,
            0, 0, 0,            1};
}


TTransformation GetInverseOrthoProjectionMatrix(const float nearClip,
                                                const float farClip,
                                                const float width,
                                                const float height,
                                                const float xOffset,
                                                const float yOffset) {
    const float workDistance = farClip - nearClip;
    ENSURE(workDistance > 0.0, "Near clip is too close to far clip.");
    const float a = 1.0f / workDistance;
    const float b = -nearClip / workDistance;
    const float h = 2.0f / width;
    const float v = 2.0f / height;
    return {1.0f / h,        0,        0, xOffset,
                   0, 1.0f / v,        0, yOffset,
                   0,        0, 1.0f / a,  -b / a,
                   0,        0,        0,       1};
}


float CalcFogStart(const float fogStartDistance, const float nearClip, const float farClip) {
    const float workDistance = farClip - nearClip;
    ENSURE(workDistance != 0.0, "Near clip is too close to far clip.");
    const float a = farClip / workDistance;
    const float b = -farClip * nearClip / workDistance;
    return a + b / fogStartDistance;
}


TTransformation GetViewMatrix(const TVector<3>& eye, const TVector<3>& lookAt, const TVector<3>& up) {
    const auto t = Normalize(lookAt - eye);
    const auto u = Normalize(CalcVectorProduct(t, up));
    const auto v = CalcVectorProduct(u, t);
    return {u(0), u(1), u(2), -CalcScalarProduct(u, eye),
            v(0), v(1), v(2), -CalcScalarProduct(v, eye),
            t(0), t(1), t(2), -CalcScalarProduct(t, eye),
               0,    0,    0,                          1};
}


TTransformation GetInverseViewMatrix(const TVector<3>& eye, const TVector<3>& lookAt, const TVector<3>& up) {
    const auto t = Normalize(lookAt - eye);
    const auto u = Normalize(CalcVectorProduct(t, up));
    const auto v = CalcVectorProduct(u, t);
    return {u(0), v(0), t(0), eye(0),
            u(1), v(1), t(1), eye(1),
            u(2), v(2), t(2), eye(2),
               0,    0,    0,      1};
}


TTransformation GetTranslationMatrix(const TVector<3>& translation) {
    return {1, 0, 0, translation(0),
            0, 1, 0, translation(1),
            0, 0, 1, translation(2),
            0, 0, 0,              1};
}


TTransformation GetScalingMatrix(const TVector<3>& scale) {
    return {scale(0),        0,        0, 0,
                   0, scale(1),        0, 0,
                   0,        0, scale(2), 0,
                   0,        0,        0, 1};
}


TTransformation GetRotationMatrix(const TVector<3>& axis, const float angle) {
    const TVector<3> normalizedAxis = Normalize(axis);
    const float& a = normalizedAxis(0);
    const float& b = normalizedAxis(1);
    const float& c = normalizedAxis(2);

    const float cosa = cos(angle);
    const float sina = sin(angle);

    return {    cosa + a * a * (1 - cosa), a * b * (1 - cosa) - c * sina, a * c * (1 - cosa) + b * sina, 0,
            a * b * (1 - cosa) + c * sina,     cosa + b * b * (1 - cosa), b * c * (1 - cosa) - a * sina, 0,
            a * c * (1 - cosa) - b * sina, b * c * (1 - cosa) + a * sina,     cosa + c * c * (1 - cosa), 0,
                                        0,                             0,                             0, 1};
}


TVector<3> Get3DVector(const TVector<4>& vector) {
    return {vector(0), vector(1), vector(2)};
}


TVector<3> Get3DPoint(const TVector<4>& point) {
    return TVector<3>({point(0), point(1), point(2)}) / point(3);
}

TVector<4> Get4DVector(const TVector<3>& vector) {
    return {vector(0), vector(1), vector(2), 0};
}

TVector<4> Get4DPoint(const TVector<3>& point) {
    return {point(0), point(1), point(2), 1};
}
