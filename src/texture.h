#pragma once
#include "d3d.h"
#include <vector>

class TTexture2D {
public:
    TTexture2D();
    ~TTexture2D();

    TTexture2D& Create(const unsigned int width,
                       const unsigned int height,
                       const DXGI_FORMAT format,
                       const D3D11_USAGE usage,
                       const unsigned int mipLevels,
                       const unsigned int arraySize,
                       const unsigned int bindFlags,
                       const unsigned int cpuAccessFlags,
                       const unsigned int miscFlags,
                       ID3D11Device* device,
                       const D3D11_SUBRESOURCE_DATA* dataDesc = nullptr);

    ID3D11Texture2D* GetResource();
    ID3D11ShaderResourceView** GetSrv(int index = 0);
    ID3D11RenderTargetView** GetRtv(int index = 0);
    ID3D11DepthStencilView** GetDsv(int index = 0);

    TTexture2D& AddSrv(ID3D11Device* device,
                       const unsigned int mipLevels = 1,
                       const unsigned int mostDetailedMip = 0,
                       const DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
    TTexture2D& AddArraySrv(ID3D11Device* device,
                            const unsigned int mipLevels = 1,
                            const unsigned int mostDetailedMip = 0,
                            const unsigned int firstArraySlice = 0,
                            const unsigned int arraySize = 1,
                            const DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
    TTexture2D& AddRtv(ID3D11Device* device,
                       const unsigned int mipSlice = 0,
                       const DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
    TTexture2D& AddArrayRtv(ID3D11Device* device,
                            const unsigned int mipSlice = 0,
                            const unsigned int firstArraySlice = 0,
                            const unsigned int arraySize = 1,
                            const DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
    TTexture2D& AddDsv(ID3D11Device* device,
                       const unsigned int mipSlice = 0,
                       const DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
    TTexture2D& AddArrayDsv(ID3D11Device* device,
                            const unsigned int mipSlice = 0,
                            const unsigned int firstArraySlice = 0,
                            const unsigned int arraySize = 1,
                            const DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);

private:
    ID3D11Texture2D* Texture = nullptr;
    std::vector<ID3D11ShaderResourceView*> Srv;
    std::vector<ID3D11RenderTargetView*> Rtv;
    std::vector<ID3D11DepthStencilView*> Dsv;

    unsigned int Width = 0;
    unsigned int Height = 0;
    DXGI_FORMAT Format = DXGI_FORMAT_UNKNOWN;
};
