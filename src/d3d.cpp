#include "d3d.h"

#ifdef _DEBUG
constexpr const UINT D3D_FLAGS = D3D11_CREATE_DEVICE_DEBUG;
#else
constexpr const UINT D3D_FLAGS = 0;
#endif


namespace {
    TDirectX* DIRECTX = nullptr;
}


std::string GetShaderErrorMessageAndRelease(ID3DBlob* errorMessageBlob) {
    std::string message(errorMessageBlob->GetBufferSize(), 0);
    std::memcpy(message.data(), errorMessageBlob->GetBufferPointer(), errorMessageBlob->GetBufferSize());
    errorMessageBlob->Release();
    return message;
}


TDirectX* TDirectX::Create() {
    ENSURE(DIRECTX == nullptr, "New DirectX instance to be created, yet the old one is not destroyed.");
    return DIRECTX = new TDirectX();
}


TDirectX* TDirectX::Get() {
    return DIRECTX;
}


void TDirectX::Destroy() {
    ENSURE(DIRECTX != nullptr, "The DirectX instance to be destroyed, but it is not created yet.");
    delete DIRECTX;
    DIRECTX = nullptr;
}


ID3D11Device* TDirectX::GetDevice() {
    return Device;
}


ID3D11DeviceContext* TDirectX::GetContext() {
    return Context;
}


TDirectX::TDirectX() {
    D3D_FEATURE_LEVEL featureLevels[] = {D3D_FEATURE_LEVEL_11_1};
    ENSURE_D3D(D3D11CreateDevice(nullptr, // default video adapter
                                 D3D_DRIVER_TYPE_HARDWARE, // use GPU driver
                                 nullptr, // no softwareRasterizer
                                 D3D_FLAGS,
                                 featureLevels,
                                 sizeof(featureLevels) / sizeof(D3D_FEATURE_LEVEL),
                                 D3D11_SDK_VERSION,
                                 &Device,
                                 &FeatureLevel,
                                 &Context),
               "Failed to create DirectX device.");
}


TDirectX::~TDirectX() {
    Context->Release();
    Device->Release();
}
