#pragma once
#include "geometry.h"


struct TCamera {
    float nearClip = 1;
    float farClip = 100;
    float horizontalAov = PI_CONST / 2;
    float verticalAov = PI_CONST / 2;
    TVector<3> eye = {0, 0, 0};
    TVector<3> lookAt = {0, 1, 0};
    TVector<3> up = {0, 0, 1};
};


inline TTransformation GetProjectionMatrix(const TCamera& camera) {
    return GetProjectionMatrix(camera.nearClip, camera.farClip, camera.horizontalAov, camera.verticalAov);
}

inline TTransformation GetViewMatrix(const TCamera& camera) {
    return GetViewMatrix(camera.eye, camera.lookAt, camera.up);
}


class TFreeCamera {
public:
    static constexpr int DIR_FORWARD  = 1 << 0;
    static constexpr int DIR_BACKWARD = 1 << 1;
    static constexpr int DIR_LEFT     = 1 << 2;
    static constexpr int DIR_RIGHT    = 1 << 3;
    static constexpr int DIR_UP       = 1 << 4;
    static constexpr int DIR_DOWN     = 1 << 5;

public:
    TFreeCamera(const TCamera& camera,
                const float movementSpeed,
                const float rotationSpeed,
                const float downLookMinAngle = 1e-6,
                const float upLookMinAngle = 1e-6);
    void Move(const float time, const int directions);
    void RotateStabilized(const int dXOnScreen,
                          const int dYOnScreen);
    void SetMovementSpeed(const float movementSpeed);
    void SetRotationSpeed(const float rotationSpeed);
    const TCamera& GetCamera() const;

private:
    TCamera Camera;
    float MovementSpeed;
    float RotationSpeed;
    float DownLookMinAngle;
    float UpLookMinAngle;
    TTransformation CameraSpaceToWorldSpace;
};
