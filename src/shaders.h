#pragma once
#include "d3d.h"
#include <string>
#include <vector>

struct TShaderMacro {
    std::string name;
    std::string definition;
};

void InitializeShaderMacros();
const std::vector<TShaderMacro>& GetShaderMacros();

class TShader {
public:
    ~TShader();

protected:
    // call shader entry point the same name as the shader file without the '.hlsl' extension
    TShader() = default;
    void Compile(const std::string& shaderName, const std::string& shaderVersion);
    ID3DBlob* GetCode() const;

private:
    ID3DBlob* Code = nullptr;
};

class TVertexShader : public TShader {
public:
    ~TVertexShader();
    void Create(const std::string& shaderName,
                const D3D11_INPUT_ELEMENT_DESC* layout,
                const unsigned int numLayoutElements,
                ID3D11Device* device);
    ID3D11VertexShader* GetShader();
    ID3D11InputLayout* GetLayout();

private:
    ID3D11VertexShader* Shader = nullptr;
    ID3D11InputLayout* Layout = nullptr;
};

class THullShader : public TShader {
public:
    ~THullShader();
    void Create(const std::string& shaderName, ID3D11Device* device);
    ID3D11HullShader* GetShader();

private:
    ID3D11HullShader* Shader = nullptr;
};

class TDomainShader : public TShader {
public:
    ~TDomainShader();
    void Create(const std::string& shaderName, ID3D11Device* device);
    ID3D11DomainShader* GetShader();

private:
    ID3D11DomainShader* Shader = nullptr;
};

class TGeometryShader : public TShader {
public:
    ~TGeometryShader();
    void Create(const std::string& shaderName, ID3D11Device* device);
    void CreateWithStreamOutput(const std::string& shaderName,
                                const D3D11_SO_DECLARATION_ENTRY soDeclarationEntries[],
                                const unsigned int numSoDeclarationEntries,
                                ID3D11Device* device);
    ID3D11GeometryShader* GetShader();

private:
    ID3D11GeometryShader* Shader = nullptr;
};

class TPixelShader : public TShader {
public:
    ~TPixelShader();
    void Create(const std::string& shaderName, ID3D11Device* device);
    ID3D11PixelShader* GetShader();

private:
    ID3D11PixelShader* Shader = nullptr;
};

// view
const D3D11_INPUT_ELEMENT_DESC VIEW_INPUT_LAYOUT[] = {
    { "INDEX", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

// object
const D3D11_INPUT_ELEMENT_DESC OBJECT_INPUT_LAYOUT[] = {
    { "MODEL_SPACE_POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0},
    { "TEXTURE_UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0},
    { "MODEL_SPACE_NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0}
};

// quadtree
const D3D11_SO_DECLARATION_ENTRY QUADTREE_SO_DECLARATION_ENTRY[] = {
    { 0, "NODE_INDEX", 0, 0, 1, 0 }
};

const D3D11_INPUT_ELEMENT_DESC QUADTREE_AND_QUADMAP_INPUT_LAYOUT[] = {
    { "NODE_INDEX", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};
