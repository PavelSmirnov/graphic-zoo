#pragma once
#include "error.h"
#include "log.h"
#include <d3d11.h>
#include <d3dcompiler.h>
#include <dxgi.h>
#include <dxgi1_2.h>
#include <string>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3DCompiler.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "DXGI.lib")

#define ENSURE_D3D(x, msg) { \
    const HRESULT status = (x); \
    ENSURE(status == S_OK, "DirectX call failure with error code " << ToHexStr(status) << ":\n" << msg); \
}

template <class C>
void ReleaseD3DObject(C** objectPtr) {
    if (*objectPtr != nullptr) {
        (*objectPtr)->Release();
        *objectPtr = nullptr;
    }
}

std::string GetShaderErrorMessageAndRelease(ID3DBlob* errorMessageBlob);

class TDirectX {
public:
    static TDirectX* Create();
    static TDirectX* Get();
    static void Destroy();

    ID3D11Device* GetDevice();
    ID3D11DeviceContext* GetContext();

private:
    TDirectX();
    ~TDirectX();

    D3D_FEATURE_LEVEL FeatureLevel = {};
    ID3D11Device* Device = nullptr;
    ID3D11DeviceContext* Context = nullptr;
};

template <typename T>
void ReleaseSafe(T* ref) {
    if (ref != nullptr) {
        ref->Release();
    }
}

template <typename T>
void AddRefSafe(T* ref) {
    if (ref != nullptr) {
        ref->AddRef();
    }
}
