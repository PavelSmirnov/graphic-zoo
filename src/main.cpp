/* This is to map `malloc` and `free` to debug versions for memory leak detection.
 * Use _DEBUG define for the map to occur.
 * More info: https://docs.microsoft.com/en-us/visualstudio/debugger/finding-memory-leaks-using-the-crt-library?view=vs-2019
 */
#define _CRTDBG_MAP_ALLOC
#include <filesystem>
#include <stdlib.h>
#include <crtdbg.h>

#include "asset_library.h"
#include "d3d.h"
#include "error.h"
#include "log.h"
#include "loop.h"
#include "mouse.h"
#include "os_win.h"
#include "renderer.h"
#include "shaders.h"
#include "window.h"

const std::filesystem::path ASSETS_PATH = "./assets";

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE/*prevInstance*/, PWSTR/*pCmdLine*/, int nCmdShow) {
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // check for memory leaks at every exit point automatically
    _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE); // dump memory leak report to file
    LPCWSTR leakReportFileName = L"dump.txt";
    const HANDLE leakReportFile = CreateFile(leakReportFileName, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (leakReportFile == INVALID_HANDLE_VALUE) {
        // TODO: Log("Failed to open leak report file " << leakReportFileName << "."); // TODO: but EnableLogging goes after!
        return 1;
    }
    _CrtSetReportFile(_CRT_WARN, leakReportFile);

    EnableLogging("log.txt");

    try {
        InitializeShaderMacros();
        TMouse::Create();
        TWindow::Create(hInstance, nCmdShow, 1920, 1080, false)->Show();
        //TWindow::Create(hInstance, nCmdShow, 1920, 1080, true)->Show();
        TDirectX::Create();
        TAssetLibrary::Create(ASSETS_PATH, TDirectX::Get());
        TRenderer::Create(TWindow::Get(), TDirectX::Get(), TAssetLibrary::Get());

        Loop(TWindow::Get(), TRenderer::Get(), TMouse::Get(), TAssetLibrary::Get());

        TRenderer::Destroy();
        TDirectX::Destroy();
        TWindow::Destroy();
        TMouse::Destroy();
    } catch (const TEngineException& e) {
        Log(e.what());
    }

    return 0;
}
