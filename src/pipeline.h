#pragma once
#include "d3d.h"
#include "shaders.h"
#include <vector>


struct TFixedPipeline {
    // MUST DEFAULT TO ALL ACTUAL DIRECTX DEFAULTS
    // IA
    ID3D11InputLayout* inputLayout = nullptr;
    D3D11_PRIMITIVE_TOPOLOGY primitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
    ID3D11Buffer* indexBuffer = nullptr;
    DXGI_FORMAT indexBufferFormat = DXGI_FORMAT_UNKNOWN;
    unsigned int indexBufferOffset = 0;
    std::vector<ID3D11Buffer*> vertexBuffers;
    std::vector<unsigned int> vertexBufferStrides;
    std::vector<unsigned int> vertexBufferOffsets;
    // VS
    ID3D11VertexShader* vertexShader = nullptr;
    std::vector<ID3D11Buffer*> vertexShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> vertexShaderSamplers;
    std::vector<ID3D11ShaderResourceView*> vertexShaderResources;
    // HS
    ID3D11HullShader* hullShader = nullptr;
    std::vector<ID3D11Buffer*> hullShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> hullShaderSamplers;
    std::vector<ID3D11ShaderResourceView*> hullShaderResources;
    // DS
    ID3D11DomainShader* domainShader = nullptr;
    std::vector<ID3D11Buffer*> domainShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> domainShaderSamplers;
    std::vector<ID3D11ShaderResourceView*> domainShaderResources;
    // GS
    ID3D11GeometryShader* geometryShader = nullptr;
    std::vector<ID3D11Buffer*> geometryShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> geometryShaderSamplers;
    std::vector<ID3D11ShaderResourceView*> geometryShaderResources;
    std::vector<ID3D11Buffer*> streamOutputTargets;
    std::vector<unsigned int> streamOutputTargetOffsets;
    // RS
    ID3D11RasterizerState* rasterizerState = nullptr;
    std::vector<D3D11_VIEWPORT> rasterizerViewports;
    // PS
    ID3D11PixelShader* pixelShader = nullptr;
    std::vector<ID3D11Buffer*> pixelShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> pixelShaderSamplers;
    std::vector<ID3D11ShaderResourceView*> pixelShaderResources;
    // OM
    ID3D11DepthStencilState* depthStencilState = nullptr;
    unsigned int depthStencilRef = 0;
    ID3D11DepthStencilView* depthStencilView = nullptr;
    std::vector<ID3D11RenderTargetView*> renderTargetViews;
    ID3D11BlendState* blendState = nullptr;
    float blendFactor[4] = {1, 1, 1, 1};
    unsigned int blendSampleMask = 0xffffffff;
};


struct TVariablePipeline {
    // IA
    ID3D11InputLayout* inputLayout = nullptr;
    D3D11_PRIMITIVE_TOPOLOGY primitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
    ID3D11Buffer** indexBuffer = nullptr;
    DXGI_FORMAT indexBufferFormat = DXGI_FORMAT_UNKNOWN;
    unsigned int indexBufferOffset = 0;
    std::vector<ID3D11Buffer**> vertexBuffers;
    std::vector<unsigned int> vertexBufferStrides;
    std::vector<unsigned int> vertexBufferOffsets;
    // VS
    ID3D11VertexShader* vertexShader = nullptr;
    std::vector<ID3D11Buffer**> vertexShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> vertexShaderSamplers;
    std::vector<ID3D11ShaderResourceView**> vertexShaderResources;
    // HS
    ID3D11HullShader* hullShader = nullptr;
    std::vector<ID3D11Buffer**> hullShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> hullShaderSamplers;
    std::vector<ID3D11ShaderResourceView**> hullShaderResources;
    // DS
    ID3D11DomainShader* domainShader = nullptr;
    std::vector<ID3D11Buffer**> domainShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> domainShaderSamplers;
    std::vector<ID3D11ShaderResourceView**> domainShaderResources;
    // GS
    ID3D11GeometryShader* geometryShader = nullptr;
    std::vector<ID3D11Buffer**> geometryShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> geometryShaderSamplers;
    std::vector<ID3D11ShaderResourceView**> geometryShaderResources;
    std::vector<ID3D11Buffer**> streamOutputTargets;
    std::vector<unsigned int> streamOutputTargetOffsets;
    // RS
    ID3D11RasterizerState** rasterizerState = nullptr;
    std::vector<D3D11_VIEWPORT> rasterizerViewports;
    // PS
    ID3D11PixelShader* pixelShader = nullptr;
    std::vector<ID3D11Buffer**> pixelShaderConstantBuffers;
    std::vector<ID3D11SamplerState*> pixelShaderSamplers;
    std::vector<ID3D11ShaderResourceView**> pixelShaderResources;
    // OM
    ID3D11DepthStencilState* depthStencilState = nullptr;
    unsigned int depthStencilRef = 0;
    ID3D11DepthStencilView** depthStencilView = nullptr;
    std::vector<ID3D11RenderTargetView**> renderTargetViews;
    ID3D11BlendState* blendState = nullptr;
    float blendFactor[4] = {1, 1, 1, 1};
    unsigned int blendSampleMask = 0xffffffff;
};


class TPipelineBuilder {
public:
    TVariablePipeline Build();

    TPipelineBuilder(ID3D11InputLayout* inputLayout,
                     D3D11_PRIMITIVE_TOPOLOGY primitiveTopology,
                     ID3D11Buffer** indexBuffer,
                     DXGI_FORMAT indexBufferFormat,
                     unsigned int indexBufferOffset = 0);
    TPipelineBuilder& AddVertexBuffer(ID3D11Buffer** buffer,
                                      unsigned int stride,
                                      unsigned int offset = 0);

    TPipelineBuilder& SetVS(TVertexShader& shader);
    TPipelineBuilder& AddVSBuffer(ID3D11Buffer** buffer);
    TPipelineBuilder& AddVSSampler(ID3D11SamplerState* sampler);
    TPipelineBuilder& AddVSResource(ID3D11ShaderResourceView** resource);

    TPipelineBuilder& SetHS(THullShader& shader);
    TPipelineBuilder& AddHSBuffer(ID3D11Buffer** buffer);
    TPipelineBuilder& AddHSSampler(ID3D11SamplerState* sampler);
    TPipelineBuilder& AddHSResource(ID3D11ShaderResourceView** resource);

    TPipelineBuilder& SetDS(TDomainShader& shader);
    TPipelineBuilder& AddDSBuffer(ID3D11Buffer** buffer);
    TPipelineBuilder& AddDSSampler(ID3D11SamplerState* sampler);
    TPipelineBuilder& AddDSResource(ID3D11ShaderResourceView** resource);

    TPipelineBuilder& SetGS(TGeometryShader& shader);
    TPipelineBuilder& AddGSBuffer(ID3D11Buffer** buffer);
    TPipelineBuilder& AddGSSampler(ID3D11SamplerState* sampler);
    TPipelineBuilder& AddGSResource(ID3D11ShaderResourceView** resource);
    TPipelineBuilder& AddSOTarget(ID3D11Buffer** buffer, unsigned int offset = 0);

    TPipelineBuilder& SetRSState(ID3D11RasterizerState** state);
    TPipelineBuilder& AddRSViewport(const D3D11_VIEWPORT& viewport);

    TPipelineBuilder& SetPS(TPixelShader& shader);
    TPipelineBuilder& AddPSBuffer(ID3D11Buffer** buffer);
    TPipelineBuilder& AddPSSampler(ID3D11SamplerState* sampler);
    TPipelineBuilder& AddPSResource(ID3D11ShaderResourceView** resource);

    TPipelineBuilder& SetOMDepthStencilState(ID3D11DepthStencilState* state, unsigned int ref = 0);
    TPipelineBuilder& SetOMDepthStencil(ID3D11DepthStencilView** depthStencil);
    TPipelineBuilder& AddOMRenderTarget(ID3D11RenderTargetView** renderTarget);
    TPipelineBuilder& SetOMBlendState(ID3D11BlendState* blendState,
                                      const float* blendFactor = nullptr, // size 4 array or nullptr
                                      unsigned int blendSampleMask = 0xffffffff);

private:
    TVariablePipeline Pipeline;
};

TFixedPipeline FixPipeline(const TVariablePipeline& variablePipeline);

TFixedPipeline SwitchPipeline(const TFixedPipeline& oldPipeline,
                              const TVariablePipeline& variableNewPipeline,
                              ID3D11DeviceContext* context);
