#include "object.h"
constexpr const float MIN_ROTATION_ANGLE = 1e-6f;

void MoveObject(const float time,
                const TVector<3>& force,
                const TVector<3>& forceMomentum,
                TObject* object) {
    object->velocity += force * time / object->mass;
    object->translation += time * object->velocity / object->mass;

    const TVector<3> angularMomentum = forceMomentum * time;
    const TMatrix<3, 3> currentRotation = object->rotation.GetRotationMatrix3x3();
    const TMatrix<3, 3> invertedInertiaTensor = currentRotation * object->invertedInertiaTensor * Transpose(currentRotation);
    object->angularVelocity += invertedInertiaTensor * forceMomentum * time;
    const float maxAngularVelocity = GetLength(object->angularVelocity);
    if (maxAngularVelocity > MIN_ROTATION_ANGLE) {
        object->rotation = TQuaternion::FromRotation(maxAngularVelocity * time, object->angularVelocity / maxAngularVelocity) * object->rotation;
    }
}
