#include "constants.h"
#include "shaders.h"

namespace {
    std::vector<TShaderMacro> SHADER_MACROS;

    void AddShaderMacro(const std::string& name, const std::string& value) {
        SHADER_MACROS.push_back(TShaderMacro{name, value});
    }
}

void InitializeShaderMacros() {
    AddShaderMacro("PI", "3.14159265359");
    AddShaderMacro("NUM_SHADOW_MAPS", std::to_string(NUM_SHADOW_MAPS));
    AddShaderMacro("PCF_KERNEL_SIZE", std::to_string(PCF_KERNEL_SIZE));
    AddShaderMacro("USE_PCF_DITHERING", std::to_string(USE_PCF_DITHERING));
    AddShaderMacro("TAA_COEFFICIENT", DefinitionToString(TAA_COEFFICIENT));
    AddShaderMacro("TAA_VARIANCE_SCALE", DefinitionToString(TAA_VARIANCE_SCALE));
    AddShaderMacro("SCATTERING_NUM_SPHERES", DefinitionToString(SCATTERING_NUM_SPHERES));
    AddShaderMacro("SCATTERING_NUM_ANGLES", DefinitionToString(SCATTERING_NUM_ANGLES));
    AddShaderMacro("EARTH_RADIUS", DefinitionToString(EARTH_RADIUS));
    AddShaderMacro("AIR_AVERAGE_HEIGHT", DefinitionToString(AIR_AVERAGE_HEIGHT));
    AddShaderMacro("AEROSOLES_AVERAGE_HEIGHT", DefinitionToString(AEROSOLES_AVERAGE_HEIGHT));
    AddShaderMacro("ATMOSPHERE_MAX_HEIGHT", DefinitionToString(ATMOSPHERE_MAX_HEIGHT));
    AddShaderMacro("AIR_SCATTERING_BETA_RED", DefinitionToString(AIR_SCATTERING_BETA_RED));
    AddShaderMacro("AIR_SCATTERING_BETA_GREEN", DefinitionToString(AIR_SCATTERING_BETA_GREEN));
    AddShaderMacro("AIR_SCATTERING_BETA_BLUE", DefinitionToString(AIR_SCATTERING_BETA_BLUE));
    AddShaderMacro("AEROSOLES_SCATTERING_BETA", DefinitionToString(AEROSOLES_SCATTERING_BETA));
    AddShaderMacro("AEROSOLES_SCATTERING_U", DefinitionToString(AEROSOLES_SCATTERING_U));
    AddShaderMacro("SUN_RADIUS", DefinitionToString(SUN_RADIUS));
    AddShaderMacro("SUN_DISTANCE", DefinitionToString(SUN_DISTANCE));
    AddShaderMacro("NIGHT_SKY_DIM", DefinitionToString(NIGHT_SKY_DIM));
    AddShaderMacro("HORIZON_WIDTH", DefinitionToString(HORIZON_WIDTH));
    AddShaderMacro("SPHERICAL_HARMONICS_NUM_BANDS", std::to_string(SPHERICAL_HARMONICS_NUM_BANDS));
    AddShaderMacro("SPHERICAL_HARMONICS_NUM_COEFFICIENTS", std::to_string(SPHERICAL_HARMONICS_NUM_COEFFICIENTS));
    AddShaderMacro("SPHERICAL_HARMONICS_ARRAY_SIZE", std::to_string(SPHERICAL_HARMONICS_ARRAY_SIZE));
    AddShaderMacro("MIN_SPECULAR_POWER", DefinitionToString(MIN_SPECULAR_POWER));
    AddShaderMacro("MAX_SPECULAR_POWER", DefinitionToString(MAX_SPECULAR_POWER));
    AddShaderMacro("QUADTREE_PROXIMITY_FACTOR_SQUARED", DefinitionToString(QUADTREE_PROXIMITY_FACTOR_SQUARED));
    AddShaderMacro("QUADTREE_MAX_LEVEL", std::to_string(QUADTREE_MAX_LEVEL));
    AddShaderMacro("GAMMA_CORRECTION_GAMMA", DefinitionToString(GAMMA_CORRECTION_GAMMA));
    AddShaderMacro("SRGB_GAMMA_CORRECTION", DefinitionToString(SRGB_GAMMA_CORRECTION));
}

const std::vector<TShaderMacro>& GetShaderMacros() {
    return SHADER_MACROS;
}

TShader::~TShader() {
    ReleaseD3DObject(&Code);
}

void TShader::Compile(const std::string& shaderName, const std::string& shaderVersion) {
    std::vector<D3D_SHADER_MACRO> shaderMacrosD3dFormat;
    const auto& shaderMacros = GetShaderMacros();
    for (const auto& macro : shaderMacros) {
        shaderMacrosD3dFormat.push_back(D3D_SHADER_MACRO{macro.name.c_str(), macro.definition.c_str()});
    }
    shaderMacrosD3dFormat.push_back(D3D_SHADER_MACRO{nullptr, nullptr});

    ReleaseD3DObject(&Code);
    ID3DBlob* compileErrors = nullptr;
    const auto path = SHADERS_PATH / (shaderName + ".hlsl");
    ENSURE(std::filesystem::exists(path), "No shader file at: " << path.string() << ".");
    ENSURE_D3D(D3DCompileFromFile(path.wstring().c_str(),
                                  shaderMacrosD3dFormat.data(),
                                  nullptr,
                                  shaderName.c_str(),
                                  shaderVersion.c_str(),
                                  0,
                                  0,
                                  &Code,
                                  &compileErrors),
               "While compiling " << shaderName << ":\n" << GetShaderErrorMessageAndRelease(compileErrors));
}

ID3DBlob* TShader::GetCode() const {
    return Code;
}

// VERTEX SHADER //

void TVertexShader::Create(const std::string& shaderName,
                           const D3D11_INPUT_ELEMENT_DESC* layout,
                           const unsigned int numLayoutElements,
                           ID3D11Device* device) {
    ReleaseD3DObject(&Shader);
    ReleaseD3DObject(&Layout);
    Compile(shaderName, "vs_4_0");
    ENSURE_D3D(device->CreateVertexShader(GetCode()->GetBufferPointer(), GetCode()->GetBufferSize(), nullptr, &Shader),
               "Failed to create " << shaderName << " vertex shader.");
    ENSURE_D3D(device->CreateInputLayout(layout,
                                         numLayoutElements,
                                         GetCode()->GetBufferPointer(),
                                         GetCode()->GetBufferSize(),
                                         &Layout),
               "Failed to create " << shaderName << " input layout.");

}

TVertexShader::~TVertexShader() {
    ReleaseD3DObject(&Shader);
    ReleaseD3DObject(&Layout);
}

ID3D11VertexShader* TVertexShader::GetShader() {
    return Shader;
}

ID3D11InputLayout* TVertexShader::GetLayout() {
    return Layout;
}

// HULL SHADER //

THullShader::~THullShader() {
    ReleaseD3DObject(&Shader);
}

void THullShader::Create(const std::string& shaderName, ID3D11Device* device) {
    ReleaseD3DObject(&Shader);
    Compile(shaderName, "hs_5_0");
    ENSURE_D3D(device->CreateHullShader(GetCode()->GetBufferPointer(), GetCode()->GetBufferSize(), nullptr, &Shader),
               "Failed to create " << shaderName << " hull shader.");
}

ID3D11HullShader* THullShader::GetShader() {
    return Shader;
}

// DOMAIN SHADER //

TDomainShader::~TDomainShader() {
    ReleaseD3DObject(&Shader);
}

void TDomainShader::Create(const std::string& shaderName, ID3D11Device* device) {
    ReleaseD3DObject(&Shader);
    Compile(shaderName, "ds_5_0");
    ENSURE_D3D(device->CreateDomainShader(GetCode()->GetBufferPointer(), GetCode()->GetBufferSize(), nullptr, &Shader),
               "Failed to create " << shaderName << " domain shader.");
}

ID3D11DomainShader* TDomainShader::GetShader() {
    return Shader;
}

// GEOMETRY SHADER //

TGeometryShader::~TGeometryShader() {
    ReleaseD3DObject(&Shader);
}

void TGeometryShader::Create(const std::string& shaderName, ID3D11Device* device) {
    ReleaseD3DObject(&Shader);
    Compile(shaderName, "gs_4_0");
    ENSURE_D3D(device->CreateGeometryShader(GetCode()->GetBufferPointer(), GetCode()->GetBufferSize(), nullptr, &Shader),
               "Failed to create " << shaderName << " geometry shader.");
}

void TGeometryShader::CreateWithStreamOutput(const std::string& shaderName,
                                             const D3D11_SO_DECLARATION_ENTRY soDeclarationEntries[],
                                             const unsigned int numSoDeclarationEntries,
                                             ID3D11Device* device) {
    ReleaseD3DObject(&Shader);
    Compile(shaderName, "gs_4_0");
    ENSURE_D3D(device->CreateGeometryShader(GetCode()->GetBufferPointer(), GetCode()->GetBufferSize(), nullptr, &Shader),
               "Failed to create " << shaderName << " geometry shader.");
    ENSURE_D3D(device->CreateGeometryShaderWithStreamOutput(GetCode()->GetBufferPointer(),
                                                            GetCode()->GetBufferSize(),
                                                            soDeclarationEntries,
                                                            numSoDeclarationEntries,
                                                            nullptr,
                                                            0,
                                                            D3D11_SO_NO_RASTERIZED_STREAM,
                                                            nullptr,
                                                            &Shader),
               "Failed to create " << shaderName << " geometry shader with stream output.");
}

ID3D11GeometryShader* TGeometryShader::GetShader() {
    return Shader;
}

// PIXEL SHADER //

TPixelShader::~TPixelShader() {
    ReleaseD3DObject(&Shader);
}

void TPixelShader::Create(const std::string& shaderName, ID3D11Device* device) {
    ReleaseD3DObject(&Shader);
    Compile(shaderName, "ps_4_1");
    ENSURE_D3D(device->CreatePixelShader(GetCode()->GetBufferPointer(), GetCode()->GetBufferSize(), nullptr, &Shader),
               "Failed to create " << shaderName << " pixel shader.");
}

ID3D11PixelShader* TPixelShader::GetShader() {
    return Shader;
}
