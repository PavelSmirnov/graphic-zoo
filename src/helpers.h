#pragma once
#include <string>

template <typename T>
inline T CeilToDivisible(const T value, const T divisor) {
    return (value + divisor - 1) / divisor;
}

std::string DefinitionToString(const float value);
