#pragma once
#include <exception>
#include <sstream>
#include <string>

class TEngineException : public std::exception {
public:
    TEngineException(const char* message)
        : Message(message) {}
    TEngineException(const std::string& message)
        : Message(message) {}

    inline const char* what() const throw () {
        return Message.c_str();
    }

private:
    std::string Message;
};

#define ENSURE(x, msg) { if (!(x)) { throw TEngineException((std::stringstream() << msg).str()); } }
