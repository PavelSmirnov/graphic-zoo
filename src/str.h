#pragma once
#include <string>
#include <vector>

inline bool IsWhiteSpace(const char c) {
    return c == ' ' ||
           c == '\n' ||
           c == '\r' ||
           c == '\t';
}

std::string StripLine(const std::string& str);
std::vector<std::string> SplitLine(const std::string& str, const char sep = ' ');
