#include "error.h"
#include "log.h"
#include <fstream>

namespace {
    bool LOG_ENABLED = false;
    std::string LOG_FILE_PATH = "";
    constexpr const char* HEX_DIGITS = "0123456789ABCDEF";
}

void EnableLogging(const char* logFilePath) {
    LOG_FILE_PATH = logFilePath;
    LOG_ENABLED = true;
    std::ofstream log(logFilePath);
}

void Log(const char* message) {
    if (LOG_ENABLED) {
        std::ofstream log(LOG_FILE_PATH, std::ostream::app);
        (log << message).flush();
    }
}

std::string ToHexStr(const int value) {
    std::string inHex = "0x00000000";
    for (int i = 0; i < 8; ++i) {
        inHex[9 - i] = HEX_DIGITS[(value >> (i * 4)) & 15];
    }
    return inHex;
}
