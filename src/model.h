#pragma once
#include <filesystem>
#include <string>
#include <vector>

struct TModelVertex {
    float positionX;
    float positionY;
    float positionZ;
    float textureU;
    float textureV;
    float normalX;
    float normalY;
    float normalZ;
};

struct TModel {
    std::string name;
    std::vector<unsigned int> indicesData;
    std::vector<TModelVertex> verticesData;
};

struct TMaterialDependency {
    std::string path;
    std::string name;
};

std::vector<std::pair<TModel, TMaterialDependency>> ReadModelsFromObj(const std::filesystem::path& path);

TModel GetCubeModel();
