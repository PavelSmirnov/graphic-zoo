#pragma once
#include <filesystem>
#include <string>

struct TMaterial {
    std::string name;
    float baseColorR = 1;
    float baseColorG = 1;
    float baseColorB = 1;
    float dielectricSpecular = 0;
    float metalness = 0;
    float roughness = 1;
    float height = 0;
};

struct TTextureDependency {
    std::string baseColor = "";
    std::string dielectricSpecular = "";
    std::string metalness = "";
    std::string roughness = "";
    std::string height = "";
};

std::vector<std::pair<TMaterial, TTextureDependency>> ReadMaterialsFromMtl(const std::filesystem::path& path);
