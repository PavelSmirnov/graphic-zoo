#include "asset_library.h"
#include "log.h"
#include "loop.h"
#include "model.h"
#include "object.h"
#include "os_win.h"
#include "renderer.h"
#include <algorithm>
#include <ctime>
#include <thread>
#include <vector>

namespace {
    constexpr int MAX_FPS = 60;
    constexpr float MIN_SECS_PER_FRAME = 1.0f / MAX_FPS;
    constexpr float CONTROLS_READING_TIME = MIN_SECS_PER_FRAME / 10;

    constexpr float SLOW_MOVEMENT_SPEED = 2e+0;
    constexpr float FAST_MOVEMENT_SPEED = 2e+3;
    constexpr float ROTATION_SPEED = 1e-3f;

    constexpr float LIGHT_CYCLE_PERIOD_IN_SEC = 60;//60 * 60;

    bool IS_QUIT_MESSAGE_POSTED = false;

    void RunRenderLoop(TWindow* window, TRenderer* renderer, TMouse* mouse, TAssetLibrary* assetLibrary) {
        TCamera camera = {0.1f, // near
                          10000, // far
                          PI_CONST * 0.4f,
                          PI_CONST * 0.4f * window->GetHeight() / window->GetWidth(),
                          {-4941.5f, 4949.5f, 2421.3f}, // eye
                          {-457.5f, -400.2f, 2859.3f}, // lookAt
                          {0, 0, 1}}; // up
        TFreeCamera freeCamera(camera, SLOW_MOVEMENT_SPEED, ROTATION_SPEED);

        TStorageItemId somethingModelId = assetLibrary->LoadModel("models/something/model.obj", "something");
        TStorageItemId cubeModelId = assetLibrary->LoadModel("models/color_cube/model.obj", "cube");
        TStorageItemId airplaneModelId = assetLibrary->LoadModel("models/airplane/model.obj", "airplane");
        TObject airplane = {
            airplaneModelId,
            {70, 70, 70}, // scale
            TQuaternion::FromRotation(-PI_CONST / 2, {0, 0, 1}), // rotation
            {0.0f, 3000.0f, 4500.0f}, // translation
            {500, 0, 0}, // velocity
            {0, 0, 0} // angular velocity
        };
        TStorageItemId sphereModelId = assetLibrary->LoadModel("models/sphere/model.obj", "sphere");

        unsigned int frame = 0;

        float lastFrameDuration = 0;
        while (!IS_QUIT_MESSAGE_POSTED) {
            // TODO: change std::clock() for single thread time alternative
            const clock_t frameStartTime = std::clock();

            bool isPause = false;
            if (!renderer->GetIsIdle()) {
                // TODO: for movements the fact of clicking must be fixated, not the state of movement mask at the end of the frame
                // Problem: one could click a button at the beginning of the frame and release before the end, and this action won't be recorded.

                // read controls
                isPause = window->GetPauseModifier();
                const auto movementMask = window->GetMovementMask();
                freeCamera.SetMovementSpeed(window->GetFastModifier() ? FAST_MOVEMENT_SPEED : SLOW_MOVEMENT_SPEED);
                int relativeX, relativeY;
                mouse->GetRelativeXYAndFlush(&relativeX, &relativeY);

                // process logic
                if (!isPause) {
                    const TVector<3> airplaneToCenterAxis = TVector<3>{airplane.translation(0), airplane.translation(1), 0};
                    const TVector<3> airplaneDirection = airplane.rotation.GetRotationMatrix3x3() * TVector<3>{0, 1, 0};
                    const bool turnRight = CalcScalarProduct(airplaneDirection, airplaneToCenterAxis) > 0;
                    const float airplaneRotationMomentum = 0.1f;
                    MoveObject(lastFrameDuration,
                               -CalcSquaredLength(airplane.velocity) / CalcSquaredLength(airplaneToCenterAxis) * airplaneToCenterAxis * airplane.mass,
                               TVector<3>({0, 0, turnRight ? -airplaneRotationMomentum : airplaneRotationMomentum}),
                               &airplane);
                }

                freeCamera.Move(lastFrameDuration, movementMask);
                freeCamera.RotateStabilized(relativeX, relativeY);
                const auto& camera = freeCamera.GetCamera();
                const float lightCyclePhase = PI_CONST - ((frame * MIN_SECS_PER_FRAME) / LIGHT_CYCLE_PERIOD_IN_SEC) * 2 * PI_CONST;
                renderer->SetLight({0, -7500 * std::cos(lightCyclePhase), 7500 * std::sin(lightCyclePhase)},
                                   {1, 1, 1, 10});
                renderer->UpdatePosition(camera.eye(0), camera.eye(1));
                renderer->UpdateCamera(camera);

                // stage models
                renderer->UnstageAllObjects();
                renderer->StageObject(airplaneModelId, GetTranslationMatrix(airplane.translation + airplane.centerOfMass) *
                                                       airplane.rotation.GetRotationMatrix() *
                                                       GetTranslationMatrix(-airplane.centerOfMass) *
                                                       GetScalingMatrix(airplane.scale));
                renderer->StageObject(somethingModelId, GetTranslationMatrix({-3974.77f, 1158.44f, 2714.01f}) * GetScalingMatrix({100, 100, 100}));
                renderer->StageObject(cubeModelId, GetTranslationMatrix({-1974.77f, 1758.44f, 3714.01f}) * GetScalingMatrix({100, 100, 100}));
                renderer->StageObject(sphereModelId, GetTranslationMatrix({-2000.0f, 2000.0f, 4000.0f}) * GetScalingMatrix({100, 100, 100}));
            }
            renderer->RenderAndUpdateIsIdle();

            while (double(std::clock() - frameStartTime) / CLOCKS_PER_SEC < MIN_SECS_PER_FRAME) {
                ; // wait
            }
            if (!isPause) {
                ++frame;
            }
            const clock_t frameEndTime = std::clock();
            lastFrameDuration = float(frameEndTime - frameStartTime) / CLOCKS_PER_SEC;
        }
    }

    void RunMessageLoop(TWindow* window, TRenderer* renderer) {
        MSG msg = {};
        while (true) {
            if (renderer->GetIsIdle()) {
                if (GetMessage(&msg, nullptr, 0, 0) > 0) {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                }
                renderer->RenderAndUpdateIsIdle();
                continue;
            }

            while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            if (msg.message == WM_QUIT) {
                IS_QUIT_MESSAGE_POSTED = true;
                break;
            }
        }
    }
}

void Loop(TWindow* window, TRenderer* renderer, TMouse* mouse, TAssetLibrary* assetLibrary) {
    IS_QUIT_MESSAGE_POSTED = false;
    std::thread renderLoop(RunRenderLoop, window, renderer, mouse, assetLibrary);
    RunMessageLoop(window, renderer);
    renderLoop.join();
}
