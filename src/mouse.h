#pragma once
#include "os_win.h"
#include <mutex>


class TMouse {
public:
    static TMouse* Create();
    static TMouse* Get();
    static void Destroy();

    void ProcessInput(const RAWINPUT* input);
    void GetRelativeXYAndFlush(int* relativeX, int* relativeY);

private:
    TMouse();
    void Flush();

    volatile int RelativeX = 0;
    volatile int RelativeY = 0;

    std::mutex MouseMutex;
};
