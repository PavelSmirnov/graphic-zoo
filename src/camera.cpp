#include "camera.h"
#include <algorithm>


TFreeCamera::TFreeCamera(const TCamera& camera,
                         const float movementSpeed,
                         const float rotationSpeed,
                         const float downLookMinAngle,
                         const float upLookMinAngle)
    : Camera(camera)
    , MovementSpeed(movementSpeed)
    , RotationSpeed(rotationSpeed)
    , DownLookMinAngle(downLookMinAngle)
    , UpLookMinAngle(upLookMinAngle) {
    CameraSpaceToWorldSpace = Transpose(GetViewMatrix(camera));
}

void TFreeCamera::Move(const float time, const int directions) {
    const int forwardStep = bool(directions & DIR_FORWARD) - bool(directions & DIR_BACKWARD);
    const int leftStep = bool(directions & DIR_LEFT) - bool(directions & DIR_RIGHT);
    const int upStep = bool(directions & DIR_UP) - bool(directions & DIR_DOWN);
    if (forwardStep == 0 && leftStep == 0 && upStep == 0) {
        return;
    }
    const TVector<4> cameraSpaceDirection = Normalize<4>({-float(leftStep), float(upStep), float(forwardStep), 0});
    const TVector<4> worldSpaceDirection = CameraSpaceToWorldSpace * cameraSpaceDirection;
    const TVector<3> delta = Get3DVector(worldSpaceDirection) * (time * MovementSpeed);
    Camera.lookAt += delta;
    Camera.eye += delta;
}

void TFreeCamera::RotateStabilized(const int dXOnScreen,
                                   const int dYOnScreen) {
    if (dXOnScreen == 0 && dYOnScreen == 0) {
        return;
    }
    const TVector<2> rotationDirection = TVector<2>{float(dXOnScreen), float(dYOnScreen)};
    TVector<2> rotationAngles = rotationDirection * RotationSpeed;
    const TVector<3> cameraDirection = Get3DVector(CameraSpaceToWorldSpace * TVector<4>{0, 0, 1, 0});
    const float currentZAngle = std::atan2(GetLength(TVector<2>{cameraDirection(0), cameraDirection(1)}), cameraDirection(2));
    rotationAngles(1) = std::max(rotationAngles(1), -currentZAngle + UpLookMinAngle);
    rotationAngles(1) = std::min(rotationAngles(1), PI_CONST - currentZAngle - DownLookMinAngle);

    const TTransformation xRotation = GetRotationMatrix({0, 0, -1}, rotationAngles(0));
    const TVector<3> yRotationAxis = Get3DVector(CameraSpaceToWorldSpace * TVector<4>{1, 0, 0, 0});
    const TTransformation yRotation = GetRotationMatrix(yRotationAxis, -rotationAngles(1));
    Camera.lookAt = Get3DVector(xRotation * (yRotation * Get4DVector(Camera.lookAt - Camera.eye))) + Camera.eye;
    Camera.up = Get3DVector(xRotation * (yRotation * Get4DVector(Camera.up)));
    CameraSpaceToWorldSpace = Transpose(GetViewMatrix(Camera));
}

void TFreeCamera::SetMovementSpeed(const float movementSpeed) {
    MovementSpeed = movementSpeed;
}

void TFreeCamera::SetRotationSpeed(const float rotationSpeed) {
    RotationSpeed = rotationSpeed;
}

const TCamera& TFreeCamera::GetCamera() const {
    return Camera;
}
