#pragma once
#include "mouse.h"
#include <windows.h>
#pragma comment(lib, "ole32.lib")
#pragma comment(lib, "user32.lib")

class TWindow {
public:
    static TWindow* Create(const HINSTANCE hInstance,
                           const int nCmdShow,
                           const int width,
                           const int height,
                           const bool isFullscreen);
    static TWindow* Get();
    static void Destroy();

    void Show();

    inline int GetWidth() const { return Width; }
    inline int GetHeight() const { return Height; }
    inline int GetIsFullscreen() const { return IsFullscreen; }
    inline HWND GetHwnd() const { return Hwnd; }

    int GetMovementMask() const;
    bool GetFastModifier() const;
    bool GetPauseModifier() const;

private:
    TWindow(const HINSTANCE hInstance,
            const int nCmdShow,
            const int width,
            const int height,
            const bool isFullscreen);

    const HINSTANCE HInstance;
    const int Width;
    const int Height;
    const bool IsFullscreen;
    const int NCmdShow;
    HWND Hwnd;
};
