#include "camera.h"
#include "error.h"
#include "log.h"
#include "os_win.h"
#include "window.h"
#include <windef.h>
#include <winuser.h>

namespace {
    TWindow* WINDOW = nullptr;

    int MOVEMENT_MASK = 0;
    bool FAST_MODIFIER = false;
    bool PAUSE_MODIFIER = false;

    LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
        switch (uMsg) {
        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
        case WM_INPUT: {
            if (wParam == RIM_INPUTSINK) {
                break;
            }
            unsigned int dataSize;
            GetRawInputData(HRAWINPUT(lParam), RID_INPUT, nullptr, &dataSize, sizeof(RAWINPUTHEADER));
            char* data = new char[dataSize];
            if (data == nullptr) {
                LOG("Error: Unable to allocate memory.");
                break;
            }
            if (GetRawInputData(HRAWINPUT(lParam), RID_INPUT, data, &dataSize, sizeof(RAWINPUTHEADER)) != dataSize) {
                LOG("Error: GetRawInputData does not return correct size.");
                break;
            }
            RAWINPUT* raw = (RAWINPUT*)data;
            if (raw->header.dwType == RIM_TYPEMOUSE) {
                TMouse::Get()->ProcessInput(raw);
            }
            delete[] data;
            break;
        }
        case WM_KEYDOWN:
            switch (wParam) {
            case 0x57: // w
                MOVEMENT_MASK |= TFreeCamera::DIR_FORWARD;
                break;
            case 0x41: // a
                MOVEMENT_MASK |= TFreeCamera::DIR_LEFT;
                break;
            case 0x53: // s
                MOVEMENT_MASK |= TFreeCamera::DIR_BACKWARD;
                break;
            case 0x44: // d
                MOVEMENT_MASK |= TFreeCamera::DIR_RIGHT;
                break;
            case 0x45: // e
                MOVEMENT_MASK |= TFreeCamera::DIR_DOWN;
                break;
            case 0x51: // q
                MOVEMENT_MASK |= TFreeCamera::DIR_UP;
                break;
            case VK_SHIFT:
                FAST_MODIFIER = true;
                break;
            case VK_SPACE:
                PAUSE_MODIFIER = !PAUSE_MODIFIER;
                break;

            case VK_ESCAPE:
                PostQuitMessage(0);
                return 0;
            }
            break;
        case WM_KEYUP:
            switch (wParam) {
            case 0x57: // w
                MOVEMENT_MASK &= ~TFreeCamera::DIR_FORWARD;
                break;
            case 0x41: // a
                MOVEMENT_MASK &= ~TFreeCamera::DIR_LEFT;
                break;
            case 0x53: // s
                MOVEMENT_MASK &= ~TFreeCamera::DIR_BACKWARD;
                break;
            case 0x44: // d
                MOVEMENT_MASK &= ~TFreeCamera::DIR_RIGHT;
                break;
            case 0x45: // e
                MOVEMENT_MASK &= ~TFreeCamera::DIR_DOWN;
                break;
            case 0x51: // q
                MOVEMENT_MASK &= ~TFreeCamera::DIR_UP;
                break;
            case VK_SHIFT:
                FAST_MODIFIER = false;
                break;
            }
            break;
        }
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
}

TWindow* TWindow::Create(const HINSTANCE hInstance,
                         const int nCmdShow,
                         const int width,
                         const int height,
                         const bool isFullscreen) {
    ENSURE(WINDOW == nullptr, "New window to be created, yet the old one is not destroyed.");
    return WINDOW = new TWindow(hInstance, nCmdShow, width, height, isFullscreen);
}

TWindow* TWindow::Get() {
    return WINDOW;
}

void TWindow::Destroy() {
    ENSURE(WINDOW != nullptr, "The window to be destroyed, but it is not created yet.");
    delete WINDOW;
    WINDOW = nullptr;
}

TWindow::TWindow(const HINSTANCE hInstance,
                 const int nCmdShow,
                 const int width,
                 const int height,
                 const bool isFullscreen)
    : HInstance(hInstance)
    , NCmdShow(nCmdShow)
    , Width(width)
    , Height(height)
    , IsFullscreen(isFullscreen) {

    WNDCLASS wc = {};
    wc.lpfnWndProc   = WindowProc;
    wc.hInstance     = hInstance;
    wc.lpszClassName = L"engine_window";
    RegisterClass(&wc);

    RECT userRect = {0, 0, Width - 1, Height - 1};
    ENSURE(AdjustWindowRectEx(&userRect, 0, false, 0),
           "Failed to adjust user rectangle for a window.");
    Hwnd = CreateWindowEx(0,
                          wc.lpszClassName,
                          L"graphic-zoo",
                          0,
                          0,
                          0,
                          userRect.right - userRect.left + 1,
                          userRect.bottom - userRect.top + 1,
                          NULL,
                          NULL,
                          HInstance,
                          NULL);
    ENSURE(Hwnd != nullptr, "Unable to create the window.");
    ENSURE(SetWindowLong(Hwnd, GWL_STYLE, 0), "Failed to change window style.");

    RAWINPUTDEVICE rid[1];
    // mouse
    rid[0].usUsagePage = 0x01;
    rid[0].usUsage = 0x02;
    rid[0].dwFlags = 0;
    rid[0].hwndTarget = 0;

    ENSURE(RegisterRawInputDevices(rid, sizeof(rid) / sizeof(RAWINPUTDEVICE), sizeof(RAWINPUTDEVICE)),
           "Failed to register input devices: " << GetLastError() << "\n");
}


void TWindow::Show() {
    ShowWindow(Hwnd, NCmdShow);
}

int TWindow::GetMovementMask() const {
    return MOVEMENT_MASK;
}

bool TWindow::GetFastModifier() const {
    return FAST_MODIFIER;
}

bool TWindow::GetPauseModifier() const {
    return PAUSE_MODIFIER;
}
