#include "asset_library.h"
#include "helpers.h"
#include "log.h"
#include "wic_tex.h"
#include <filesystem>

namespace {
    TAssetLibrary* ASSET_LIBRARY = nullptr;

    std::string JoinPathAndName(const std::string& path, const std::string& name) {
        return std::filesystem::path(path).string() + "#" + name;
    }
} // anonymous

TAssetLibrary* TAssetLibrary::Create(const std::filesystem::path& assetsPath, TDirectX* directx) {
    ENSURE(ASSET_LIBRARY == nullptr, "New asset library to be created, yet the old one is not destroyed.");
    return ASSET_LIBRARY = new TAssetLibrary(assetsPath, directx);
}

TAssetLibrary* TAssetLibrary::Get() {
    return ASSET_LIBRARY;
}

void TAssetLibrary::Destroy() {
    ENSURE(ASSET_LIBRARY != nullptr, "The asset library to be destroyed, but it is not created yet.");
    delete ASSET_LIBRARY;
    ASSET_LIBRARY = nullptr;
}

TStorageItemId TAssetLibrary::LoadModel(const std::string& path, const std::string& name) {
    const auto fullPath = AssetsPath / path;
    ENSURE(std::filesystem::exists(fullPath), "No asset found: " << fullPath);

    std::vector<std::pair<TModel, TMaterialDependency>> models = ReadModelsFromObj(fullPath);
    std::vector<TStorageItemId> modelIds;
    modelIds.reserve(models.size());
    for (auto& [model, materialDependency] : models) {
        if (model.name != name) {
            continue;
        }
        TStorageItemId materialId;
        if (materialDependency.path == "" && materialDependency.name == "") {
            materialId = DefaultMaterialId;
            LOG_WARNING("No material set for model '" << model.name << "' at " << path << ".\n");
        } else {
            const std::string dependencyPath = (std::filesystem::path(path).parent_path() / materialDependency.path).string();
            materialId = LoadMaterial(dependencyPath, materialDependency.name);
        }
        return LoadModel(std::move(model), materialId);
    }

    LOG_WARNING("No model with name '" << name << "' found at: " << path << ".");
    return DefaultModelId;
}

void TAssetLibrary::FillMaterialTextures(const TTextureDependency& textureDependency, const std::string& path, TMaterialData* materialData) {
    FillMaterialTexture(textureDependency.baseColor, path, &(materialData->baseColor), &(materialData->baseColorView));
    FillMaterialTexture(textureDependency.dielectricSpecular, path, &(materialData->dielectricSpecular), &(materialData->dielectricSpecularView));
    FillMaterialTexture(textureDependency.metalness, path, &(materialData->metalness), &(materialData->metalnessView));
    FillMaterialTexture(textureDependency.roughness, path, &(materialData->roughness), &(materialData->roughnessView));
    FillMaterialTexture(textureDependency.height, path, &(materialData->height), &(materialData->heightView));
}

void TAssetLibrary::FillMaterialTexture(const std::string& dependency, const std::string& path, ID3D11Texture2D** texture, ID3D11ShaderResourceView** view) {
    if (dependency == "") {
        return;
    }

    const auto fullPath = AssetsPath / std::filesystem::path(path).parent_path() / dependency;
    if (!std::filesystem::exists(fullPath)) {
        LOG_WARNING("No asset found: " << fullPath << ".\n");
        return;
    }

    const auto hr = CreateWICTextureFromFile(DirectX->GetDevice(), DirectX->GetContext(), fullPath.wstring().c_str(), (ID3D11Resource**)texture, view, 0);
    if (hr != S_OK) {
       LOG_ERROR("Failed to load WIC texture from file: " << fullPath << ".\n");
       *texture = nullptr;
       *view = nullptr;
       return;
    }
}

TStorageItemId TAssetLibrary::LoadMaterial(const std::string& path, const std::string& name) {
    const auto fullPath = AssetsPath / path;
    ENSURE(std::filesystem::exists(fullPath), "No asset found: " << fullPath);

    std::vector<std::pair<TMaterial, TTextureDependency>> materials = ReadMaterialsFromMtl(fullPath);
    for (auto& [material, textureDependency] : materials) {
        if (material.name != name) {
            continue;
        }
        return LoadMaterial(std::move(material), path, textureDependency);
    }

    LOG_WARNING("No material with name '" << name << "' found in " << path << ".\n");
    return DefaultMaterialId;
}

TStorageItemId TAssetLibrary::LoadModel(TModel&& model, const TStorageItemId materialId) {
    TModelData modelData = {};

    modelData.numModelIndices = static_cast<int>(model.indicesData.size());
    { // index buffer
        D3D11_BUFFER_DESC buffDesc = {};
        buffDesc.ByteWidth = static_cast<UINT>(model.indicesData.size() * sizeof(unsigned int));
        buffDesc.Usage = D3D11_USAGE_IMMUTABLE;
        buffDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
        buffDesc.CPUAccessFlags = 0;
        buffDesc.MiscFlags = 0;
        buffDesc.StructureByteStride = sizeof(int);

        D3D11_SUBRESOURCE_DATA storeDesc = {};
        storeDesc.pSysMem = model.indicesData.data();
        storeDesc.SysMemPitch = 0;
        storeDesc.SysMemSlicePitch = 0;

        ENSURE_D3D(DirectX->GetDevice()->CreateBuffer(&buffDesc, &storeDesc, &modelData.indexBuffer),
                   "Failed to create an index buffer for model " << model.name << ".");
    }
    { // vertex buffer
        D3D11_BUFFER_DESC buffDesc = {};
        buffDesc.ByteWidth = static_cast<UINT>(model.verticesData.size() * sizeof(TModelVertex));
        buffDesc.Usage = D3D11_USAGE_IMMUTABLE;
        buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        buffDesc.CPUAccessFlags = 0;
        buffDesc.MiscFlags = 0;
        buffDesc.StructureByteStride = sizeof(TModelVertex);

        D3D11_SUBRESOURCE_DATA storeDesc = {};
        storeDesc.pSysMem = model.verticesData.data();
        storeDesc.SysMemPitch = 0;
        storeDesc.SysMemSlicePitch = 0;

        ENSURE_D3D(DirectX->GetDevice()->CreateBuffer(&buffDesc, &storeDesc, &modelData.vertexBuffer),
                   "Failed to create a vertex buffer for model '" << model.name << "'.");
    }
    modelData.materialId = materialId;
    modelData.model = std::move(model);

    return ModelStorage.LoadItem(std::move(modelData));
}

TStorageItemId TAssetLibrary::LoadMaterial(TMaterial&& material, const std::string& path, const TTextureDependency& textureDependency) {
    TMaterialData materialData = {};

    FillMaterialTextures(textureDependency, path, &materialData);
    { // material buffer
        D3D11_BUFFER_DESC buffDesc = {};
        buffDesc.ByteWidth = static_cast<UINT>(sizeof(float) * 8);
        buffDesc.Usage = D3D11_USAGE_IMMUTABLE;
        buffDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
        buffDesc.CPUAccessFlags = 0;
        buffDesc.MiscFlags = 0;
        buffDesc.StructureByteStride = 0;

        unsigned int mapFlags = static_cast<unsigned int>(
            ((materialData.baseColorView != nullptr)          << 0) |
            ((materialData.dielectricSpecularView != nullptr) << 1) |
            ((materialData.metalnessView != nullptr)          << 2) |
            ((materialData.roughnessView != nullptr)          << 3) |
            ((materialData.heightView != nullptr)             << 4));
        char mapFlagsData[sizeof(float)];
        std::memcpy(mapFlagsData, &mapFlags, sizeof(float));
        float data[8] = {
            material.baseColorR,
            material.baseColorG,
            material.baseColorB,
            material.dielectricSpecular,
            material.metalness,
            material.roughness,
            material.height,
            *reinterpret_cast<float*>(mapFlagsData)
        };
        D3D11_SUBRESOURCE_DATA storeDesc = {};
        storeDesc.pSysMem = data;
        storeDesc.SysMemPitch = 0;
        storeDesc.SysMemSlicePitch = 0;

        ENSURE_D3D(DirectX->GetDevice()->CreateBuffer(&buffDesc, &storeDesc, &materialData.materialBuffer),
                   "Failed to create a buffer for material '" << material.name << "'.");
    }
    materialData.material = std::move(material);

    return MaterialStorage.LoadItem(std::move(materialData));
}

void TAssetLibrary::UnloadModel(const TStorageItemId modelId) {
    const TModelData& model = ModelStorage.GetItem(modelId);
    if (model.materialId != DefaultMaterialId) {
        MaterialStorage.UnloadItem(model.materialId);
    }
    ModelStorage.UnloadItem(modelId);
}

void TAssetLibrary::GetModel(const TStorageItemId modelId, int* numIndices, ID3D11Buffer** indexBuffer, ID3D11Buffer** vertexBuffer, TStorageItemId* materialId) {
    TModelData& modelData = ModelStorage.GetItem(modelId);
    *numIndices = modelData.numModelIndices;
    *indexBuffer = modelData.indexBuffer;
    *vertexBuffer = modelData.vertexBuffer;
    *materialId = modelData.materialId;
}

void TAssetLibrary::GetMaterial(const TStorageItemId materialId,
                                ID3D11Buffer** materialBuffer,
                                ID3D11ShaderResourceView** baseColorView,
                                ID3D11ShaderResourceView** dielectricSpecularView,
                                ID3D11ShaderResourceView** metalnessView,
                                ID3D11ShaderResourceView** roughnessView,
                                ID3D11ShaderResourceView** heightView) {
    TMaterialData& materialData = MaterialStorage.GetItem(materialId);
    *materialBuffer = materialData.materialBuffer;
    *baseColorView = materialData.baseColorView;
    *dielectricSpecularView = materialData.dielectricSpecularView;
    *metalnessView = materialData.metalnessView;
    *roughnessView = materialData.roughnessView;
    *heightView = materialData.heightView;
}

TAssetLibrary::TAssetLibrary(const std::filesystem::path& assetsPath, TDirectX* directx)
    : AssetsPath(assetsPath)
    , DirectX(directx) {
    DefaultMaterialId = LoadMaterial(TMaterial{}, "", TTextureDependency{});
    DefaultModelId = LoadModel(GetCubeModel(),
                               DefaultMaterialId);
}

TAssetLibrary::TModelData::TModelData(const TModelData& other) {
    numModelIndices = other.numModelIndices;
    indexBuffer = other.indexBuffer;
    AddRefSafe(indexBuffer);
    vertexBuffer = other.vertexBuffer;
    AddRefSafe(vertexBuffer);
    materialId = other.materialId;
    model = std::move(other.model);
}

TAssetLibrary::TModelData::TModelData(TModelData&& other) {
    this->swap(other);
}

TAssetLibrary::TModelData::~TModelData() {
    ReleaseSafe(indexBuffer);
    ReleaseSafe(vertexBuffer);
}

TAssetLibrary::TModelData& TAssetLibrary::TModelData::operator=(TModelData other) {
    this->swap(other);
    return *this;
}

void TAssetLibrary::TModelData::swap(TModelData& other) {
    std::swap(numModelIndices, other.numModelIndices);
    std::swap(indexBuffer, other.indexBuffer);
    std::swap(vertexBuffer, other.vertexBuffer);
    std::swap(materialId, other.materialId);
    std::swap(model, other.model);
}

TAssetLibrary::TMaterialData::TMaterialData(const TMaterialData& other) {
    AddRefSafe(materialBuffer = other.materialBuffer);
    material = std::move(other.material);
    AddRefSafe(baseColor = other.baseColor);
    AddRefSafe(baseColorView = other.baseColorView);
    AddRefSafe(dielectricSpecular = other.dielectricSpecular);
    AddRefSafe(dielectricSpecularView = other.dielectricSpecularView);
    AddRefSafe(metalness = other.metalness);
    AddRefSafe(metalnessView = other.metalnessView);
    AddRefSafe(roughness = other.roughness);
    AddRefSafe(roughnessView = other.roughnessView);
    AddRefSafe(height = other.height);
    AddRefSafe(heightView = other.heightView);
}

TAssetLibrary::TMaterialData::TMaterialData(TMaterialData&& other) {
    this->swap(other);
}

TAssetLibrary::TMaterialData::~TMaterialData() {
    ReleaseSafe(materialBuffer);
    ReleaseSafe(baseColor);
    ReleaseSafe(baseColorView);
    ReleaseSafe(dielectricSpecular);
    ReleaseSafe(dielectricSpecularView);
    ReleaseSafe(metalness);
    ReleaseSafe(metalnessView);
    ReleaseSafe(roughness);
    ReleaseSafe(roughnessView);
    ReleaseSafe(height);
    ReleaseSafe(heightView);
}

TAssetLibrary::TMaterialData& TAssetLibrary::TMaterialData::operator=(TMaterialData other) {
    this->swap(other);
    return *this;
}

void TAssetLibrary::TMaterialData::swap(TMaterialData& other) {
    std::swap(materialBuffer, other.materialBuffer);
    std::swap(material, other.material);
    std::swap(baseColor, other.baseColor);
    std::swap(baseColorView, other.baseColorView);
    std::swap(dielectricSpecular, other.dielectricSpecular);
    std::swap(dielectricSpecularView, other.dielectricSpecularView);
    std::swap(metalness, other.metalness);
    std::swap(metalnessView, other.metalnessView);
    std::swap(roughness, other.roughness);
    std::swap(roughnessView, other.roughnessView);
    std::swap(height, other.height);
    std::swap(heightView, other.heightView);
}
