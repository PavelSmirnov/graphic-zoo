#include "error.h"
#include "log.h"
#include "mouse.h"


namespace {
    TMouse* MOUSE = nullptr;
}

TMouse* TMouse::Create() {
    ENSURE(MOUSE == nullptr, "New mouse to be created, yet the old one is not destroyed.");
    return MOUSE = new TMouse();
}

TMouse* TMouse::Get() {
    return MOUSE;
}

void TMouse::Destroy() {
    ENSURE(MOUSE != nullptr, "The mouse to be destroyed, but it is not created yet.");
    delete MOUSE;
    MOUSE = nullptr;
}

void TMouse::ProcessInput(const RAWINPUT* input) {
    std::scoped_lock lock(MouseMutex);
    if (input->data.mouse.usFlags & MOUSE_MOVE_ABSOLUTE) {
        LOG("Error: mouse outputs absolute position.");
    }
    RelativeX += input->data.mouse.lLastX;
    RelativeY += input->data.mouse.lLastY;
}

void TMouse::GetRelativeXYAndFlush(int* relativeX, int* relativeY) {
    std::scoped_lock lock(MouseMutex);
    *relativeX = RelativeX;
    *relativeY = RelativeY;
    Flush();
}

void TMouse::Flush() {
    RelativeX = 0;
    RelativeY = 0;
}

TMouse::TMouse() {}
