#include "error.h"
#include "log.h"
#include "material.h"
#include "str.h"
#include <fstream>

std::vector<std::pair<TMaterial, TTextureDependency>> ReadMaterialsFromMtl(const std::filesystem::path& path) {
    TMaterial material;
    TTextureDependency textureDependency;

    std::ifstream inputFile(path);
    std::string fileLine;

    std::vector<std::pair<TMaterial, TTextureDependency>> result;

    bool readFirstMaterialStatement = false;
    int lineNum = 0;
    while (std::getline(inputFile, fileLine)) {
        ++lineNum;
        fileLine = StripLine(fileLine);
        if (fileLine.empty() || fileLine[0] == '#') {
            continue;
        }
        const auto tokens = SplitLine(fileLine);
        const std::string& statement = tokens[0]; // tokens not empty since fileLine.empty() == false
        if (statement == "newmtl") {
            if (readFirstMaterialStatement) {
                result.emplace_back(material, textureDependency);
            }
            readFirstMaterialStatement = true;
            ENSURE(tokens.size() == 2, "Invalid material format line " << lineNum << ", file " << path << ".");
            material = TMaterial();
            textureDependency = TTextureDependency();
            material.name = tokens[1];
        } else if (statement == "Kd") {
            ENSURE(tokens.size() == 4, "Invalid material format line " << lineNum << ", file " << path << ".");
            material.baseColorR = std::stof(tokens[1]);
            material.baseColorG = std::stof(tokens[2]);
            material.baseColorB = std::stof(tokens[3]);
        } else if (statement == "Ks") {
            ENSURE(tokens.size() == 4, "Invalid material format line " << lineNum << ", file " << path << ".");
            material.dielectricSpecular = std::stof(tokens[1]); // Blender writes 3 identical values for Ks field
        } else if (statement == "Ka") {
            ENSURE(tokens.size() == 4, "Invalid material format line " << lineNum << ", file " << path << ".");
            material.metalness = std::stof(tokens[1]); // Blender writes 3 identical values for Kd field
        } else if (statement == "Ns") {
            ENSURE(tokens.size() == 2, "Invalid material format line " << lineNum << ", file " << path << ".");
            material.roughness = 1 - std::sqrt(std::stof(tokens[1]) / 900);
        } else if (statement == "map_Kd") {
            ENSURE(tokens.size() == 2, "Invalid material format line " << lineNum << ", file " << path << ".");
            textureDependency.baseColor = tokens[1];
        } else if (statement == "map_Ks") {
            ENSURE(tokens.size() == 2, "Invalid material format line " << lineNum << ", file " << path << ".");
            textureDependency.dielectricSpecular = tokens[1];
        } else if (statement == "refl") {
            ENSURE(tokens.size() == 2, "Invalid material format line " << lineNum << ", file " << path << ".");
            textureDependency.metalness = tokens[1];
        } else if (statement == "map_Ns") {
            ENSURE(tokens.size() == 2, "Invalid material format line " << lineNum << ", file " << path << ".");
            textureDependency.roughness = tokens[1];
        } else {
            LOG_WARNING("Don't know what to do with statement '" << statement << "' while reading material in line " << lineNum << ", file " << path << ".\n");
        }
    }
    if (readFirstMaterialStatement) {
        result.emplace_back(material, textureDependency);
    }
    return result;
}
