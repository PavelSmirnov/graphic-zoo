#pragma once
#include "error.h"
#include <set>
#include <unordered_map>
#include <vector>

using TStorageItemId = int;
constexpr const TStorageItemId NO_ITEM = -1;

template <typename TItem>
class TStorage {
public:
    TStorageItemId LoadItem(TItem&& item) {
        unsigned int localItemId;
        if (FreeLocalItemIds.empty()) {
            localItemId = static_cast<unsigned int>(ItemIdToLocalItemId.size());
        } else {
            auto iter = FreeLocalItemIds.begin();
            localItemId = *iter;
            FreeLocalItemIds.erase(iter);
        }

        int itemId = NumItemIds;
        ++NumItemIds;
        ItemIdToLocalItemId[itemId] = localItemId;

        if (localItemId >= Items.size()) {
            Items.emplace_back();
            ENSURE(localItemId < Items.size(), "Something went wrong with local item id expanding in TStorage.");
        }
        Items[localItemId] = std::move(item);

        return itemId;
    }

    TItem& GetItem(const TStorageItemId itemId) {
        auto localIdIter = ItemIdToLocalItemId.find(itemId);
        ENSURE(localIdIter != ItemIdToLocalItemId.end(), "Attempted to get unexisting item with id " << itemId << ".");
        const int localId = localIdIter->second;
        return Items[itemId];
    }

    void UnloadItem(const TStorageItemId itemId) {
        auto localIdIter = ItemIdToLocalItemId.find(itemId);
        ENSURE(localIdIter != ItemIdToLocalItemId.end(), "Attempted to unload unexisting item with id " << itemId << ".");
        const int localId = localIdIter->second;

        Items[localId] = TItem();

        FreeLocalItemIds.insert(localId);
        if (localId != Items.size() - 1) {
            return;
        }

        int freeLocalId = localId;
        while (true) {
            auto iter = FreeLocalItemIds.find(freeLocalId);
            if (iter == FreeLocalItemIds.end()) {
                break;
            }
            FreeLocalItemIds.erase(iter);
            Items.pop_back();
            if (freeLocalId == 0) {
                break;
            }
            --freeLocalId;
        }
    }

private:
    unsigned int NumItemIds = 0;
    std::unordered_map<TStorageItemId, unsigned int> ItemIdToLocalItemId;
    std::set<int> FreeLocalItemIds;
    std::vector<TItem> Items;
};
