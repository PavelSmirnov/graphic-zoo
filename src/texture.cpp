#include "texture.h"

TTexture2D::TTexture2D() {
}

TTexture2D& TTexture2D::Create(const unsigned int width,
                               const unsigned int height,
                               const DXGI_FORMAT format,
                               const D3D11_USAGE usage,
                               const unsigned int mipLevels,
                               const unsigned int arraySize,
                               const unsigned int bindFlags,
                               const unsigned int cpuAccessFlags,
                               const unsigned int miscFlags,
                               ID3D11Device* device,
                               const D3D11_SUBRESOURCE_DATA* dataDesc) {
    Width = width;
    Height = height;
    Format = format;

    D3D11_TEXTURE2D_DESC desc = {};
    desc.Width = Width;
    desc.Height = Height;
    desc.MipLevels = mipLevels;
    desc.ArraySize = arraySize;
    desc.Format = Format;
    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;
    desc.Usage = usage;
    desc.BindFlags = bindFlags;
    desc.CPUAccessFlags = cpuAccessFlags;
    desc.MiscFlags = miscFlags;

    ENSURE_D3D(device->CreateTexture2D(&desc, dataDesc, &Texture),
               "Failed to create a 2D texture.");

    return *this;
}

TTexture2D::~TTexture2D() {
    ReleaseD3DObject(&Texture);
    for (auto* srv : Srv) {
        ReleaseD3DObject(&srv);
    }
    for (auto* rtv : Rtv) {
        ReleaseD3DObject(&rtv);
    }
    for (auto* dsv : Dsv) {
        ReleaseD3DObject(&dsv);
    }
}

ID3D11Texture2D* TTexture2D::GetResource() {
    return Texture;
}

ID3D11ShaderResourceView** TTexture2D::GetSrv(int index) {
    return (index > Srv.size()) ? nullptr : &Srv[index];
}

ID3D11RenderTargetView** TTexture2D::GetRtv(int index) {
    return (index > Rtv.size()) ? nullptr : &Rtv[index];
}

ID3D11DepthStencilView** TTexture2D::GetDsv(int index) {
    return (index > Dsv.size()) ? nullptr : &Dsv[index];
}

TTexture2D& TTexture2D::AddSrv(ID3D11Device* device,
                               const unsigned int mipLevels,
                               const unsigned int mostDetailedMip,
                               const DXGI_FORMAT format) {
    D3D11_SHADER_RESOURCE_VIEW_DESC desc = {};
    desc.Format = (format == DXGI_FORMAT_UNKNOWN) ? Format : format;
    desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    desc.Texture2D.MipLevels = mipLevels;
    desc.Texture2D.MostDetailedMip = mostDetailedMip;
    ENSURE_D3D(device->CreateShaderResourceView(Texture, &desc, &Srv.emplace_back(nullptr)),
               "Failed to create a shader resource view.");
    return *this;
}

TTexture2D& TTexture2D::AddArraySrv(ID3D11Device* device,
                                    const unsigned int mipLevels,
                                    const unsigned int mostDetailedMip,
                                    const unsigned int firstArraySlice,
                                    const unsigned int arraySize,
                                    const DXGI_FORMAT format) {
    D3D11_SHADER_RESOURCE_VIEW_DESC desc = {};
    desc.Format = (format == DXGI_FORMAT_UNKNOWN) ? Format : format;
    desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
    desc.Texture2DArray.MipLevels = mipLevels;
    desc.Texture2DArray.MostDetailedMip = mostDetailedMip;
    desc.Texture2DArray.FirstArraySlice = firstArraySlice;
    desc.Texture2DArray.ArraySize = arraySize;
    ENSURE_D3D(device->CreateShaderResourceView(Texture, &desc, &Srv.emplace_back(nullptr)),
               "Failed to create a shader resource view.");
    return *this;
}

TTexture2D& TTexture2D::AddRtv(ID3D11Device* device,
                               const unsigned int mipSlice,
                               const DXGI_FORMAT format) {
    D3D11_RENDER_TARGET_VIEW_DESC desc = {};
    desc.Format = (format == DXGI_FORMAT_UNKNOWN) ? Format : format;
    desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
    desc.Texture2D.MipSlice = mipSlice;
    ENSURE_D3D(device->CreateRenderTargetView(Texture, &desc, &Rtv.emplace_back(nullptr)),
               "Failed to create a render target view.");
    return *this;
}

TTexture2D& TTexture2D::AddArrayRtv(ID3D11Device* device,
                                    const unsigned int mipSlice,
                                    const unsigned int firstArraySlice,
                                    const unsigned int arraySize,
                                    const DXGI_FORMAT format) {
    D3D11_RENDER_TARGET_VIEW_DESC desc = {};
    desc.Format = (format == DXGI_FORMAT_UNKNOWN) ? Format : format;
    desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
    desc.Texture2DArray.MipSlice = mipSlice;
    desc.Texture2DArray.FirstArraySlice = firstArraySlice;
    desc.Texture2DArray.ArraySize = arraySize;
    ENSURE_D3D(device->CreateRenderTargetView(Texture, &desc, &Rtv.emplace_back(nullptr)),
               "Failed to create a render target view.");
    return *this;
}

TTexture2D& TTexture2D::AddDsv(ID3D11Device* device,
                               const unsigned int mipSlice,
                               const DXGI_FORMAT format) {
    D3D11_DEPTH_STENCIL_VIEW_DESC desc = {};
    desc.Format = (format == DXGI_FORMAT_UNKNOWN) ? Format : format;
    desc.Flags = 0;
    desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    desc.Texture2D.MipSlice = 0;
    ENSURE_D3D(device->CreateDepthStencilView(Texture, &desc, &Dsv.emplace_back(nullptr)),
               "Failed to create a depth stencil view.");
    return *this;
}

TTexture2D& TTexture2D::AddArrayDsv(ID3D11Device* device,
                                    const unsigned int mipSlice,
                                    const unsigned int firstArraySlice,
                                    const unsigned int arraySize,
                                    const DXGI_FORMAT format) {
    D3D11_DEPTH_STENCIL_VIEW_DESC desc = {};
    desc.Format = (format == DXGI_FORMAT_UNKNOWN) ? Format : format;
    desc.Flags = 0;
    desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
    desc.Texture2DArray.MipSlice = mipSlice;
    desc.Texture2DArray.FirstArraySlice = firstArraySlice;
    desc.Texture2DArray.ArraySize = arraySize;
    ENSURE_D3D(device->CreateDepthStencilView(Texture, &desc, &Dsv.emplace_back(nullptr)),
               "Failed to create a depth stencil view.");
    return *this;
}
