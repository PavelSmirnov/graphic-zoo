#pragma once
#include "asset_library.h"
#include "camera.h"
#include "constants.h"
#include "d3d.h"
#include "geometry.h"
#include "model.h"
#include "pipeline.h"
#include "shaders.h"
#include "texture.h"
#include "window.h"
#include <filesystem>
#include <set>
#include <unordered_map>

class TRenderer {
public:
    static TRenderer* Create(const TWindow* window, TDirectX* directx, TAssetLibrary* assetLibrary);
    static TRenderer* Get();
    static void Destroy();

    bool GetIsIdle() const;
    void RenderAndUpdateIsIdle();
    void UpdatePosition(const float positionX, const float positionY);
    void UpdateCamera(const TCamera& camera);
    void SetFog(const float volumetricFogDensity, float volumetricFogHeight, float distanceFogStart);
    void SetLight(const TVector<3>& lightPosition, const TVector<4>& colorAndStrength);
    void StageObject(const TStorageItemId modelId, const TTransformation& transformation);
    void UnstageAllObjects();

private:
    TRenderer(const TWindow* window, TDirectX* directx, TAssetLibrary* assetLibrary);
    ~TRenderer();

    void InitializeSwapChain();
    void InitializeResources();
    void InitializeSphereSamples(const unsigned int sizeX,
                                 const unsigned int sizeY,
                                 const bool hemisphereOnly,
                                 TTexture2D* samplesTexture);
    void InitializeShaders();
    void InitializeTextures();
    void InitializeBuffers();
    void InitializePipelines();
    void PrecalcScattering();
    void PrecalcHarmonicValues();
    void PrecalcReflectanceHarmonics();
    void PrecalcSpecularBrdfAndDiffuseFresnel();

    void SetPipeline(const TVariablePipeline& pipeline);

    void UpdateViewProjectionBuffer();
    void UpdateLightViewProjectionBuffer(const int shadowPart);
    void UpdateLightBuffer();
    void UpdateAmbientBuffer(const TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS>& red,
                             const TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS>& green,
                             const TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS>& blue);
    void UpdateFocalPointBuffer();
    void UpdateFogBuffer();
    void UpdateTaaBuffer();
    void UpdateHdrBuffer(const float averageLuma);
    void UpdateObjectTransformationBuffer(const TTransformation& transformation);

    void CalculateShadowMapSetup();
    void CalculateVolumetricFogColor();
    TVector<3> CalculateAverageColor(ID3D11Texture2D* srv);
    TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS> CalculateHarmonicValues(ID3D11ShaderResourceView* resource);

    void DrawObject(const TVariablePipeline& pipeline, const TStorageItemId modelId, const TTransformation& transformation);

    void Render();

    // TODO: make a path type
    bool ReadTiffTextureFromFile(const wchar_t* path,
                                 const bool generateMipMaps,
                                 ID3D11Texture2D** texture,
                                 ID3D11ShaderResourceView** view);

    const TWindow* const Window;
    bool IsIdle = false;

    const std::filesystem::path ShadersPath;

    IDXGISwapChain1* SwapChain = nullptr;
    ID3D11Device* Device = nullptr;
    ID3D11DeviceContext* Context = nullptr;

    ID3D11Texture2D* BackBuffer = nullptr;
    ID3D11RenderTargetView* BackBufferRenderTargetView = nullptr;

    TAssetLibrary* AssetLibrary = nullptr;

    TFixedPipeline CurrentPipeline;
    TVariablePipeline DefaultPipeline;

    TVariablePipeline AmbientPipeline;
    TVariablePipeline CalculateDiffuseFresnelPipeline;
    TVariablePipeline CalculateSpecularBrdfPipeline;
    TVariablePipeline ConvertPixelPipeline;
    TVariablePipeline DiffuseLightingTermsPipeline;
    TVariablePipeline DistanceFogPipeline;
    TVariablePipeline HarmonicValuesPipeline;
    TVariablePipeline HorizonDirectionsPipeline;
    TVariablePipeline HorizonPipeline;
    TVariablePipeline LightPipeline;
    TVariablePipeline MultiplyByHarmonicsPipeline;
    TVariablePipeline NormalizeHDRPipeline;
    TVariablePipeline ObjectDrawPipeline;
    TVariablePipeline ObjectShadowPipeline;
    TVariablePipeline PrecalcScatteringPipeline;
    TVariablePipeline QuadmapDrawPipeline;
    TVariablePipeline QuadmapShadowPipeline;
    TVariablePipeline QuadtreeUpdatePipeline;
    TVariablePipeline ReflectionsHarmonicValuesPipeline;
    TVariablePipeline ReflectionsPipeline;
    TVariablePipeline SeparateRGBPipeline;
    TVariablePipeline SkyDirectionsPipeline;
    TVariablePipeline SkyPipeline;
    TVariablePipeline SkySamplesPipeline;
    TVariablePipeline TaaPipeline;
    TVariablePipeline ViewPipeline;
    TVariablePipeline VolumetricFogPipeline;

    TVertexShader ViewVS;
    TVertexShader QuadtreeVS;
    TVertexShader QuadmapVS;
    TVertexShader ObjectVS;
    THullShader QuadmapHS;
    TDomainShader QuadmapDS;
    TGeometryShader HarmonicsArrayGS;
    TGeometryShader HorizonGS;
    TGeometryShader QuadmapGS;
    TGeometryShader QuadtreeGS;
    TGeometryShader SkyDirectionsGS;
    TGeometryShader ViewGS;
    TPixelShader AmbientPS;
    TPixelShader DiffuseFresnelPS;
    TPixelShader DiffuseLightingTermsPS;
    TPixelShader DistanceFogPS;
    TPixelShader HarmonicValuesPS;
    TPixelShader HdrPS;
    TPixelShader LightPS;
    TPixelShader MultiplyByHarmonicsPS;
    TPixelShader ObjectPS;
    TPixelShader PrecalcScatteringPS;
    TPixelShader QuadmapPS;
    TPixelShader ReflectionsPS;
    TPixelShader SeparateRGBPS;
    TPixelShader SkyDirectionsPS;
    TPixelShader SkyPS;
    TPixelShader SpecularBrdfPS;
    TPixelShader TaaPS;
    TPixelShader ViewPS;
    TPixelShader VolumetricFogPS;

    ID3D11BlendState* AdditiveBlend = nullptr;
    ID3D11BlendState* ProportionalBlend = nullptr;
    ID3D11DepthStencilState* DepthStencilState = nullptr;
    ID3D11RasterizerState* DrawRasterizerState = nullptr;
    ID3D11RasterizerState* ShadowDepthRasterizerState = nullptr;
    ID3D11SamplerState* AllLinearClampSamplerState = nullptr;
    ID3D11SamplerState* AllPointClampComparisonSamplerState = nullptr;
    ID3D11SamplerState* AllPointZeroSamplerState = nullptr;

    D3D11_VIEWPORT HorizonViewport = {};
    D3D11_VIEWPORT LightScreenViewport = {};
    D3D11_VIEWPORT ReflectanceViewport = {};
    D3D11_VIEWPORT ScatteringViewport = {};
    D3D11_VIEWPORT ScreenViewport = {};
    D3D11_VIEWPORT SkySamplesViewport = {};
    D3D11_VIEWPORT UnitViewport = {};

    TTexture2D AverageColorCalculation;
    TTexture2D AverageColorCalculationStaging;
    TTexture2D AverageColorImage;
    TTexture2D DepthStencil;
    TTexture2D DiffuseFresnel;
    TTexture2D DiffuseLightingTerms;
    TTexture2D GBuffer[4];
    TTexture2D Horizon;
    TTexture2D HorizonDirections;
    TTexture2D HorizonStaging;
    TTexture2D LitScene;
    TTexture2D NormalizedHdr;
    TTexture2D PrecalcedScattering;
    TTexture2D ReflectionDirections;
    TTexture2D ReflectionsHarmonicValues;
    TTexture2D SeparatedRGB;
    TTexture2D ShadowMap;
    TTexture2D Sky;
    TTexture2D SkyDirections;
    TTexture2D SkySamples;
    TTexture2D ReflectanceCalculation;
    TTexture2D ReflectanceSphereSamples;
    TTexture2D SphereSamples;
    TTexture2D SphericalHarmonicsCalculation;
    TTexture2D SphericalHarmonicsCalculationStaging;
    TTexture2D SphericalHarmonicValues;
    TTexture2D TaaFrames[2];

    ID3D11DepthStencilView* ShadowMapDepthViewPtr = nullptr;
    ID3D11ShaderResourceView* ViewInputResourcePtr = nullptr;

    TTransformation ShadowMapProjectionMatrices[NUM_SHADOW_MAPS];
    TTransformation ShadowMapViewMatrices[NUM_SHADOW_MAPS];
    TTransformation ShadowMapViewProjectionMatrices[NUM_SHADOW_MAPS];
    float ShadowMapViewWidths[NUM_SHADOW_MAPS];

    ID3D11Buffer* SingleZeroIndexBuffer = nullptr;

    bool HasQuadtreeBeenUpdatedAtLeastOnce = false;
    ID3D11Buffer* QuadtreeNodesFrontBufferPtr = nullptr;
    ID3D11Buffer* QuadtreeNodesBackBufferPtr = nullptr;
    ID3D11Buffer* QuadtreeNodesMainBuffers[2] = {nullptr, nullptr};

    unsigned int CurrentFrame = 0;

    float PositionX = 0;
    float PositionY = 0;

    TVector<4> LightPosition = {0, 0, 1, 0};
    TVector<4> LightColorAndStrength = {1, 1, 1, 1};
    ID3D11Buffer* LightBuffer = nullptr;

    float FocalPointX;
    float FocalPointY;
    float HorizontalFov;
    ID3D11Buffer* FocalPointBuffer = nullptr;

    ID3D11Texture2D* NightSky = nullptr;
    ID3D11ShaderResourceView* NightSkyResource = nullptr;
    ID3D11Texture2D* TerrainHeightsTexture = nullptr;
    ID3D11ShaderResourceView* TerrainHeightsView = nullptr;
    ID3D11Texture2D* TerrainNormalTexture = nullptr;
    ID3D11ShaderResourceView* TerrainNormalView = nullptr;
    ID3D11Texture2D* TerrainAlbedoTexture = nullptr;
    ID3D11ShaderResourceView* TerrainAlbedoView = nullptr;
    ID3D11Texture2D* TerrainDetailTexture = nullptr;
    ID3D11ShaderResourceView* TerrainDetailView = nullptr;
    ID3D11Buffer* TerrainPlacementBuffer = nullptr;

    TCamera Camera = {};
    bool HaveHistoryFrames = false;
    ID3D11Buffer* ViewProjectionBuffer = nullptr;
    ID3D11Buffer* LightViewProjectionBuffer = nullptr;
    ID3D11Buffer* FogBuffer = nullptr;

    ID3D11Buffer* MaterialBufferPtr = nullptr;
    ID3D11Buffer* ObjectTransformationBuffer = nullptr;
    ID3D11Buffer* ObjectIndexBufferPtr = nullptr;
    ID3D11Buffer* ObjectVertexBufferPtr = nullptr;
    std::vector<TStorageItemId> StagedObjectsModelIds;
    std::vector<TTransformation> StagedObjectsTransformations;

    ID3D11ShaderResourceView* TaaHistoryResourcePtr = nullptr;
    ID3D11ShaderResourceView* TaaNewFrameResourcePtr = nullptr;
    ID3D11RenderTargetView* TaaBlendedTargetPtr = nullptr;
    int TaaFrontBufferIndex = 0;
    ID3D11Buffer* TaaBuffer = nullptr;
    ID3D11Buffer* HdrBuffer = nullptr;
    TTransformation PreviousFrameViewProjectionMatrix;

    ID3D11ShaderResourceView* NormalizeHDRInputResourcePtr = nullptr;

    int PlaneModelId = -1;

    ID3D11ShaderResourceView* BaseColorViewPtr = nullptr;
    ID3D11ShaderResourceView* DielectricSpecularViewPtr = nullptr;
    ID3D11ShaderResourceView* MetalnessViewPtr = nullptr;
    ID3D11ShaderResourceView* RoughnessViewPtr = nullptr;
    ID3D11ShaderResourceView* HeightViewPtr = nullptr;

    float VolumetricFogDensity = 1.5e-3f;
    float VolumetricFogHeight = 1.2e3f;
    float DistanceFogStart = 0.9f;
    TVector<3> FogColor;

    ID3D11ShaderResourceView* FunctionToHarmonizeResourcePtr = nullptr;
    TVector<SPHERICAL_HARMONICS_NUM_COEFFICIENTS> DiffuseHarmonics;
    ID3D11Buffer* AmbientBuffer = nullptr;
    ID3D11Texture3D* SpecularBrdf = nullptr;
    ID3D11ShaderResourceView* SpecularBrdfResource = nullptr;
    ID3D11Buffer* ReflectanceParametersBuffer = nullptr;
};
