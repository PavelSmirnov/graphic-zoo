#pragma once
#include "error.h"
#include "log.h"
#include <initializer_list>


constexpr const float PI_CONST = 3.14159265359f;


template <typename T>
T CalcSquare(const T& value) {
    return value * value;
}


float CalcDet(const float a00, const float a01,
              const float a10, const float a11);


template <unsigned int n = 4>
class TVector {
public:
    TVector() {}

    static TVector Zero() {
        TVector zero;
        for (int pos = 0; pos < n; ++pos) {
            zero.Data[pos] = 0;
        }
        return zero;
    }

    TVector(const TVector<n>& other) {
        std::memcpy(Data, other.GetData(), GetDataSize());
    }

    TVector(const std::initializer_list<float>& components) {
        ENSURE(components.size() == n,
               "Wrong number of elements passed as an initializer list to a TVector constructor.");
        int pos = 0;
        for (const auto value : components) {
            Data[pos++] = value;
        }
    }

    const float* GetData() const { return Data; }
    static size_t GetDataSize() { return sizeof(float) * n; }

    float& operator()(const int i) { return Data[i]; }
    float operator()(const int i) const { return Data[i]; }

private:
    float Data[n];
};


template <unsigned int n = 4, unsigned int m = 4>
class TMatrix {
public:
    TMatrix() {}

    static TMatrix Zero() {
        TMatrix zero;
        for (int pos = 0; pos < n * m; ++pos) {
            zero.Data[pos] = 0;
        }
        return zero;
    }

    static TMatrix Factor(const float factor) {
        TMatrix result;
        for (int pos = 0; pos < n * m; ++pos) {
            result.Data[pos] = 0;
        }
        for (int pos = 0; pos < n * m; pos += n + 1) {
            result.Data[pos] = factor;
        }
        return result;
    }

    static TMatrix Identity() {
        return Factor(1);
    }

    TMatrix(const TMatrix<n, m>& other) {
        std::memcpy(Data, other.GetData(), GetDataSize());
    }

    TMatrix(const std::initializer_list<float>& components) {
        ENSURE(components.size() == n * m,
               "Wrong number of elements passed as an initializer list to a TMatrix constructor.");
        auto iter = components.begin();
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                (*this)(i, j) = *iter;
                ++iter;
            }
        }
    }

    const float* GetData() const { return Data; }
    static size_t GetDataSize() { return sizeof(float) * n * m; }

    float& operator()(const int i, const int j) { return Data[i + j * n]; }
    float operator()(const int i, const int j) const { return Data[i + j * n]; }

private:
    float Data[n * m];
};

using TTransformation = TMatrix<4, 4>;

class TQuaternion {
public:
    TQuaternion(float r, float ux, float uy, float uz);
    TQuaternion(const float r, const TVector<3>& u);

    TQuaternion operator*(const TQuaternion& other) const;

    TTransformation GetRotationMatrix() const;
    TMatrix<3, 3> GetRotationMatrix3x3() const;

    static TQuaternion FromRotation(const float angle, const TVector<3>& axis);

private:
    float R; // cos(angle / 2)
    TVector<3> U; // sin(angle / 2) * axis
};

TTransformation GetProjectionMatrix(const float nearClip,
                                    const float farClip,
                                    const float horizontalAov,
                                    const float verticalAov,
                                    const float xOffset = 0,
                                    const float yOffset = 0);
TTransformation GetInverseProjectionMatrix(const float nearClip,
                                           const float farClip,
                                           const float horizontalAov,
                                           const float verticalAov,
                                           const float xOffset = 0,
                                           const float yOffset = 0);
TTransformation GetOrthoProjectionMatrix(const float nearClip,
                                         const float farClip,
                                         const float width,
                                         const float height,
                                         const float xOffset = 0,
                                         const float yOffset = 0);
TTransformation GetInverseOrthoProjectionMatrix(const float nearClip,
                                                const float farClip,
                                                const float width,
                                                const float height,
                                                const float xOffset = 0,
                                                const float yOffset = 0);
float CalcFogStart(const float fogStartDistance, const float nearClip, const float farClip);
TTransformation GetViewMatrix(const TVector<3>& eye, const TVector<3>& lookAt, const TVector<3>& up);
TTransformation GetInverseViewMatrix(const TVector<3>& eye, const TVector<3>& lookAt, const TVector<3>& up);
TTransformation GetTranslationMatrix(const TVector<3>& translation);
TTransformation GetScalingMatrix(const TVector<3>& scale);
TTransformation GetRotationMatrix(const TVector<3>& axis, const float angle);

TVector<3> Get3DVector(const TVector<4>& vector);
TVector<3> Get3DPoint(const TVector<4>& point);
TVector<4> Get4DVector(const TVector<3>& vector);
TVector<4> Get4DPoint(const TVector<3>& point);


template <unsigned int n, unsigned int m>
TMatrix<m, n> Transpose(const TMatrix<n, m>& matrix) {
    TMatrix<m, n> result;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            result(i, j) = matrix(j, i);
        }
    }
    return result;
}


template <unsigned int n>
TVector<n>& operator+=(TVector<n>& lhs, const TVector<n>& rhs) {
    for (int i = 0; i < n; ++i) {
        lhs(i) += rhs(i);
    }
    return lhs;
}


template <unsigned int n>
TVector<n> operator+(const TVector<n>& lhs, const TVector<n>& rhs) {
    TVector<n> sum = lhs;
    return sum += rhs;
}


template <unsigned int n>
TVector<n>& operator-=(TVector<n>& lhs, const TVector<n>& rhs) {
    for (int i = 0; i < n; ++i) {
        lhs(i) -= rhs(i);
    }
    return lhs;
}


template <unsigned int n>
TVector<n> operator-(const TVector<n>& lhs, const TVector<n>& rhs) {
    TVector<n> diff = lhs;
    return diff -= rhs;
}


template <unsigned int n>
TVector<n> operator-(const TVector<n>& vec) {
    TVector<n> neg;
    for (int i = 0; i < n; ++i) {
        neg(i) = -vec(i);
    }
    return neg;
}


template <unsigned int n>
TVector<n> operator*(const TVector<n>& lhs, const float rhs) {
    TVector<n> result;
    for (int i = 0; i < n; ++i) {
        result(i) = lhs(i) * rhs;
    }
    return result;
}


template <unsigned int n>
TVector<n> operator*(const float lhs, const TVector<n>& rhs) {
    return rhs * lhs;
}


template <unsigned int n>
TVector<n> operator/(const TVector<n>& lhs, const float rhs) {
    TVector<n> result;
    for (int i = 0; i < n; ++i) {
        result(i) = lhs(i) / rhs;
    }
    return result;
}


template <unsigned int n, unsigned int m>
TMatrix<n, m> operator+(const TMatrix<n, m>& lhs, const TMatrix<n, m>& rhs) {
    TMatrix<n, m> sum = lhs;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            sum(i, j) += rhs(i, j);
        }
    }
    return sum;
}


template <unsigned int n>
float CalcScalarProduct(const TVector<n>& lhs, const TVector<n>& rhs) {
    float product = 0;
    for (int pos = 0; pos < n; ++pos) {
        product += lhs(pos) * rhs(pos);
    }
    return product;
}


TVector<3> CalcVectorProduct(const TVector<3>& lhs, const TVector<3>& rhs);


template <unsigned int n>
inline float CalcSquaredLength(const TVector<n>& vec) {
    return CalcScalarProduct(vec, vec);
}


template <unsigned int n>
float GetLength(const TVector<n>& vec) {
    return std::sqrt(CalcSquaredLength(vec));
}


template <unsigned int n>
TVector<n> Normalize(const TVector<n>& vec) {
    return vec / GetLength(vec);
}


template <unsigned int n, unsigned int m>
TMatrix<n, m> operator-(const TMatrix<n, m>& lhs, const TMatrix<n, m>& rhs) {
    TMatrix<n, m> diff = lhs;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            diff(i, j) -= rhs(i, j);
        }
    }
    return diff;
}


template <unsigned int n, unsigned int m>
TMatrix<n, m> operator-(const TMatrix<n, m>& mat) {
    TMatrix<n, m> neg;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            neg(i, j) = -mat(i, j);
        }
    }
    return neg;
}


template <unsigned int n, unsigned int m>
TMatrix<n, m> operator*(const TMatrix<n, m>& lhs, const float rhs) {
    TMatrix<n, m> result;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            result(i, j) = lhs(i, j) * rhs;
        }
    }
    return result;
}


template <unsigned int n, unsigned int m>
TMatrix<n, m> operator*(const float lhs, const TMatrix<n, m>& rhs) {
    return rhs * lhs;
}


template <unsigned int n, unsigned int m>
TMatrix<n, m> operator/(const TMatrix<n, m>& lhs, const float rhs) {
    TMatrix<n, m> result;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            result(i, j) = lhs(i, j) / rhs;
        }
    }
    return result;
}


template <unsigned int n, unsigned int k, unsigned int m>
TMatrix<n, m> operator*(const TMatrix<n, k>& lhs, const TMatrix<k, m>& rhs) {
    TMatrix<n, m> prod = TMatrix<n, m>::Zero();
    for (int i = 0; i < n; ++i) {
        for (int l = 0; l < k; ++l) {
            for (int j = 0; j < m; ++j) {
                prod(i, j) += lhs(i, l) * rhs(l, j);
            }
        }
    }
    return prod;
}


template <unsigned int n, unsigned int k>
TVector<n> operator*(const TMatrix<n, k>& lhs, const TVector<k>& rhs) {
    TVector<n> prod = TVector<n>::Zero();
    for (int i = 0; i < n; ++i) {
        for (int l = 0; l < k; ++l) {
            prod(i) += lhs(i, l) * rhs(l);
        }
    }
    return prod;
}
