#pragma once
#include "asset_library.h"
#include "mouse.h"
#include "renderer.h"
#include "window.h"

void Loop(TWindow* window, TRenderer* renderer, TMouse* mouse, TAssetLibrary* assetLibrary);
