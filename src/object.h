#pragma once
#include "geometry.h"
#include "model.h"

struct TObject {
    const int modelId;
    TVector<3> scale;

    // position
    TQuaternion rotation;
    TVector<3> translation;

    // velocity
    TVector<3> velocity = {0, 0, 0};
    TVector<3> angularVelocity = {0, 0, 0};

    float mass = 1;
    TVector<3> centerOfMass = {0, 0, 0};
    TMatrix<3, 3> invertedInertiaTensor = {1, 0, 0,
                                           0, 1, 0,
                                           0, 0, 1};
};

void MoveObject(const float time,
                const TVector<3>& force,
                const TVector<3>& forceMomentum,
                TObject* object);
