## Description

A 3D rendering engine using deferred shading.
Made this to educate myself by implementing rendering techniques.
Check out the video at: https://youtu.be/HPi8B_VOpf8.

Implemented techniques and the sources I used:

* **Quadtrees fully on GPU**.\
    The application presents a terrain generated using the algorithm.\
    The detalization level of the parts of the terrain changes dynamically depending on the proximity (in $`xy`$ plane) to the observer.\
    _Source: "[Quadtrees on the GPU](https://doi.org/10.1201/9781351052108-12)" paper by Jonathan Dupuy, Jean-Claude Iehl and Pierre Poulin._

* **Spherical harmonics**.\
    To calculate the environment light intensity.\
    Only the rays falling from the sky from all directions are considered.\
    _Source: "[Spherical Harmonic Lighting: The Gritty Details](http://www.cse.chalmers.se/~uffe/xjobb/Readings/GlobalIllumination/Spherical%20Harmonic%20Lighting%20-%20the%20gritty%20details.pdf)" by Robin Green._

* **TAA** (Temporal Antialiasing).\
    My implementation features jittering with Halton(2, 3) sampling pattern, reprojection and history rectification with variance clipping.\
    _Source: "Temporal Antialiasing Techniques" [talk](https://www.youtube.com/watch?v=Ya8xgT0_SpM) by Lei Yang and a [paper](http://behindthepixels.io/assets/files/TemporalAA.pdf)._

* **PSSM** (Parallel-Split Shadow Maps).\
    The application presents shadow maps for a directional light source with clip planes parallel to the screen.\
    The split planes are chosen as a linear mix between logarithmic and uniform splits.\
    _Source: "[Parallel-Split Shadow Maps on Programmable GPUs](https://developer.nvidia.com/gpugems/gpugems3/part-ii-light-and-shadows/chapter-10-parallel-split-shadow-maps-programmable-gpus)" by Fan Zhang, Hanqiu Sun and Oskari Nyman._

* **PBR** (Physically Based Rendering).\
    The Fresnel reflactance term is calculated via Schlick's approximation.\
    The normal distribution term is the normalized specular term of the Blinn-Phong model.\
    The geometric term is Cook-Torrance factor approximation by Kelemen et. al.\
    The diffuse BRDF is Lambertian.\
    _Source: a [SIGGRAPH course](http://renderwonk.com/publications/s2010-shading-course/), specifically notes by Naty Hoffman and Yoshiharu Gotanda._

* **Atmospheric scattering**.\
    The sky and fog are drawn taking the atmospheric scattering into account.\
    _Sources: "[Display of The Earth Taking into Account Atmospheric Scattering](http://nishitalab.org/user/nis/cdrom/sig93_nis.pdf)" by Tomoyuki Nishita, Takao Sirai, Katsumi Tadamura and Eihachiro Nakamae; also "[Real-time Atmospheric Effects in Games](https://developer.amd.com/wordpress/media/2012/10/Wenzel-Real-time_Atmospheric_Effects_in_Games.pdf)" by Carsten Wenzel._

* **HDR** (High Dynamic Range).\
    The engine does tone mapping, gamma correction and all color calculations in linear color space.\
    _Sources: a [blog post](http://graphicrants.blogspot.com/2013/12/tone-mapping.html) by Brias Karis and an [article](https://developer.nvidia.com/gpugems/gpugems3/part-iv-image-effects/chapter-24-importance-being-linear) by Larry Gritz and Eugene d'Eon._

* **PCF** (Percentage-Closer Filtering) with **dithering**.\
    The shadows are antialiased using dithered PCF.\
    _Source: "[Shadow map antialiasing](https://developer.nvidia.com/gpugems/gpugems/part-ii-lighting-and-shadows/chapter-11-shadow-map-antialiasing)" by Michael Bunnell and Fabio Pellacini._

## Installation

* Install VS C++ Build Tools.
* Install Windows SDK.

Either:

* Open `graphic-zoo.sln` with Visual Studio and build.

or

* Run `build.bat` from Developer Command Prompt.

## Controls

* Move the mouse to look around.
* Press `w`, `a`, `s`, `d`, `q` and `e` to move.
* Hold `Shift` to move faster.
* Press `Space` to pause.
* Press `Esc` to exit.
