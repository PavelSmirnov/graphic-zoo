@ECHO off

CD %~dp0

SET CXX_ARGS=/std:c++17 /EHsc

IF "%OS%" == "Windows_NT" (
    msbuild graphic-zoo.sln /property:Configuration=Release /m
    IF NOT EXIST "graphic-zoo.exe" (
        MKLINK graphic-zoo.exe x64\Release\graphic-zoo.exe
    )
) ELSE (
    ECHO Unknown platform '%OS%', can't build.
)
